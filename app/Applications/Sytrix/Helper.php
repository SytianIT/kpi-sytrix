<?php

namespace App\Applications\Sytrix;
use Illuminate\Database\Eloquent\Model;
use App\Sytrix\Project;

class Helper {

	public function getName($modelName, $id, $columnName = "") {	

		$model = $modelName->find($id);

		return $model->$columnName;

	}	

}