<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectCategory extends \Baum\Node
{
	
	use SoftDeletes;

	protected $fillable = [
        'title',
        'description',
        'other_data'
    ];

    protected $dates = ['deleted_at'];

}
