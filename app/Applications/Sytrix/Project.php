<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Department;
use App\Applications\Sytrix\ProjectCategory;
use App\Applications\Sytrix\Client;
use App\Applications\Sytrix\ProjectStatus;
use App\Applications\Sytrix\TimeTrack;
use App\Applications\Sytrix\Card;
use App\User;

class Project extends \Baum\Node
{

	use SoftDeletes;
    
	protected $fillable = [
		'name',
		'description',
		'max_development_time',
		'max_design_time',
		'max_pm_time',
		'project_status_id',
		'client_id'
    ];

    protected $dates = [
    	'deleted_at',
    	'start_date',
		'due_date'
    ];

    public function categories() {
    	return $this->belongsToMany(ProjectCategory::class, 'category_project');
    }	

    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function departments() {
    	return $this->belongsToMany(Department::class);
    }

    public function status() {
		return $this->belongsTo(ProjectStatus::class, 'project_status_id');
    }

    public function returnStatus() {

    	$currentStatus = $this->status;

    	if( $currentStatus ) {
	    	if( $currentStatus->isChild() ) {
	    		$parentStatus = $currentStatus->ancestors()->first();
	    	}
 		   	return compact('currentStatus', 'parentStatus');
    	}

    	return false;

    }

    public function cards() {
        return $this->hasMany(Card::class, 'project_id');
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }

    public function endDate() {
        return $this->timeTracks()->orderBy('date', 'desc')->first();
    }

    public function pm() {
        return  $this->belongsTo(User::class, 'pm_id');
    }

    // Helper Methods
    public function timeTracks (){
        return $this->hasMany(TimeTrack::class, 'project_id');
    }

    public function timeTracksSumDev() 
    {
        $totalMins = $this->timeTracks()->whereHas('user', function($user) {
            $user->where('job_title', 'like', '%developer%');
        })->with('user')->sum('hours_rendered');
        $hours = (int) ($totalMins / 60);
        $mins = $totalMins % 60;

        if( $hours > 0 ) {
            $hoursText = $hours.'hr';      
        }
        else {
            $hoursText = '';
        }

        if( $mins > 0 ) {
            if( $hours > 0 ) {
                $minsText = ' - ' . $mins.'min';
            }
            else {
                $minsText = $mins.'min';
            }
        }
        else {
            $minsText = '';
        }

        return $hoursText . $minsText;
    }

    public function timeTracksSumDesign() 
    {
        $totalMins = $this->timeTracks()->whereHas('user', function($user) {
            $user->where('job_title', 'like', '%designer%');
        })->with('user')->sum('hours_rendered');
        
        $hours = (int) ($totalMins / 60);
        $mins = $totalMins % 60;

        if( $hours > 0 ) {
            $hoursText = $hours.'hr';      
        }
        else {
            $hoursText = '';
        }

        if( $mins > 0 ) {
            if( $hours > 0 ) {
                $minsText = ' - ' . $mins.'min';
            }
            else {
                $minsText = $mins.'min';
            }
        }
        else {
            $minsText = '';
        }

        return $hoursText . $minsText;
    }

    public function totalHoursHTML($totalHours = null) {
        $totalMins = $totalHours ? $totalHours : $this->timeTracks->sum('hours_rendered');
        $hours = (int) ($totalMins / 60);
        $mins = $totalMins % 60;

        if( $hours > 0 ) {
            $hoursText = $hours.'hr';      
        }
        else {
            $hoursText = '';
        }

        if( $mins > 0 ) {
            if( $hours > 0 ) {
                $minsText = ' - ' . $mins.'min';
            }
            else {
                $minsText = $mins.'min';
            }
        }
        else {
            $minsText = '';
        }

        return $hoursText . $minsText;
    }

    public function totalHours() {
        $totalMins = $this->timetracks->sum('hours_rendered');
        $hours = (int) ($totalMins / 60);

        return $hours;
    }
    
    public function estimatedHours() {
        $desTime =  $this->max_design_time;
        $devTime =  $this->max_development_time;
        $pmTime =  $this->max_pm_time;

        return ($desTime + $devTime + $pmTime);

    }

}
