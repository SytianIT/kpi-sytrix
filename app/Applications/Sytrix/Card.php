<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\TimeTrack;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
    
	use SoftDeletes;

	protected $table = 'project_cards';

	protected $fillable = [
        'project_id',
        'title',
        'slug'
    ];

    protected $dates = ['deleted_at'];

    public function tasks() {
    	return $this->hasMany(Task::class, 'card_id');
    }

    public function users() {
        return $this->belongsToMany(User::class, 'project_card_user');
    }
    
    public function timeTracks() {
        return $this->hasMany(TimeTrack::class, 'timeable_id');
    }

}
