<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\TimeTrack;
use App\User;

class Task extends Model
{

	protected $table = 'stx_tasks';

	protected $fillable = [
		'tiitle',
		'user_id',
		'card_id',
    ];

	public function context() {
        return $this->morphTo();
    }

	public function tasks() {
		return  $this->belongsTo(Project::class, 'project_id');
	}

	public function assignedUsers() {
		return $this->belongsToMany(User::class, 'user_id');
	}

	public function card() {
		return $this->belongsTo(Card::class, 'card_id');
	}

	public function timeTracks() {
		return $this->hasMany(TimeTrack::class, 'timeable_id');
	}

}