<?php

namespace App\Applications\Sytrix;

use App\User;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

	protected $appends = ['elapsed_time'];

 	public function contextable() {
        return $this->morphTo();
    }

    public function getElapsedTimeAttribute() {
    	return $this->created_at->diffForHumans();
    }
}
