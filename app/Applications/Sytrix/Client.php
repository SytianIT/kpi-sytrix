<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use App\Applications\Sytrix\Project;

use Illuminate\Database\Eloquent\SoftDeletes;


class Client extends Model
{

	use SoftDeletes;

	protected $fillable = [
		'company_logo',
		'company_name',
		'company_addres',
		'contact_person',
		'mobile_number',
		'landline_number',
		'email',
		'skype_id',
		'payment_terms',
		'discounts',
		'layout',
		'hosting',
		'domain',
		'notes'		
	];
	
	protected $dates = ['deleted_at'];
	
	public function projects() {
		return $this->hasMany(Project::class, 'client_id');
	}

}
