<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectStatus extends \Baum\Node
{	

	use SoftDeletes;

	protected $table = 'project_status';

	protected $fillable = [
        'name',
        'title',
        'description'
    ];

     protected $dates = ['deleted_at'];

}
