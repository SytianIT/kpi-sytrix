<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;

use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\Client;
use App\User;

class TimeTrack extends Model
{
    protected $fillable = [
		'user_id',
		'timeable_id',
		'timeable_type',
		'time_in',
		'time_out',
		'hours_rendered',
    ];

    protected $appends =  ['note_html'];

    public function project() {
    	return $this->belongsTo(Project::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function timeable() {
        return $this->morphTo();
    }

    public function manuallyCreatedBy() {
        return $this->belongsTo(User::class, 'manual_entry');
    }

    public function getNoteHtmlAttribute() {
        return nl2br($this->note);
    }    
}