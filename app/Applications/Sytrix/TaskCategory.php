<?php

namespace App\Applications\Sytrix;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskCategory extends \Baum\Node
{

	use SoftDeletes;

	protected $table = 'stx_task_categories';

	protected $fillable = [
        'name',
        'title'
    ];

	protected $dates = ['deleted_at'];

}
