<?php

namespace App\Applications\Expense;

use App\Applications\Expense\Currency;
use App\Applications\Expense\ExpenseCategory;
use App\Applications\Expense\Payment;
use App\Applications\Expense\Recurring;
use App\BaseModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends BaseModel
{
    use SoftDeletes;

    protected $table = 'exp_expenses';

    protected $dates = ['date_occured','deleted_at','created_at','updated_at','next_expense_recurring'];

    protected $casts = [
        'attachments' => 'array'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function recurring()
    {
        return $this->belongsTo(Recurring::class, 'recurring_id');
    }

    public function mainRecurrings()
    {
        return $this->belongsTo(Recurring::class, 'main_recurring_id');
    }
    
    public function currency()
    {
    	return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function category()
    {
    	return $this->belongsTo(ExpenseCategory::class, 'category_id');	
    }

    public function payment()
    {
    	return $this->belongsTo(Payment::class, 'payment_id');
    }

    /**
     * Scope Queries
     */
    public function scopeRecurrings($query, $args = true)
    {
        if($args){
            return $query->whereNotNull('recurring_id');
        }
        
        return $query->whereNull('recurring_id');
    }

    /**
     * Getters and Setters
     */
    public function getCanRecurrAttribute()
    {
        if($this->recurring_times != 0){
            if($this->count_expense_recurring < $this->recurring_times){
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Other Methods & Static Function that can be called globally
     */

    public static function filterExpense($request, $query, $recurrings = false)
    {
        if($request->has('filter_key')){
            $string = $request->filter_key;
            $query = $query->orWhere('id', 'LIKE', "%{$string}%")
                           ->orWhere('title', 'LIKE', "%{$string}%");
        }

        if($request->has('filter_daterange')){
            $dates = explode(' - ', $request->filter_daterange);
            if(count($dates) != 2){
                return redirect()->back()
                                 ->withWarning('There is an error in filter request.');
            }
            $fromDates = new Carbon($dates[0]);
            $toDates = new Carbon($dates[1]);

            if($recurrings){
                $query = $query->whereHas('recurring', function($q) use($fromDates, $toDates){
                    $q->where('next_expense_recurring', '>=', $fromDates->format('Y-m-d'))
                      ->where('next_expense_recurring', '<=', $toDates->format('Y-m-d'));
                });
            } else {
                $query = $query->where('date_occured', '>=', $fromDates->format('Y-m-d'))
                               ->where('date_occured', '<=', $toDates->format('Y-m-d'));
            }
        }

        if($request->has('filter_category')){
            $query = $query->whereHas('category', function ($query) use ($request){
                        $query->where('id', '=', $request->filter_category);
                    });
        }

        if($request->has('filter_payment_method')){
            $query = $query->whereHas('payment', function ($query) use ($request){
                        $query->where('id', '=', $request->filter_payment_method);
                    });
        }

        return $query;
    }
}
