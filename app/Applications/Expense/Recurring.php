<?php

namespace App\Applications\Expense;

use App\Applications\Expense\Expense;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Recurring extends Model
{
    protected $table = 'exp_recurring_expense';

    protected $dates = ['next_expense_recurring'];

    const ACTIVE = 'active';
	const EXPIRED = 'expired';
	const DISABLED = 'disabled';

    public static $status = [
    	self::ACTIVE => 'Active',
    	self::EXPIRED => 'Expired',
    	self::DISABLED => 'Disabled'
    ];

    public static $statusClass = [
        self::ACTIVE => 'label-success',
        self::EXPIRED => 'label-yellow',
        self::DISABLED => 'label-black'
    ];

    const DAYS = 'days';
    const WEEKS = 'weeks';
    const MONTHS = 'months';
    const YEARS = 'years';

    public static $recurEveryEntities = [
        self::DAYS => 'Days', 
        self::WEEKS => 'Weeks', 
        self::MONTHS => 'Months', 
        self::YEARS => 'Years'
    ];

    public function expense()
    {
    	return $this->hasOne(Expense::class, 'recurring_id');
    }

    public function expenseEntries()
    {
    	return $this->hasMany(Expense::class, 'main_recurring_id');
    }

    /**
     * Scope Queries
     */
    public function scopeActive($query)
    {
        return $query->where('status', '=', self::ACTIVE);
    }

    /**
     * Other Methods & Static Function that can be called globally
     */
    public function generateNextExpenseRecurring($date, $every, $of, $times)
    {
        $date = new Carbon($date);
        if ($of == self::DAYS){
            return $date->addDays($every);
        } else if ($of == self::WEEKS){
            return $date->addWeeks($every);
        } else if ($of == self::MONTHS){
            return $date->addMonths($every);
        } else if ($of == self::YEARS){
            return $date->addYears($every);
        }
    }
}
