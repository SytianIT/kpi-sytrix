<?php

namespace App\Applications\Expense;

use App\BaseModel;

class Payment extends BaseModel
{
	public function expenses()
	{
		return $this->hasMany(Expense::class, 'payment_id');
	}
}
