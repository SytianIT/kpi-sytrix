<?php

namespace App\Applications\Expense;

use App\BaseModel;
use App\Applications\Expense\Expense;

class Currency extends BaseModel
{
    protected $table = 'currency';

    public function expenses()
    {
    	return $this->hasMany(Expense::class, 'currency_id');
    }
}
