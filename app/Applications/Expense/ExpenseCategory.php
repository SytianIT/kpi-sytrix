<?php

namespace App\Applications\Expense;

use App\BaseModel;
use App\Applications\Expense\Expense;
use Illuminate\Database\Eloquent\Collection;

class ExpenseCategory extends BaseModel
{
    protected $table = "exp_expense_category";

    public function expenses()
    {
    	return $this->hasMany(Expense::class, 'category_id');
    }

    /**
     * Custom Get Attributes
     */
    public function getChildLevelAttribute()
    {
    	$level = $this->categoryLevel($this);
    	
    	return $level;
    }

    private function categoryLevel($obj, $level = 0)
    {
    	if($obj->parent_id != null){
    		$newObj = ExpenseCategory::find($obj->parent_id);
    		$level = $level + 1;
    		$level = $this->categoryLevel($newObj, $level);
    	}

    	return $level;
    }

    public function getHasSubAttribute()
    {
        $childs = ExpenseCategory::where('parent_id', '=', $this->id)->get();

        if($childs->count() > 0){
            return true;
        }

        return false;
    }

    /**
     * Custom Query Builders
     */
    public static function getParentNested()
    {
    	$category = new ExpenseCategory;
    	$categories = $category->whereNull('parent_id')->orderBy('created_at')->get();

    	return $category->getParentChilds($categories);
    }

    private function getParentChilds($collection)
    {
    	$newCollect = new Collection;

    	foreach($collection as $item){
    		$newCollect->push($item);
    		$childs = ExpenseCategory::where('parent_id', '=', $item->id)->get();
    		if($childs->count() > 0){
    			$newCollect = $newCollect->merge($this->getParentChilds($childs));
    		}
    	}

    	return $newCollect;
    }
}
