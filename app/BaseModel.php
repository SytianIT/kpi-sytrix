<?php

namespace App;

use App\AuditLog;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	/**
	 * Method call to insert log for any database entry
	 * @param  String $description
	 * @param  Object $syion       
	 * @param  Object $user        
	 * @return Boolean              
	 */
    public function insertAuditLog($description, $syion, $user)
	{
		$log = new AuditLog;
		$log->activity_desc = $description;
		$log->user()->associate($user);
		$log->syion()->associate($syion);
		$log->auditable()->associate($this);
		$log->save();

		return true;
	}
}
