<?php

namespace App;

use App\AuditLog;
use App\Permission;
use Illuminate\Database\Eloquent\Model;

class Syion extends Model
{
	const GENERAL = 'general';
	const EXPENSE = 'expense';
	const SYTRIX = 'sytrix';

    protected $table = "syion";

    /**
     * Relation of has one Permission class
     * @return Collection collection of all permission related to this Syion
     */
    public function permissions()
    {
    	return $this->hasMany(Permission::class, 'syion_id');	
    }

    public function logs()
    {
    	return $this->hasMany(AuditLog::class, 'syion_id');
    }

    /**
     * Custom method to get the syion needed
     * @param  String $name
     * @return Object 
     */
   	public static function getThisSyion($name)
   	{
   		return Syion::where('name', '=', $name)->first();
   	}
}
