<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	/**
	 * The attributes that are mass assignable
	 * @var array
	 */
    protected $fillable = [
    	'name','title','description'
    ];

    /**
     * Relation of many to many for Staff class
     * @return Collection collection of staffs
     */
    public function roles()
    {
    	return $this->belongsToMany(Role::class, 'role_permissions', 'permission_id', 'role_id');
    }

    /**
     * Relation of belongs to Syion class
     * @return Object object of syion
     */
    public function syion()
    {
        return $this->belongsTo(Syion::class, 'syion_id');
    }
}
