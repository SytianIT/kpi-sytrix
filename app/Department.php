<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{	

    use softDeletes;

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
        'title'
    ];

    protected $dates = ['deleted_at'];

    public function users() {
    	return $this->hasMany(User::Class, 'department_id');
    }

}
