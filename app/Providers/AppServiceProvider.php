<?php

namespace App\Providers;

use App\Applications\Expense\Currency;
use App\Applications\Expense\Expense;
use App\Applications\Expense\ExpenseCategory;
use App\Applications\Expense\Payment;
use App\Applications\Expense\Recurring;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Task;

use App\User;
use App\Department;

use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * General Application Provider
         * @var view
         */
        View::composer([
            'templates.master',
            'modules.*',
            'expense-app.*'
            ], function($view)
        {
            $user = Auth::user();
            
            $view->with('authUser', $user ? $user : null);
        });

        /**
         * Expense Application Provider
         */
        View::composer([
            'expense-app.*',
            ], function($view)
        {
            $title = 'Sytian Management System - Expense';
            $view->with('title', $title);
        });
        View::composer([
            'expense-app.expense.*',
            ], function($view)
        {
            $status = Recurring::$status;
            $category = ExpenseCategory::getParentNested();

            $payments = Payment::lists('title','id');
            $currencies = Currency::get();

            $view->with('status', $status ? $status : null);
            $view->with('category', $category ? $category : null);
            $view->with('payments', $payments ? $payments : null);
            $view->with('currencies', $currencies ? $currencies : null);
        });
        /**
         * End Expense Application Provider
         */

        /**
         * Blade Format Providers
         */
        Blade::directive('money', function($expression) {
            return "<?php echo number_format($expression, 2, '.', ',') ?>";        
        });


        /**
        * Sytrix Application Provider
        **/
        // employees collection
        View::composer(['sytrix-app.*'], function($view) {
            $employees = User::where('id', '!=', 1)->get();

            $view->with('employees', $employees ? $employees : null);
        });

        // departments collection
        View::composer(['sytrix-app.*'], function($view) {
            $departments = Department::all();

            $view->with('departments', $departments ? $departments : null);
        });

         // notifications collection
        View::composer(['*'], function($view) {

            if( Auth::user() ) {
                $user = Auth::user();
                $notifications = $user->notifications()->where('is_read', 0)->orderBy('id', 'desc')->get();
            }
            else {
                $notifications = null;
            }

            $view->with('notifications', $notifications ? $notifications : null);
        });

        /*
        * Blade Time Format
        */
        Blade::directive('formatTime', function($theTime){
            return "<?php echo (date('g:i A', strtotime($theTime))); ?>";
        });

        /*
        * Blade for Date
        */
        Blade::directive('formatDate', function($theDate){
            return "<?php echo (date('M j, Y', strtotime($theDate))); ?>";
        });

        /*
        * Blade Hours Rendered Format
        */
        Blade::directive('hoursRendered', function($theMins){
            return "<?php echo (floor($theMins / 60) ). ':' . (intval($theMins) % 60); ?>";
        });

        Blade::directive('theMins', function($theMins){
            return "<?php echo (intval($theMins) % 60); ?>";
        });

        Blade::directive('theHours', function($theMins){
            return "<?php echo (floor($theMins / 60) ); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
