<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Authentication Routes
 */
Route::get('login', ['uses' => 'Auth\AuthController@showLoginForm', 'as' => 'show.login']);
Route::post('login', ['uses' => 'Auth\AuthController@login', 'as' => 'auth.login']);

Route::get('register', ['uses' => 'Auth\AuthController@showRegistrationForm', 'as' => 'show.registration']);
Route::post('register', ['uses' => 'Auth\AuthController@register', 'as' => 'auth.register']);

Route::post('password/email', ['uses' => 'Auth\PasswordController@sendResetLinkEmail', 'as' => 'send.reset.link']);
Route::post('password/email/validate', ['uses' => 'Auth\PasswordController@validateEmail', 'as' => 'password.reset.validate.email']);
Route::get('password/reset/{token?}', ['uses' => 'Auth\PasswordController@showResetForm', 'as' => 'show.password.reset']);
Route::post('password/reset', ['uses' => 'Auth\PasswordController@reset', 'as' => 'password.reset']);

/**
 * Routes to be accessed with auth object
 */
Route::group(['middleware' => 'auth'], function()
{
	Route::get('logout', ['uses' => 'Auth\AuthController@logout', 'as' => 'auth.logout']);
	Route::get('/', 'GeneralController@index');
	Route::get('/home', 'GeneralController@index')->name('home');

	/**
	 * Applications Controller
	 */
	Route::get('/', ['uses' => 'GeneralController@index', 'as' => 'main-app']);
	Route::get('expense-app', ['middleware' => 'expense.app', 'uses' => 'GeneralController@goExpense', 'as' => 'expense-app']);
	Route::get('sytrix-app', ['uses' => 'GeneralController@goSytrix', 'as' => 'sytrix-app']);

	/**
	 * General Settings Routes
	 */
	Route::get('settings', ['uses' => 'SettingsController@index', 'as' => 'settings']);

	/**
	 * Roles Routes
	 */
	Route::get('roles', ['uses' => 'RoleController@index', 'as' => 'roles']);
	Route::get('role/create', ['uses' => 'RoleController@create', 'as' => 'role.create']);
	Route::post('role/store', ['uses' => 'RoleController@store', 'as' => 'role.store']);
	Route::put('role/{role}', ['uses' => 'RoleController@update', 'as' => 'role.update']);
	Route::get('role/{role}/edit', ['uses' => 'RoleController@edit', 'as' => 'role.edit']);
	Route::get('role/{role}/delete', ['uses' => 'RoleController@delete', 'as' => 'role.delete']);	

	/**
	 * User Routes
	 */
	Route::get('users', ['uses' => 'UserController@index', 'as' => 'users']);
	Route::get('user/create', ['uses' => 'UserController@create', 'as' => 'user.create']);
	Route::post('user/store', ['uses' => 'UserController@store', 'as' => 'user.store']);
	Route::put('user/{user}', ['uses' => 'UserController@update', 'as' => 'user.update']);
	Route::get('user/{user}/edit', ['uses' => 'UserController@edit', 'as' => 'user.edit']);
	Route::get('user/{user}/delete', ['uses' => 'UserController@delete', 'as' => 'user.delete']);

	/**
	 * Profile Routes
	 */
	Route::get('profile', ['uses' => 'AccountController@profile', 'as' => 'profile']);
	Route::post('profile/update', ['uses' => 'AccountController@updateProfile', 'as' => 'profile.update']);
	/**
	 * Department Routes
	*/
	Route::get('departments', ['uses' => 'DepartmentsController@index', 'as' => 'departments']);
	Route::get('departments/create', ['uses' => 'DepartmentsController@create', 'as' => 'department.create']);
	Route::post('departments/store', ['uses' => 'DepartmentsController@store', 'as' => 'department.store']);
	Route::get('departments/{department}/edit', ['uses' => 'DepartmentsController@edit', 'as' => 'department.edit']);
	Route::post('departments/{department}/update', ['uses' => 'DepartmentsController@update', 'as' => 'department.update']);
	Route::get('departments/{department}/delete', ['uses' => 'DepartmentsController@delete', 'as' => 'department.delete']);
	Route::get('departments/trash/{trash}', ['uses' => 'DepartmentsController@index', 'as' => 'department.trash']);
	Route::get('departments/{department}/restore/', ['uses' => 'DepartmentsController@restore', 'as' => 'department.restore']);

	/**
	 * Routes for Expense Application
	 */
	Route::group(['prefix' => 'expense-app', 'namespace' => 'Expense', 'middleware' => 'expense.app'], function(){

		/**
		 * Routes for Expense Settings Module
		 */
		Route::get('categories', ['uses' => 'SettingsController@categories', 'as' => 'expense.categories']);
		Route::post('category/store', ['uses' => 'SettingsController@storeCategory', 'as' => 'expense.category.store']);
		Route::get('category/validate', ['uses' => 'SettingsController@validateCategoryDetails', 'as' => 'expense.category.validate']);
		Route::get('category/{category}/edit', ['uses' => 'SettingsController@editCategory', 'as' => 'expense.category.edit']);
		Route::post('category/{category}/update', ['uses' => 'SettingsController@updateCategory', 'as' => 'expense.category.update']);
		Route::get('category/{category}/delete', ['uses' => 'SettingsController@deleteCategory', 'as' => 'expense.category.delete']);

		Route::get('mode-of-payment', ['uses' => 'SettingsController@payments', 'as' => 'payments']);
		Route::get('currencies', ['uses' => 'SettingsController@currencies', 'as' => 'currencies']);

		/**
		 * Routes for Expense API calls
		 */
		Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
			Route::get('get/categories', ['uses' => 'SettingsController@allCategories']);
			Route::post('store/category', ['uses' => 'SettingsController@storeCategory']);
			Route::post('update/{category}/category', ['uses' => 'SettingsController@updateCategory']);
			Route::get('delete/{category}/category', ['uses' => 'SettingsController@deleteCategory']);
			Route::get('get/{category}/child', ['uses' => 'SettingsController@getChildCategories']);
			Route::post('create/{category}/child', ['uses' => 'SettingsController@createChildCategory']);

			Route::get('get/payments', ['uses' => 'SettingsController@allPayments']);
			Route::post('store/payment', ['uses' => 'SettingsController@storePayment']);
			Route::post('update/{payment}/payment', ['uses' => 'SettingsController@updatePayment']);
			Route::get('delete/{payment}/payment', ['uses' => 'SettingsController@deletePayment']);

			Route::get('get/currencies', ['uses' => 'SettingsController@allCurrencies']);
			Route::post('store/currency', ['uses' => 'SettingsController@storeCurrency']);
			Route::post('update/{currency}/currency', ['uses' => 'SettingsController@updateCurrency']);
			Route::get('delete/{currency}/currency', ['uses' => 'SettingsController@deleteCurrency']);
		});

		/**
		 * Routes for Expense Logs Module
		 */
		Route::get('logs', ['uses' => 'SettingsController@logs', 'as' => 'expense.logs']);

		/**
		 * Routes for Expense Module
		 */
		Route::get('list-expense', ['uses' => 'ExpenseController@listExpense', 'as' => 'expense']);
		Route::get('create-expense', ['uses' => 'ExpenseController@addExpense', 'as' => 'expense.add']);
		Route::get('list-recurring-expense', ['uses' => 'ExpenseController@listRecurringExpense', 'as' => 'expense-recurring']);
		Route::get('create-recurring-expense', ['uses' => 'ExpenseController@addExpenseRecurring', 'as' => 'expense-recurring.add']);
		Route::post('expense/store', ['uses' => 'ExpenseController@storeExpense', 'as' => 'expense.store']);

		Route::get('expense/{expense}/edit', ['uses' => 'ExpenseController@editExpense', 'as' => 'expense.edit']);
		Route::post('expense/{expense}/update', ['uses' => 'ExpenseController@updateExpense', 'as' => 'expense.update']);

		Route::get('expense/{expense}/delete', ['uses' => 'ExpenseController@deleteExpense', 'as' => 'expense.delete']);

		/**
		 * Routes for Expense Reports Module
		 */
		Route::get('reports', ['uses' => 'ReportsController@index', 'as' => 'expense.reports']);
		Route::get('annual', ['uses' => 'ReportsController@annualReport', 'as' => 'expense.reports.annual']);
		Route::get('monthly', ['uses' => 'ReportsController@monthlyReport', 'as' => 'expense.reports.monthly']);
		Route::get('pie-chart/{slug}', ['uses' => 'ReportsController@pieChart', 'as' => 'expense.reports.pie-chart']);
		Route::get('date-range', ['uses' => 'ReportsController@dateRange', 'as' => 'expense.reports.date-range']);
	});
	/**
	 * END Routes for Expense Application
	 */


	/**
	 * Routes for Sytrix Application
	 */
	Route::group(['prefix' => 'sytrix-app', 'namespace' => 'Sytrix', 'middleware' => 'sytrix.app'], function(){

		/**
		* Project
		**/
		Route::get('projects', ['uses' => 'ProjectsController@index', 'as' => 'sytrix.projects']);
		Route::get('projects/create', ['uses' => 'ProjectsController@create', 'as' => 'sytrix.project.create']);
		Route::post('projects/store', ['uses' => 'ProjectsController@store', 'as' => 'sytrix.project.store']);
		Route::get('projects/{project}/edit', ['uses' => 'ProjectsController@edit', 'as' => 'sytrix.project.edit']);
		Route::post('projects/{project}/update', ['uses' => 'ProjectsController@update', 'as' => 'sytrix.project.update']);
		Route::get('projects/{project}/movetoarchive', ['uses' => 'ProjectsController@moveToArchive', 'as' => 'sytrix.project.movetoarchive']);
		Route::get('projects/archives', ['uses' => 'ProjectsController@archive', 'as' => 'sytrix.project.archive']);
		Route::get('projects/archives/{project}/restore', ['uses' => 'ProjectsController@restore', 'as' => 'sytrix.project.restore']);
		Route::get('projects/{project}/delete', ['uses' => 'ProjectsController@delete',  'as' => 'sytrix.project.delete']);
		// Assign Users to a Project
		Route::post('project/{project}/search-users', ['uses' => 'ProjectsController@searchUser']);
		Route::post('project/{project}/assign-user', ['uses' => 'ProjectsController@assignUser']);
		Route::post('project/{project}/remove-user', ['uses' => 'ProjectsController@removeUser']);

		/**
		* Project Categories
		**/
		Route::get('categories', ['uses' => 'SettingsController@categories', 'as' => 'sytrix.categories']);
		Route::get('categories/create', ['uses' => 'SettingsController@create', 'as' => 'sytrix.category.create']);
		Route::post('categories/store', ['uses' => 'SettingsController@store', 'as' => 'sytrix.category.store']);
		Route::get('categories/{projectcat}/edit', ['uses' => 'SettingsController@edit', 'as' => 'sytrix.category.edit']);
		Route::post('categories/{projectcat}/update', ['uses' => 'SettingsController@update', 'as' => 'sytrix.category.update']);
		Route::get('categories/{projectcat}/delete', ['uses' => 'SettingsController@delete', 'as' => 'sytrix.category.delete']);
		Route::get('categories/trash/', ['uses' => 'SettingsController@trash', 'as' => 'sytrix.category.trash']);
		Route::get('categories/{projectcat}/restore', ['uses' => 'SettingsController@restore', 'as' => 'sytrix.category.restore']);

		/**
		* Project Statuses
		**/ 
		Route::get('status', ['uses' => 'SettingsController@status', 'as' => 'sytrix.status']);
		Route::get('status/create', ['uses' => 'SettingsController@createStatus', 'as' => 'sytrix.status.create']);
		Route::post('status/store', ['uses' => 'SettingsController@storeStatus', 'as' => 'sytrix.status.store']);
		Route::get('status/{status}/edit', ['uses' => 'SettingsController@editStatus', 'as' => 'sytrix.status.edit']);
		Route::post('status/{status}/update', ['uses' => 'SettingsController@updateStatus', 'as' => 'sytrix.status.update']);
		Route::get('status/{status}/delete', ['uses' => 'SettingsController@deleteStatus', 'as' => 'sytrix.status.delete']);
		Route::get('status/trash', ['uses' => 'SettingsController@trashStatus', 'as' => 'sytrix.status.trash']);
		Route::get('status/{status}/restore', ['uses' => 'SettingsController@restoreStatus', 'as' => 'sytrix.status.restore']);
		Route::post('status/change-color', ['uses' => 'SettingsController@changeColorStatus']);		
		Route::get('status/sort', ['uses' => 'SettingsController@sortStatus', 'as' => 'sytrix.status.sort']);
		Route::post('status/update-sort', ['uses' => 'SettingsController@updateStatusSort']);

		/**
		* Cards
		**/
		Route::post('cards/create', ['uses' => 'CardsController@create']);
		Route::post('cards/{project}/update', ['uses' => 'CardsController@update']);
		Route::post('cards/{project}/update-sort', ['uses' => 'CardsController@sort']);
		Route::post('cards/{card}/archive', ['uses' => 'CardsController@archive', 'as' => 'sytrix.card.archive']);
		Route::get('cards/{project}/archive', ['uses' => 'CardsController@trash', 'as' => 'sytrix.card.trash']);
		Route::get('cards/{project}/{card}/restore', ['uses' => 'CardsController@restore', 'as' => 'sytrix.card.restore']);
		// Assign Users to specific tasks
		Route::post('card/{card}/assign-members', ['uses' => 'CardsController@assignMembers']);
		Route::post('card/{card}/remove-members', ['uses' => 'CardsController@removeMembers']);

		/**
		* Tasks
		**/
		Route::get('project/{project}/tasks', ['uses' => 'TasksController@index', 'as' => 'sytrix.project.tasks']);
		Route::post('tasks/create', ['uses' => 'TasksController@create']);
		Route::post('tasks/{task}/update', ['uses' => 'TasksController@update']);
		Route::post('tasks/{task}/delete', ['uses' => 'TasksController@delete', 'as' => 'sytrix.task.delete']);

		/**
		* Task Categories
		**/
		Route::get('tasks/categories', ['uses' => 'SettingsController@taskCategories', 'as' => 'sytrix.task-categories']);
		Route::get('tasks/categories/create', ['uses' => 'SettingsController@createTaskCategory', 'as' => 'sytrix.task-category.create']);
		Route::post('tasks/categories/store', ['uses' => 'SettingsController@storeTaskCategory', 'as' => 'sytrix.task-category.store']);
		Route::get('tasks/categories/{taskcategory}/edit', ['uses' => 'SettingsController@editTaskCategory', 'as' => 'sytrix.task-category.edit']);
		Route::post('tasks/categories/{taskcategory}/update', ['uses' => 'SettingsController@updateTaskCategory', 'as' => 'sytrix.task-category.update']);
		Route::get('tasks/categories/{taskcategory}/delete', ['uses' => 'SettingsController@deleteTaskCategory', 'as' => 'sytrix.task-category.delete']);
		Route::get('tasks/categories/trash', ['uses' => 'SettingsController@trashTaskCategory', 'as' => 'sytrix.task-category.trash']);
		Route::get('tasks/categories/{taskcategory}/restore', ['uses' => 'SettingsController@restoreTaskCategory', 'as' => 'sytrix.task-category.restore']);
		Route::get('tasks/categories/sort', ['uses' => 'SettingsController@sortTaskCategory', 'as' => 'sytrix.task-category.sort']);
		Route::post('tasks/categories/update-sort', ['uses' => 'SettingsController@updatesortTaskCategory']);

		/**
		* Clients
		**/
		Route::get('clients', ['uses' => 'ClientsController@index', 'as' => 'clients']);
		Route::get('clients/create', ['uses' => 'ClientsController@create', 'as' => 'client.create']);
		Route::post('clients/store', ['uses' => 'ClientsController@store', 'as' => 'client.store']);
		Route::get('clients/{client}/edit', ['uses' => 'ClientsController@edit', 'as' => 'client.edit']);
		Route::post('clients/{client}/update', ['uses' => 'ClientsController@update', 'as' => 'client.update']);
		Route::get('clients/{client}/delete', ['uses' => 'ClientsController@delete', 'as' => 'client.delete']);
		Route::get('clients/trash/', ['uses' => 'ClientsController@trash', 'as' => 'client.trash']);
		Route::get('clients/{client}/restore', ['uses' => 'ClientsController@restore', 'as' => 'client.restore']);
		//  Client Projects
		Route::get('client/{client}/projects', ['uses' => 'ProjectsController@clientProjects', 'as' => 'client.projects']);		

		/**
		* Time Tracks
		**/
		Route::get('time-tracks/create', ['uses' => 'TimeController@create', 'as' => 'sytrix.timetrack.create']);
		Route::post('time-tracks/get-projects', ['uses' => 'TimeController@timeTrackGetProjects']);
		Route::post('time-tracks/get-tasks', ['uses' => 'TimeController@timeTrackGetTasks']);
		Route::post('time-tracks/store', ['uses' => 'TimeController@timeTrackStore', 'as' => 'sytrix.timetrack.store']);
		Route::get('time-tracks/{timeTrack}/edit', ['uses' => 'TimeController@timeTrackEdit', 'as' => 'sytrix.timetrack.edit']);
		Route::post('time-tracks/{timeTrack}/update', ['uses' => 'TimeController@timeTrackUpdate', 'as' => 'sytrix.timetrack.update']);

		/**
		* Notifications
		**/
		Route::post('notifications/{notification}/notified', ['uses' => 'NotificationsController@notified']);

		/**
		* Reports
		**/	
		// TimeTrack Report
		Route::get('reports/time-tracks', ['uses' => 'ReportsController@timeTracks', 'as' => 'sytrix.reports.tracks']);
		Route::get('reports/time-tracks/manual', ['uses' => 'ReportsController@manualTimeTracks', 'as' => 'sytrix.reports.manual-tracks']);
		Route::get('reports/time-tracks/employee', ['uses' => 'ReportsController@employeeTimeTracks', 'as' => 'sytrix.reports.employee-tracks']);

		// Projects Report
		Route::get('reports/projects', ['uses' => 'ReportsController@projects', 'as' => 'sytrix.reports.projects']);

		// KPI
		Route::get('reports/kpi', ['uses' => 'ReportsController@kpi', 'as' => 'sytrix.reports.kpi']);

		// Leaderboard
		Route::get('reports/leaderboard', ['uses' => 'ReportsController@leaderboard', 'as' => 'sytrix.reports.leaderboard']);


	});
	/**
	 * END Routes for Sytrix Application
	 */

});

/**
* API
**/
Route::group(['prefix' => 'sytrix-app/api/', 'namespace' => 'Sytrix\Api'], function(){

	/**
	* Timein App
	**/
	//Login
	Route::post('time-nurse/login', ['uses' => 'TimeController@login']);
	
	// Main Func	
	Route::post('time-nurse/get-clients', ['uses' => 'TimeController@getClients']);
	Route::post('time-nurse/refresh-list', ['uses' => 'TimeController@refreshList']);
	Route::post('time-nurse/{user}/get-projects', ['uses' => 'TimeController@getProjects']);
	Route::get('time-nurse/{user}/{project}/get-cards', ['uses' => 'TimeController@getProjectCards']);
	Route::post('time-nurse/{user}/login-task', ['uses' => 'TimeController@loginTask']);
	Route::post('time-nurse/{user}/logout-task', ['uses' => 'TimeController@logoutTask']);	
	Route::post('time-nurse/{user}/view-details', ['uses' => 'TimeController@viewDetails']);
	Route::post('time-nurse/{time}/add-note', ['uses' => 'TimeController@addNote']);
	Route::post('time-nurse/updated-time', ['uses' => 'TimeController@getUpdatedHoursRendered']);
	Route::post('time-nurse/get-tracks', ['uses' => 'TimeController@getTimeTracks']);
	Route::post('time-nurse/{user}/update-current-task', ['uses' => 'TimeController@updateCurrentTask']);
	
});