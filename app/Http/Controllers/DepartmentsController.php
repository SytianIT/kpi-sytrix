<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

use App\Http\Requests;

class DepartmentsController extends Controller
{
    
	public function index( $trash = false ) {

		if( $trash == true ) {
			$departments = Department::onlyTrashed()->get();
		}
		else {
			$departments = Department::all();
		}

		return view('modules.departments.index', compact('departments', 'trash'));

	}

	public function create() {

		return view('modules.departments.create');
		
	}

	public function store(Request $request) {

		$this->validate($request, [
    		'title' => 'required|unique:departments'
    	]);

		$department = new Department;

		$department->title = $request->title;

		$department->save();

		return redirect()->route('departments')->withSuccess('Department saved.');
	}

	public function edit(Department $department){
		return view('modules.departments.edit', compact('department'));
	}

	public function update(Request $request, Department $department) {

		$this->validate($request, [
    		'title' => 'required|unique:departments,title,'.$department->id
    	]);
	
		$department->update($request->all());
		
		return redirect()->route('departments')->withSuccess('Department updated.');
	}

	public function delete(Department $department) {

		$department->delete();

		return redirect()->route('departments')->withStatus('Department deleted.');

	}

	public function restore($department) {	

		$department = Department::onlyTrashed()->where('id', $department)->first();

		$department->restore();

		return redirect()->route('departments')->withStatus('Department successfully stored');

	}

}
