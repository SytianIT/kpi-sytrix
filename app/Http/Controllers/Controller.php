<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * Method globally used to make a slug name and look for the existence of it, in model pass
     * @param String $value 
     * @param Integer $id    
     * @param Class $model 
     * @return String $slug
     */
    public function setSlugAttribute($value, $id, $model) {
		$slug = Str::slug($value);

		$slugs = $model->whereRaw("name REGEXP '^{$slug}(-[0-9]*)?$'")
					   ->where('id', '<>', $id);

		if ($slugs->count() === 0) {
			return $slug;
		}

		return $slug . '-' . $id;
	}

    /**
     * Function to call to upload basic image with parameters of optimizing image
     * @param  UploadedFile|null $image
     * @param  String            $path  
     * @param  Int            $width 
     * @param  Int            $height
     * @return String                   
     */
    public function baseUpload(UploadedFile $image = null, $path = null, $width = null, $height = null, $saveOriginal = true)
    {
        if ($image && $image->isValid()) {
            $validator = Validator::make(
                ['image' => $image],
                ['image' => 'required|image']
            );

            if ($validator->fails()) {
                return null;
            }

            $img = Image::make($image->getRealPath());

            // Save first the original image
            if($saveOriginal){
                $originalFileName = $this->makeFileNameWithCheckExistence($image, $path, false, 0, null, null);
                $uploadPath = public_path($path.'/'.$originalFileName);
                $img->save($uploadPath, $originalFileName);
            }

            // Save with different option of crop and rotate
            $fileName = $this->makeFileNameWithCheckExistence($image, $path, false, 0, $width, $height);
            $uploadPath = public_path($path.'/'.$fileName);
            if($width && $height) {
                $img->fit($width, $height)->save($uploadPath);
            } else if ($width) {
                $img->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadPath);
            } else if ($height) {
                $img->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadPath); 
            } else {
                $img->save($uploadPath, $fileName);
            }

            return $path.'/'.$fileName;
        }

        return null;
    }

    /**
     * Method use to check the file name existence in path provided, if exist will add incremental number after the name specified
     * @param  Image  $image   
     * @param  String  $path    
     * @param  boolean $repeat  
     * @param  integer $counter 
     * @param  integer  $width   
     * @param  integer  $height  
     * @return String           
     */
    public function makeFileNameWithCheckExistence($image, $path, $repeat = false, $counter = 0, $width, $height)
    {
        $originalName = $image->getClientOriginalName();
        $fileExtension = '.'.$image->getClientOriginalExtension();
        
        $slugName = str_slug(substr($originalName, 0, strpos($originalName, $fileExtension)));
        $rawName = $repeat ? $slugName.'-'.$counter : $slugName;
        if($width && $height){
            $rawName .= '-'.$width.'x'.$height;
        } else if($width){
            $rawName .= '-w'.$width;
        } else if($height){
            $rawName .= '-h'.$height;
        }

        $demandPath = public_path($path.'/'.$rawName.$fileExtension);
        $fileName = $rawName.$fileExtension;
        if(File::exists($demandPath)){
            $fileName = $this->makeFileNameWithCheckExistence($image, $path, true, $counter + 1, $width, $height);
        }

        return $fileName;
    }
}
