<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Role;
use App\Syion;
use Illuminate\Http\Request;

class RoleController extends Controller
{
	/**
	 * Display all role method
	 * @return view
	 */
    public function index()
    {
    	$roles = Role::orderBy('created_at', 'DESC')->where('id', '<>', 1)->get();

    	return view('modules.role.index', compact('roles'));
    }

    /**
     * Creation method for Role
     * @return view w/ var permissions
     */
    public function create()
    {
		$syions = Syion::with(['permissions' => function($q){
									$q->orderBy('created_at', 'ASC');
								}])->get();

    	return view('modules.role.create', compact('syions'));
    }

    /**
     * Store method to create Role
     * @param  Request $request
     * @return redirect 
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'title' => 'required'
    	]);

    	$role = new Role;
    	$role = $this->updateRoleRecord($request, $role);

    	return redirect()->route('roles')
    					 ->withSuccess("Role creation of '{$role->title}' was successfull");
    }

    /**
     * Edit method for Role records
     * @param  Role  $role
     * @return redirect       
     */
    public function edit(Role $role)
    {
    	$syions = Syion::with(['permissions' => function($q){
									$q->orderBy('created_at', 'ASC');
								}])->get();

    	$permissions = $role->permissions;
    	$currentPermissions = array_flatten($permissions->pluck('id'));

    	return view('modules.role.edit', compact('role','syions','currentPermissions'));
    }

    /**
     * Update method for Role records
     * @param  Request $request
     * @param  Role   $role  
     * @return redirect          
     */
    public function update(Request $request, Role $role)
    {
    	$this->validate($request, [
    		'title' => 'required'
    	]);

    	$role = $this->updateRoleRecord($request, $role);

    	return redirect()->back()
    					 ->withSuccess("Role creation of '{$role->title}' was successfull");
    }

    /**
     * Method to change records call by update and store
     * @param  Request $request
     * @param  Role $role
     * @return object
     */
    public function updateRoleRecord($request, $role)
    {
    	$slug = str_slug($request->title, '-');

    	$role->title = $request->title;
    	$role->description = $request->description;
    	$role->save();
    	$role->permissions()->sync($request->permissions);

    	$role->name = $this->setSlugAttribute($role->title, $role->id, $role);
    	$role->save();

    	return $role;
    }

    /**
     * Method to delete Role record
     * @param  Role  $role
     * @return redirect
     */
    public function delete(Role $role)
    {
    	$name = $role->title;
    	$role->delete();

    	return redirect()->route('roles')
    					 ->withSuccess("Deletion of Role '{$name}' was successfull");
    }
}
