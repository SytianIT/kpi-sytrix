<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords {
        getSendResetLinkEmailSuccessResponse as getSendResetLinkEmailSuccessResponseOverride;
        getSendResetLinkEmailFailureResponse as getSendResetLinkEmailFailureResponseOverride;
    }

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Check if email entered is in database : AJAX Call
     * @param  Request $request
     * @return JSON
     */
    public function validateEmail(Request $request)
    {
        $exist = User::where('email', '=', $request->email)->get();

        if($exist->count() > 0){
            return response()->json(true);
        }

        return response()->json('We cannot find an account associated with this email.');    
    }

    /**
     * Get the response for after the reset link has been successfully sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->json(['success' => 'true', 'message' => 'Password reset link has been sent to your email.'], 200);
    }

    /**
     * Get the response for after the reset link could not be sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailFailureResponse($response)
    {
        return redirect()->json(['success' => 'false', 'message' => 'There was an error in processing your request, please try again later.'], 400);
    }
}
