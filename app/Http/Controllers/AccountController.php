<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function profile()
    {
    	return view('modules.account.profile');
    }

    public function updateProfile(Request $request)
    {
    	$user = Auth::user();
    	$basicFields = [
    		'firstname' => 'required',
            'lastname' => 'required',
            'middlename' => 'required',
            'address' => 'required',
            'birthdayPicker_birthDay' => 'required|date',
            'mobile_number' => 'required',
            'username' => 'required|max:15|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
		];

    	if($request->has('password')){
    		$passwordFields = [
	            'password' => 'required|max:15|min:6|confirmed',
	            'password_confirmation' => 'required'
	        ];

	        $basicFields = array_merge($basicFields, $passwordFields);
        }

        $this->validate($request, $basicFields, [
        	'birthdayPicker_birthDay.required' => 'Your birthdate is required.',
            'firstname.required' => 'Your first name is required',
            'lastname.required' => 'Your last name is required',
            'middlename.required' => 'Your middle name is required',
        ]);

    	$birthdate = new Carbon($request->birthdayPicker_birthDay);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->middlename = $request->middlename;
        $user->address = $request->address;
        $user->birthdate = $birthdate;
        $user->mobile_number = $request->mobile_number;
        $user->age = $birthdate->diffInYears(Carbon::now());
        $user->job_title = $request->job_title;
        $user->department_id = 0; // To be change in the future
        $user->email = $request->email;
        $user->username = $request->username;

        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('image')){
            $user->user_picture = $this->baseUpload($request->file('image'), 'uploads/users/user-pictures', 350, 350);
        }
        $user->save();

        return redirect()->back()
        				 ->withSuccess('Account details updated successfully.');
    }
}
