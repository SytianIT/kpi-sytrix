<?php

namespace App\Http\Controllers\Expense\Api;

use App\Syion;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Applications\Expense\Currency;
use App\Applications\Expense\Expense;
use App\Applications\Expense\Payment;
use App\Http\Controllers\Controller;
use App\Applications\Expense\ExpenseCategory;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function allPayments()
    {
        $payments = Payment::get()->toArray();
        foreach($payments as $key => $payment){
            $expense = Expense::where('payment_id', '=', $payment['id'])->get();

            if($expense->count() > 0){
                $message = '<ul class="mrg5T text-left">';
                foreach($expense as $item){
                    $url = route('expense.edit', $item->id);
                    $message .= "<li><a href='{$url}'>Entry: {$item->title}</a></li>";
                };
                $message .= '</ul>';

                $payment['hasExpenses'] = true;
                $payment['countExpenses'] = $expense->count();
                $payment['messageExpense'] = $message;
            }
            $payments[$key] = $payment;
        }

        return response()->json($payments);
    }

    public function storePayment(Request $request)
    {
        $payment = new Payment;
        $payment->title = $request->title;
        $payment->save();
        $payment->insertAuditLog('New payment entitled "'.$payment->title.'" created.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return $this->allPayments();
    }

    public function updatePayment(Request $request, Payment $payment)
    {
        $payment->title = $request->title;
        $payment->save();
        $payment->insertAuditLog('Payment details entitled "'.$payment->title.'" updated.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return $this->allPayments();
    }

    public function deletePayment(Payment $payment)
    {
        $title = $payment->title;
        $payment->delete();
        $payment->insertAuditLog('Payment entitled "'.$title.'" was deleted.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return $this->allPayments();
    }

    public function allCurrencies()
    {
        $currencies = Currency::get()->toArray();
        foreach($currencies as $key => $currency){
            $expense = Expense::where('currency_id', '=', $currency['id'])->get();
            
            if($expense->count() > 0){
                $message = '<ul class="mrg5T text-left">';
                foreach($expense as $item){
                    $url = route('expense.edit', $item->id);
                    $message .= "<li><a href='{$url}'>Entry: {$item->title}</a></li>";
                };
                $message .= '</ul>';

                $currency['hasExpenses'] = true;
                $currency['countExpenses'] = $expense->count();
                $currency['messageExpenses'] = $message;
            }
            $currencies[$key] = $currency;
        }

        return response()->json($currencies);
    }

    public function storeCurrency(Request $request)
    {
        $currency = new Currency;
        $currency->prefix = $request->prefix;
        $currency->title = $request->title;
        $currency->conversion_rate = $request->conversion_rate;
        $currency->save();
        $currency->insertAuditLog('New currency entitled "'.$currency->title.'" created.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return $this->allCurrencies();
    }

    public function updateCurrency(Request $request, Currency $currency)
    {
        $currency->prefix = $request->prefix;
        $currency->title = $request->title;
        $currency->conversion_rate = $request->conversion_rate;
        $currency->save();
        $currency->insertAuditLog('Currency details entitled "'.$currency->title.'" updated.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return $this->allCurrencies();
    }

    public function deleteCurrency(Currency $currency)
    {
        $title = $currency->title;
        $currency->delete();
        $currency->insertAuditLog('Currency entitled "'.$currency->title.'" was deleted.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return $this->allCurrencies();
    }
}
