<?php

namespace App\Http\Controllers\Expense;

use App\Applications\Expense\Currency;
use App\Applications\Expense\Expense;
use App\Applications\Expense\ExpenseCategory;
use App\Applications\Expense\Payment;
use App\Applications\Expense\Recurring;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Syion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ExpenseController extends Controller
{
    public function listExpense(Request $request = null)
    {
        // foreach($mod_expenses as $item){
        //     $item = (object) $item;

        //     $currency = Currency::find(1);
        //     $category = ExpenseCategory::where('title', '=', $item->name)->first();
        //     $cleanAmount = preg_replace("/[^0-9.]/", "", $item->total);

        //     $expense = new Expense;
        //     $expense->id = $item->id;
        //     $expense->title = $item->description;
        //     $expense->amount = floatval($cleanAmount);
        //     $expense->amount_in_php = floatval($cleanAmount);
        //     $expense->date_occured = new Carbon($item->datepaid);
        //     $expense->main_recurring_id = $item->futureexpense_id;
        //     $expense->currency()->associate($currency->id);
        //     $expense->category()->associate($category != null ? $category->id : 1);
        //     $expense->payment()->associate(1);
        //     $expense->user()->associate(Auth::user());
        //     $expense->save();
        // };
        $query = new Expense;

        if($request != null){
            $query = $query->filterExpense($request, $query);
            $filterDate = [
                'filter_daterange' => $request->filter_daterange,
                'filter_payment_method' => $request->filter_payment_method,
                'filter_category' => $request->filter_category,
            ];
        }
        $expenses = $query->recurrings(false)->orderBy('id', 'DESC')->paginate(50);

        return view('expense-app.expense.index-expense', compact('expenses','filterDate'));
    }

    public function addExpense()
    {
    	return view('expense-app.expense.create');
    }

    public static function listRecurringExpense(Request $request = null)
    {
        $query = new Expense;

        if($request != null){
            $query = Expense::filterExpense($request, $query, true);
            $filterDate = [
                'filter_daterange' => $request->filter_daterange,
                'filter_payment_method' => $request->filter_payment_method,
                'filter_category' => $request->filter_category,
            ];
        }
        $expenses = $query->recurrings()->orderBy('id', 'DESC')->paginate(50);

        return view('expense-app.expense.index-recurring-expense', compact('expenses','filterDate'));
    }

    public function addExpenseRecurring()
    {
    	$isRecurring = true;

    	return view('expense-app.expense.create', compact('isRecurring'));
    }

    public function storeExpense(Request $request)
    {
    	$this->validateExpense($request, $request->has('is_recurring'));

        $expense = new Expense;
        $expense = $this->fillBasicExpenseData($expense, $request);
        $expense->save();
        $startText = $request->has('is_recurring') ? 'recurring' : '';
        $expense->insertAuditLog('New '.$startText.' expense entitled "'.$expense->title.'" created.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

    	return redirect()->back()
    					 ->withSuccess('Creation successfull!');
    }

    public function editExpense(Expense $expense)
    {
        return view('expense-app.expense.edit', compact('expense'));
    }

    public function updateExpense(Request $request, Expense $expense)
    {
        $this->validateExpense($request, $request->has('is_recurring'));

        $expense = $this->fillBasicExpenseData($expense, $request);
        $expense->save();
        $startText = $request->has('is_recurring') ? 'Recurring expense' : 'Expense';
        $expense->insertAuditLog($startText.' entitled "'.$expense->title.'" updated.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return redirect()->back()
                         ->withSuccess("Updated successfully!");
    }

    public function deleteExpense(Expense $expense)
    {
        $title = $expense->title;
        $expense->delete();
        $expense->insertAuditLog('Expense entitled "'.$title.'" was deleted.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return redirect()->route('expense')
                         ->withWarning("Deleted successfully!");
    }

    private function fillBasicExpenseData($expense, $request)
    {
        $cleanAmount = preg_replace("/[^0-9.]/", "", $request->amount);
        $expense->title = $request->title;
        $expense->description = $request->description;
        $expense->amount = floatval($cleanAmount);
        $expense->date_occured = new Carbon($request->date_occured);
        $expense->currency()->associate($request->currency);
        $expense->category()->associate($request->category);
        $expense->payment()->associate($request->payment);
        $expense->user()->associate(Auth::user());

        $cleanAmounInPhp = $request->amount;
        if($request->currency != 1){
            $currencyObj = Currency::find($request->currency);
            if($currencyObj){
                $generatedAmount = $request->amount * $currencyObj->conversion_rate;
                $cleanAmounInPhp = $generatedAmount;
            }
        }
        $cleanAmounInPhp = preg_replace("/[^0-9.]/", "", $cleanAmounInPhp);
        $expense->amount_in_php = floatval($cleanAmounInPhp);

        if($request->has('is_recurring')){
            if($expense->recurring_id == null){
                $recurring = new Recurring;
            } else {
                $recurring = $expense->recurring;
            }

            $recurring->status = ($request->has('status')) ? $request->status : Expense::ACTIVE;
            if($this->checkChangesInRecurring($request, $expense)){
                $nextRecurring = $recurring->generateNextExpenseRecurring($request->date_occured, $request->recur_every, $request->recur_of, $request->recur_times);
                $recurring->next_expense_recurring = $nextRecurring->format('Y-m-d');
            }
            $recurring->recurring_every = $request->recur_every;
            $recurring->recurring_of = $request->recur_of;
            $recurring->recurring_times = $request->recur_times;
            $recurring->save();
            $expense->recurring_id = $recurring->id;
        }

        if($request->has('attachments_count') && $request->attachments_count > 0){
            $attachments = [];
            for($i = 1; $i <= $request->attachments_count; $i++){
                $this->validate($request, [
                    'attachment_'.$i => 'required|image|max:5000'
                ]);

                $path = 'uploads/expense/attachments';
                $data = $this->baseUpload($request->file('attachment_'.$i), $path);
                $attachments[] = $data;
            }
            $expense->attachments = $expense->attachments != null ? array_merge($expense->attachments, $attachments) : $attachments;
        }

        if($request->has('for_deletion')){
            $attachments = $expense->attachments;
            $path = explode(', ', $request->for_deletion);
            foreach($path as $item){
                $key = array_search($item, $attachments);
                unset($attachments[$key]);
            }
            $expense->attachments = $attachments;
        }

        return $expense;
    }

    private function validateExpense($request, $recurring = null)
    {
    	$data = [
            'title' => 'required',
    		'category' => 'required',
    		'currency' => 'required',
    		'amount' => 'required|numeric',
    		'payment' => 'required',
    		'date_occured' => 'required',
    	];

        if($recurring){
            $add = [
                'recur_every' => 'required|numeric',
                'recur_of' => 'required',
                'recur_times' => 'required|numeric'
            ];

            $data = array_merge($data, $add);
        }

        $this->validate($request, $data, [
            'title.required' => 'Please provide name of the expense',
            'category.required' => 'Please choose the category of the expense.',
            'currency.required' => 'Please provide the currency of the expense.',
            'amount.required' => 'Please enter the amount acquired on the expense.',
            'payment.required' => 'Please choose the payment use in the expense.',
            'date_occured.required' => 'Please pick the date the expense happen.',
            'recur_every.required' => 'Please provide value for recurring settings recur every.',
            'recur_of.required' => 'Please provide value for recurring settings recur every selection.',
            'recur_times.required' => 'Please provide value for recurring settings recur times.'
        ]);
    }

    private function checkChangesInRecurring($request, $expense)
    {
        if($expense->recurring_every != null && $expense->recurring_of != null){
            if($expense->recurring_every != $request->recur_every || $expense->recurring_of != $request->recur_of){
                return true;
            } else {
                return false;
            }
        }

        return true;
    }
}
