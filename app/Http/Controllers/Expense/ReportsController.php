<?php

namespace App\Http\Controllers\Expense;

use App\Applications\Expense\Expense;
use App\Applications\Expense\ExpenseCategory;
use App\Applications\Expense\Payment;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsController extends Controller
{   
    /**
     * Method to open annual report page and get the current year needed for the display.
     * @param  Request|null $request 
     * @return View                
     */
    public function annualReport(Request $request = null)
    {
        $currentYear = $request->has('year') ? $request->year : Carbon::now()->year;
        $lastYear = $request->has('year') ? $request->year - 1 : Carbon::now()->subYear()->year;
        $nextYear = $request->has('year') ? $request->year + 1 : Carbon::now()->addYear()->year;

        $curExpenses = $this->getThisYear($currentYear);
        $lastExpenses = $this->getThisYear($lastYear);
        $nextExpenses = $this->getThisYear($nextYear);

        return view('expense-app.reports.annual', compact('currentYear','curExpenses','lastYear','lastExpenses','nextExpenses','nextYear'));
    }

    /**
     * Function to separate expense date into months and the total amount occured in that month.
     * @param  Integer $year 
     * @return Array
     */
    private function getThisYear($year)
    {
    	$firstDayThisYear = new Carbon('first day of january '.$year);
        $lastDayThisYear = new Carbon('last day of december '.$year);

        $expenses = Expense::recurrings(false)->whereBetween('date_occured', [$firstDayThisYear, $lastDayThisYear])->orderBy('date_occured', 'ASC')->get();

        $data = ["January" => 0,"February" => 0,"March" => 0,"April" => 0,"May" => 0,"June" => 0,"July" => 0,"August" => 0,"September" => 0,"October" => 0,"November" => 0,"December" => 0];
        if($expenses->count() > 0){
        	
        	foreach($expenses as $expense){
        		if(isset($data[$expense->date_occured->format('F')])){
        			$data[$expense->date_occured->format('F')] = $data[$expense->date_occured->format('F')] + $expense->amount_in_php;
        		} else {
        			$data[$expense->date_occured->format('F')] = floatval($expense->amount_in_php);
        		}
        	}

        	return $data;
        }

        return $data;
    }

    /**
     * Method to generate and display report monthly
     * @param  Request|null $request 
     * @return View                
     */
    public function monthlyReport(Request $request = null)
    {
        $year = $request->has('year') ? $request->year : Carbon::now()->year;
        $currentMonth = $request->has('month') ? $request->month : Carbon::now()->month;
        $intLastMonth = $request->has('month') ? $request->month - 1 : Carbon::now()->subMonth()->month;
        $intNextMonth = $request->has('month') ? $request->month + 1 : Carbon::now()->addMonth()->month;

        $curExpenses = $this->getThisMonth($currentMonth, $year);
        $lastMonth = Carbon::createFromDate($year, $intLastMonth);
        $nextMonth = Carbon::createFromDate($year, $intNextMonth);
        $firstExpense = $curExpenses->first();

        if($intLastMonth == 0){
            $intLastMonth = 12;
        }
        if($intNextMonth == 13){
            $intNextMonth = 1;
        }

        $yearsList = [];
        $yearsList =  $this->getYearList(Carbon::now()->addYears(5)->year, $yearsList, 'increment');
        $yearsList =  $this->getYearList(Carbon::now()->subYears(5)->year, $yearsList, 'decrement');

        $monthsList = [1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December"];

        $totalExpense = $curExpenses->sum('amount_in_php');

        return view('expense-app.reports.monthly', compact('curExpenses','intLastMonth','lastMonth','intNextMonth','nextMonth','firstExpense','yearsList','monthsList','totalExpense'));
    }

    /**
     * Function to call to retrieve expense record base on month pass.
     * @param  String $month 
     * @param  Integer $year  
     * @return Collection        
     */
    public function getThisMonth($month, $year)
    {
        $month = Carbon::createFromDate($year, $month)->format('F');
        
        $firstDayThisMonth = new Carbon('first day of '.$month.' '.$year);
        $lastDayThisMonth = new Carbon('last day of '.$month.' '.$year);

        $expenses = Expense::recurrings(false)->whereBetween('date_occured', [$firstDayThisMonth, $lastDayThisMonth])->orderBy('date_occured', 'ASC')->get();

        return $expenses;
    } 

    /**
     * Function to call to get all years applicablt
     * @param  Integer $yearCap   
     * @param  Integer $yearList  
     * @param  String $direction 
     * @return Array            
     */
    private function getYearList($yearCap, $yearList, $direction)
    {
        $currentYear = Carbon::now()->year;
        if($direction == 'increment'){
            for($i = $currentYear; $i <= $yearCap; $i++){
                array_push($yearList, $i);
            }
        } else {
            for($i = $currentYear; $i >= $yearCap; $i--){
                array_push($yearList, $i);
            }
        }
        
        return $yearList;
    }

    /**
     * Method for generate and display report of expense by category in Pie Chart
     * @param  Request|null $request 
     * @param  String       $slug    
     * @return View                
     */
    public function pieChart(Request $request = null, $slug)
    {
        $startDay = new Carbon('first day of this month');
        $endDay = new Carbon('last day of this month');

        $query = Expense::recurrings(false)->orderBy('date_occured', 'ASC');

        if($request->has('filter_daterange')){
            $range = $request->filter_daterang;
            $dates = explode(' - ', $request->filter_daterange);
            if(count($dates) != 2){
                return redirect()->back()
                                 ->withWarning('There is an error in filter request.');
            }

            $startDay = new Carbon($dates[0]);
            $endDay = new Carbon($dates[1]);
        }

        $expenses = $query->whereBetween('date_occured', [$startDay, $endDay])->with('category')->get();
        

        if($slug == 'category'){
            $title = 'Category';
            $itemList = $this->getThisExpenseByCategory($expenses);
        } else if($slug == 'payment-mode'){
            $title = 'Payment Mode';
            $itemList = $this->getThisExpenseByPaymentMode($expenses);
        } else {
            return redirect()->back()->withWarning('There was a problem in redirecting URL, please try again later or ask the administrator.');
        }

        return view('expense-app.reports.pie-chart', compact('itemList','title','range','startDay','endDay'));
    }

    /**
     * Method to use to get the category reports base on expense parameter
     * @param  Collection $expenses 
     * @return Array           
     */
    private function getThisExpenseByCategory($expenses)
    {
        $finalList = [];
        $categories = ExpenseCategory::orderBy('title', 'ASC')->get();
        
        foreach($categories as $category){
            $catExpense = $expenses->filter(function($q) use($category){
                if($category->id == $q->category->id){
                    return $q;
                }
            });

            $finalList = $this->makeTheFinalListArray($category, $catExpense, $finalList);
        }

        return $finalList;
    }

    /**
     * Method to use to get the payment mode reports base on expense parameter
     * @param  [type] $expenses [description]
     * @return [type]           [description]
     */
    private function getThisExpenseByPaymentMode($expenses)
    {
        $finalList = [];
        $modes = Payment::get();

        foreach($modes as $mode){
            $modeExpense = $expenses->filter(function($q) use($mode){
                if($mode->id == $q->payment->id){
                    return $q;
                }
            });

            $finalList = $this->makeTheFinalListArray($mode, $modeExpense, $finalList);
        }

        return $finalList;
    }

    /**
     * Method to call to add entry in fina list of array items
     * @param  Object $obj     
     * @param  Collection $expense 
     * @param  Array $list    
     * @return Array          
     */
    private function makeTheFinalListArray($obj, $expense, $list)
    {
        $list[$obj->id]['title'] = $obj->title;
        $list[$obj->id]['expense_count'] = $expense->count();
        $list[$obj->id]['expense_total'] = $expense->sum('amount_in_php');

        return $list;
    }

    public function dateRange(Request $request = null)
    {
        $startDay = new Carbon('first day of this month');
        $endDay = new Carbon('last day of this month');

        $query = new Expense;
        $query = $query->filterExpense($request, $query);
        
        if(!$request->has('filter_daterange')){
            $query = $query->whereBetween('date_occured', [$startDay->format('Y-m-d'),$endDay->format('Y-m-d')]);
            $request->filter_daterange = $startDay->format('m/d/Y').' - '.$endDay->format('m/d/Y');
        }
        $expenses = $query->recurrings(false)->orderBy('date_occured', 'DESC')->get();

        $filterDate = [
            'filter_daterange' => $request->filter_daterange,
        ];

        return view('expense-app.reports.date-range', compact('expenses','filterDate'));
    }
}
