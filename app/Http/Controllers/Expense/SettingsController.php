<?php

namespace App\Http\Controllers\Expense;

use App\Applications\Expense\ExpenseCategory;
use App\AuditLog;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Syion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    /**
     * [categories description]
     * @return [type] [description]
     */
    public function categories()
    {
    	$categories = ExpenseCategory::getParentNested();
        $categoryList = ExpenseCategory::getParentNested();

    	return view('expense-app.category.index', compact('categories','categoryList'));
    }

    public function validateCategoryDetails(Request $request)
    {
        if($request->has('category')){
            $existing = ExpenseCategory::where('title', '=', $request->title)->where('id', '<>', $request->category)->get();
        } else {   
            $existing = ExpenseCategory::where('title', '=', $request->title)->get();
        }

        if($existing->count() > 0){
            return response()->json('Title already exist, please provide different one.'); 
        }

        return response()->json(true);
    }

    public function storeCategory(Request $request)
    {
        $category = new ExpenseCategory;
        $category->title = $request->title;
        if($request->has('parent_id')){
            $category->parent_id = $request->parent_id;
        }
        $category->save();
        $category->insertAuditLog('New expense category entitled "'.$category->title.'" created.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return redirect()->back()
                         ->withSuccess('Creation Successfull.');
    }

    public function editCategory(ExpenseCategory $category)
    {
        $categoryList = ExpenseCategory::getParentNested();
        $categoryList = $categoryList->filter(function($value, $key) use ($category){
                                        if($value->id != $category->id){
                                            return true;
                                        }
                                    });

        return view('expense-app.category.ajax-edit-category', compact('category','categoryList'));
    }

    public function updateCategory(Request $request, ExpenseCategory $category)
    {
        $category->title = $request->title;
        if($request->has('parent_id')){
            $category->parent_id = $request->parent_id;
        }
        $category->save();
        $category->insertAuditLog('Expense category details entitled "'.$category->title.'" updated.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return redirect()->back()
                         ->withSuccess('Update Successfull.');
    }

    public function deleteCategory(ExpenseCategory $category)
    {
        $name = $category->title;
        $category->delete();
        $category->insertAuditLog('Expense category entitled "'.$name.'" was deleted.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

        return redirect()->back()
                         ->withWarning('Delete Successful.');
    }

    /**
     * Methods for Payments
     */
    public function payments()
    {
        return view('expense-app.payment.index');
    }

    /**
     * Methods for Payments
     */
    public function currencies()
    {
        return view('expense-app.currency.index');   
    }

    /**
     * Methods for Logs
     */
    public function logs(Request $request = null)
    {
        $query = AuditLog::thisApp(Syion::EXPENSE)->orderBy('created_at', 'DESC');

        if($request->has('filter_daterange')){
            $dates = explode(' - ', $request->filter_daterange);
            if(count($dates) != 2){
                return redirect()->back()
                                 ->withWarning('There is an error in filter request.');
            }
            $fromDates = new Carbon($dates[0]);
            $toDates = new Carbon($dates[1]);

            // $query = $query->whereBetween('created_at', [$fromDates->format('Y-m-d'), $toDates->format('Y-m-d').' 99:99:99']);
            $query = $query->where('created_at', '>=', $fromDates->format('Y-m-d'))
                           ->where('created_at', '<=', $toDates->format('Y-m-d').'99:99:99');
        }

        if($request->has('filter_user_name')){
            $strings = explode('-', str_slug($request->filter_user_name, $separator = "-"));
            foreach($strings as $string){
                $query = $query->whereHas('user', function($q) use($string){
                                            $q->orWhere('firstname', 'LIKE', "%{$string}%")
                                              ->orWhere('lastname', 'LIKE', "%{$string}%");
                                        });
            }
        }

        if($request->has('filter_activity')){
            $strings = explode('-', str_slug($request->filter_activity, $separator = "-"));
            foreach($strings as $string){
                $query = $query->where('activity_desc', 'LIKE', "%{$string}%");
            }
        }

        if($request != null){
            $filters = [
                'filter_daterange' => $request->filter_daterange,
                'filter_user_name' => $request->filter_user_name,
                'filter_activity' => $request->filter_activity,
            ];
        }

        $logs = $query->paginate(50);
        return view('expense-app.logs.index', compact('logs','filters'));
    }
}
