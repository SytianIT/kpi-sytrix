<?php

namespace App\Http\Controllers\Sytrix;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Project;
use App\User;

class CardsController extends Controller
{

	public function create() {

	}

	public function update(Request $request, Project $project) {

		if( $request->card_id != null ) {
			$card = $project->cards()->where('id', $request->card_id)->first();

			$card->slug = str_slug( $project->name . "-" . $request->card_title );
			$card->title = $request->card_title;
			$card->save();
		}
		else {
			$card = new Card;

			$card->slug = str_slug( $project->name . "-" . $request->card_title );
			$card->title = $request->card_title;
			$card->project_id = $project->id;

			// get the max order
			$maxOrder = Card::where('project_id', $project->id)->max('order');
			$card->order = $maxOrder + 1;
			
			$card->save();
		}


		return response()->json( $card );

	}

	public function sort(Project $project, Request $request) {

		foreach( $request->sort_val as $sortKey => $sortVal ) {
			$card = Card::find($sortVal);
			$card->order = $sortKey + 1;
			$card->save();
		}

	}

	public function archive(Card $card) {
		$card->delete();
	}	

	public function assignMembers(Card $card, Request $request) {
		if( !$card->users->contains($request->member_id) ) {
			$card->users()->attach($request->member_id);
		}

		$cardMembers = $card->users()->get();

		return response()->json( $cardMembers );
	}

	public function removeMembers(Card $card, Request $request) {
		$card->users()->detach($request->member_id);

		$cardMembers = $card->users()->get();
		return response()->json( $cardMembers );
	}

	public function trash(Project $project) {	

		$cards = $project->cards()->onlyTrashed()->get();

		$selectedUsers = $project->users;
		$users = User::where('id', '!=', 1);

		return view('sytrix-app.task.archive', compact('cards', 'project', 'selectedUsers', 'users'));

	}

	public function restore(Project $project, $cardID) {

		$card = Card::onlyTrashed()->where('id', $cardID);
		$card->restore();

		$cards = Card::onlyTrashed()->where('project_id', $project->id)->get();

		return view('sytrix-app.task.archive', compact('project', 'cards'))->withSuccess('Card restored.');

	}

}
