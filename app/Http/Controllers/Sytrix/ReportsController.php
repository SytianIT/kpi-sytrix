<?php

namespace App\Http\Controllers\Sytrix;

use Illuminate\Http\Request;

use App\Applications\Sytrix\TimeTrack;
use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\ProjectStatus;
use App\User;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ReportsController extends Controller
{
    
    /**
    * Time Tracks Report
    **/
	public function timeTracks(Request $request) {		
		if( $request->track_range || $request->track_employee ) {	
			$query = new TimeTrack;

			if( $request->track_employee ) {
				$query = $query->where('user_id', $request->track_employee);				
			}

			if( $request->track_range ) {
				$trackRange = explode('-', $request->track_range);	

				$fromDate = new Carbon($trackRange[0]);
				$toDate = new Carbon($trackRange[1]);

				$query = $query->whereDate('date', '>=', $fromDate->format('Y-m-d'))
							   ->whereDate('date', '<=', $toDate->format('Y-m-d'));

			}

			$timeTracks = $query->orderBy('id', 'desc');

		}
		else {
			$timeTracks = TimeTrack::orderBy('id', 'desc');
		}

		$timeTracks = $timeTracks->with(['project.client'])->paginate(20);

		return view('sytrix-app.reports.time-tracks.index', compact('timeTracks'));		
	}

	public function manualTimeTracks(Request $request) {

		if( $request->track_range || $request->track_employee ) {	
			$query = new TimeTrack;

			if( $request->track_employee ) {
				$query = $query->where('user_id', $request->track_employee);				
			}

			if( $request->track_range ) {
				$trackRange = explode('-', $request->track_range);	

				$fromDate = new Carbon($trackRange[0]);
				$toDate = new Carbon($trackRange[1]);

				$query = $query->whereDate('date', '>=', $fromDate->format('Y-m-d'))
							   ->whereDate('date', '<=', $toDate->format('Y-m-d'));

			}

			$timeTracks = $query->where('manual_entry', '!=', null)->orderBy('id', 'desc')->paginate(20);

		}
		else {
			$timeTracks = TimeTrack::where('manual_entry', '!=', null)->orderBy('id', 'desc')->paginate(20);
		}

		return view('sytrix-app.reports.time-tracks.manual-tracks', compact('timeTracks'));
	}

	public function employeeTimeTracks(Request $request) {

		if( $request->track_range || $request->track_employee ) {

			$query = new User;

			$query = $query->with(['timeTracks' => function($dateQuery) use  ($request) {
				if( $request->track_range ) {

					$trackRange = explode('-', $request->track_range);

					$fromDate = new Carbon($trackRange[0]);
					$toDate = new Carbon($trackRange[1]);

					$dateQuery->whereDate('date', '>=', $fromDate->format('Y-m-d'))
						  ->whereDate('date', '<=', $toDate->format('Y-m-d'));
				}
			}]);

			if( $request->track_employee ) {
				$query = $query->where('id', $request->track_employee);
			}

			$emptts = $query->get();
		}
		else {
			$emptts = User::where('id', '!=' , 1)->with(['timeTracks'])->get();
		}

		$usersTracks = [];
		foreach( $emptts as $user ) {

			// push 
			if( !in_array($user->firstname, $usersTracks) ) {
				$usersTracks[$user->firstname] = [];
			}

			if( $user->timeTracks->count() > 0 ) {
				// set dates array for user
				$datesArr = [];

				foreach( $user->timeTracks->sortByDesc('id') as $timeTrack ) {
					if( !in_array($timeTrack->date, $datesArr) ) {
						// get the rendered hours per day
						$thisDate = new Carbon($timeTrack->date);
						$hours = TimeTrack::where('user_id', $user->id)->whereDate('date', '=', $thisDate->format('Y-m-d'))->sum('hours_rendered');

						$datesArr[$timeTrack->date] = $hours;
					}
				}

				// then push
				if( array_key_exists($user->firstname, $usersTracks) ) {
					$usersTracks[$user->firstname] = $datesArr;
				}

			}

		}

		return view('sytrix-app.reports.time-tracks.employee-tracks', compact('usersTracks'));

	}

	/**
    * Projects Reports
    **/
    public function projects(Request $request) {

    	$query = Project::where('id', '!=', 82);    	

    	// Project name/client
		if( $request->project_search && $request->project_search != '' ) {
			$query = $query->where('name', 'like', '%'.$request->project_search.'%')
						   ->orWhereHas('client', function($theClient) use($request) {
						   		$theClient->where('company_name', 'like', '%'.$request->project_search.'%');
						   });
		}

		// Project Status
		if( $request->project_status ) {
			if( $request->project_status != 0 ) {			
				$query = $query->whereHas('status', function($theStatus) use($request){
					$theStatus->where('id', $request->project_status);
				});
			}
		}			

		$projects = $query->orderBy('id', 'desc')->get();

		// Hours Quota
		if( $request->project_quota ) {

			$projects = $projects->filter(function($project) use($request) {

	    		$devTime = (int) $project->max_development_time; 
	    		$desTime = (int) $project->max_design_time; 
	    		$pmTime = (int) $project->max_pm_time; 

	    		$estimatedHours = $devTime + $desTime + $pmTime;

	    		$hoursRendered = $project->totalHours();

	    		// filter for on time projects
				if( $request->project_quota == 'met' ) {
					return $estimatedHours > $hoursRendered; 
				}
		    	// Filter for delayed projects		    	
				else {				
		    		return $estimatedHours < $hoursRendered;
				}

	    	});

		}

		// Project Due
		if( $request->project_due ) {

	    	$projects = $projects->filter(function($project) use($request){	

				$currentDate = Carbon::now();
				$dueDate = new Carbon($project->due_date);

		    	// Filter for on time projects
				if( $request->project_due == 'on-time' ) {
					if( $project->status->name == 'completed' ) {
						return $project;			    		
					}
					else {
						return $currentDate < $dueDate;			    								
					}
				}
		    	// Fiilter for delayed projects
				else {
					if( $project->status->name != 'completed' ) {
						return $currentDate > $dueDate;	
					}				
				}

    		});

		}

		if( $request->project_user ) {

			$projects = $projects->filter(function($project) use($request){
				
				$cards = $project->cards;
				$usersArr = [];
				
				foreach( $cards as $card ) {
					if( $card->users()->count() > 0 ) {
						
						// include in user array
						foreach( $card->users as $user ) {
							if( !in_array($user->id, $usersArr) ) {
								$usersArr[] = $user->id;
							}
						}

					}
				}

				return in_array($request->project_user, $usersArr);
			});
		}

    	$projectStatuses = ProjectStatus::roots()->get();

    	$temp = $projects;
    	$currentPage = $request->page;
    	$perPage = 20;
		$currentIndex = (($currentPage == 1 || $currentPage == 0) ? 0 : $currentPage - 1) * $perPage;
		$slice = $temp->slice($currentIndex, $perPage);

		$projects = new Paginator($slice, count($temp), $perPage, $currentPage);

		$projects->setPath( route('sytrix.reports.projects') );

    	return view('sytrix-app.reports.projects.index', compact('projects', 'projectStatuses'));

    }

    public function kpi() {

    	return view('sytrix-app.reports.kpi', compact('users'));

    }

    public function leaderboard() {

    	$users = User::where('job_title', 'like', '%developer%')->get();

    	$users = $users->sortByDesc(function($user){
    		return $user->rankPoints();
    	});

    	return view('sytrix-app.reports.leaderboard.index', compact('users'));
    }

}
