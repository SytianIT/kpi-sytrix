<?php

namespace App\Http\Controllers\Sytrix;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\TimeTrack;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\Client;
use App\Department;
use App\User;
use Carbon\Carbon;

class TimeController extends Controller
{
    
	// Manual Adding of time track
	public function create() {
		$departments = Department::with(['users'])->get();
		$clients = Client::all();

		return view('sytrix-app.time-track.create', compact('departments', 'clients'));
	}

	public function timeTrackGetProjects(Request $request) {

		$user = User::find($request->userid);

		$projects = $user->projects->filter(function($project) use($request) {
			return $project->client_id === $request->clientid;
		});

		$projectsArray = [];
		if( $projects->count() > 0 ) {
			foreach( $projects as $project ) {
				$projectsArray[] = $project;
			}
		}

		return response()->json($projectsArray);
	}

	public function timeTrackGetTasks(Request $request) {

		$project = Project::find($request->projectid);

		$cards = $project->cards()->whereHas('users', function($query) use($request){
			$query->where('id', $request->userid);
		})->with('tasks')->get();

		$cardsArray = [];
		if( $cards->count() > 0 ) {
			foreach( $cards as $card ) {
				$cardsArray[] = $card;
			}
		}

		return response()->json($cardsArray);
	}

	public function timeTrackStore(Request $request) {	

		$this->createTrack($request);

		return redirect()->route('sytrix.reports.tracks')->withSuccess('Manual Time Added');
	}

	public function timeTrackEdit(TimeTrack $timeTrack) {

		$departments = Department::with(['users'])->get();
		$clients = Client::all();

		return view('sytrix-app.time-track.edit', compact('timeTrack', 'departments', 'clients'));
	}

	public function timeTrackUpdate(Request $request, TimeTrack $timeTrack) {

		$this->createTrack($request, $timeTrack);

		return redirect()->route('sytrix.reports.tracks')->withSuccess('Time successfuly edited.');

	}

	private function createTrack($theRequest, $theModel = null) {

		$this->validate($theRequest, [
			'user' => 'required',
			'client' => 'required',
			'project' => 'required',
			'task' => 'required',
			'time_in' => 'required',
			'time_out' => 'required',			
			'hours' => 'required_if:mins,'.null,
			'date' => 'required',
			'note' => 'required'
		]);

		// creates new task
		if( $theModel != null ) {
			$timeLog = $theModel;
		}
		else {
			$timeLog = new TimeTrack;
		}

		$timeLog->user_id = $theRequest->user;
		$timeLog->project_id = $theRequest->project;
		$timeLog->timeable_id = $theRequest->time_id;
		$timeLog->timeable_type = ($theRequest->time_type == 'task') ? Task::class : Card::class;
		$timeLog->timeable_type_value = $theRequest->time_type;
		$timeLog->note = $theRequest->note;
		$timeLog->manual_entry = $theRequest->user()->id;

		// Time in - Time out
		//Check for pm or am time in or time out
		$timeLog->time_in = date('H:i:s', strtotime($theRequest->time_in));
		$timeLog->time_out = date('H:i:s', strtotime($theRequest->time_out));

		// Hours rendered
		$hours = $theRequest->hours;
		$mins = $theRequest->mins + ($hours * 60);
		$timeLog->hours_rendered = $mins;

		// Date rendered
		$dateRendered = explode('-', $theRequest->date);
		$year = intval($dateRendered[0]);
		$month = intval($dateRendered[1]);
		$date = intval($dateRendered[2]);
		$timeLog->date = Carbon::createFromDate($year, $month, $date, 'GMT');

		$timeLog->save();

	}

}
