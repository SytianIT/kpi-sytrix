<?php

namespace App\Http\Controllers\Sytrix;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Project;
use App\User;

class TasksController extends Controller
{
    
	public function index(Project $project) {	
		$cards = $project->cards->sortBy('order')
				->load(['tasks' => function($q) {
					$q->orderBy('id', 'asc');
				}, 'users']);

		$selectedUsers = $project->users;

		$selectedUsersArray = [];
		foreach( $project->users as $sUser ) {
			$selectedUsersArray[] = $sUser->id;
		}

		$users = User::where('id', '<>', 1)->get();

		return view('sytrix-app.task.index', compact('cards', 'project', 'selectedUsers', 'selectedUsersArray', 'users'));
	}

	public function create(Request $request) {

		$this->validate($request, [
			'task_title' => 'required|unique:stx_tasks,title,NULL,id,card_id,'.$request->task_title	
		]);

		$task = new Task;

		$task->title = $request->task_title;
		$task->card_id = $request->card_id;
		$task->save();

		return response()->json( $task );
	}

	public function update( Request $request, Task $task ) {

		$this->validate($request, [
			'task_title' => 'required|unique:stx_tasks,title,NULL,id,card_id,'.$request->task_title		
		]);

		$task->title = $request->task_title;
		$task->save();

		return response()->json( $task );

	}

	public function delete(Task $task) {

		$task->delete();

	}	
}
