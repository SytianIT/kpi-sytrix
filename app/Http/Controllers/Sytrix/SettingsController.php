<?php

namespace App\Http\Controllers\Sytrix;

use App\Applications\Sytrix\ProjectCategory;
use App\Applications\Sytrix\ProjectStatus;
use App\Applications\Sytrix\TaskCategory;
use Illuminate\Http\Request;


use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    
	// ************************ //
	// **** PROJECT CATEGORIES **** //
	// ************************ //
	public function categories() {

		$projectCategories = ProjectCategory::roots()->get();	

		return view('sytrix-app.category.index', compact('projectCategories'));

	}

	public function create() {

		$parentcategories = ProjectCategory::roots()->get();

		return view('sytrix-app.category.create', compact('parentcategories'));

	}

	public function store(Request $request) {

		$this->validate($request, [
			'title' => 'required|unique:project_categories,title,'.$request->title
		]);

		$projectCat = new ProjectCategory;

		$projectCat->parent_id = $request->parent_id;
		$projectCat->title = $request->title;
		$projectCat->name = str_slug( $request->title );
		$projectCat->description = $request->description;

		$projectCat->save();

		return redirect()->route('sytrix.categories')->withSuccess('Project Category saved.');
	}

	public function edit(ProjectCategory $projectcat) {

		if( $projectcat->parent_id != null || $projectcat->parent_id != 0 ) {
			$parentcategories = ProjectCategory::roots()->get();
		}
		else {
			$parentcategories = ProjectCategory::roots()->where('id', '!=', $projectcat->id)->get();
		}

		$currentProjectCat = $projectcat;

		return view('sytrix-app.category.edit', compact('parentcategories', 'currentProjectCat'));

	}

	public function update(Request $request, ProjectCategory $projectcat) {

		$parent = ProjectCategory::find($request->parent_id);

		$projectcat->title = $request->title;
		$projectcat->name = str_slug( $request->title );
		$projectcat->description = $request->description;
		$projectcat->save();

		if( $parent ) {
			$projectcat->makeChildOf($parent);
		}
		else {
			$projectcat->makeRoot();
		}


		return redirect()->route('sytrix.categories')->withSuccess('Project Category successfully updated.');
	}

	public function delete(ProjectCategory $projectcat) {

		//CHECK IF CHILD EXISTS
		$children = $projectcat->children()->get();

		$projectcat->delete();	
	
		return redirect()->route('sytrix.categories')->withStatus('Project Category deleted.');

	}

	public function trash() {

		$projectCategories = ProjectCategory::onlyTrashed()->get();

		return view('sytrix-app/category/trash', compact('projectCategories'));

	}

	public function restore($projectcat) {

		$projectcat = ProjectCategory::onlyTrashed()->where('id', $projectcat)->first();

		$projectcat->restore();
		

		return redirect()->route('sytrix.categories')->withStatus('Project Category restored');
	}


	// ************************ //
	// **** STATUS CATEGORIES **** //
	// ************************ //

	public function status() {

		$statuses = ProjectStatus::roots()->get();

		return view('sytrix-app.status.index', compact('statuses'));

	}

	public function createStatus() {

		$statuses = ProjectStatus::roots()->get();

		return view('sytrix-app.status.create', compact('statuses'));
	}

	public function storeStatus( Request $request ) {
		
		if( $request->parent_status == null ) {
			$parentVal = 'NULL';
		}
		else {
			$parentVal = $request->parent_status;
		}

		$this->validate($request, [
			'title' => 'required|unique:project_status,title,NULL,id,parent_id,'.$parentVal		
		]);

		$parentStatus = ProjectStatus::find($request->parent_status);

		$status = new ProjectStatus;

		if( $parentStatus ) {
			$status->name = str_slug($parentStatus->title . "-" . $request->title);
		}
		else {			
			$status->name = str_slug($request->title);
		}

		$status->title = $request->title;
		$status->description = $request->description;
		$status->color_status = $request->color_status;
		$status->save();

		if( $parentStatus ) {
			$status->makeChildOf( $parentStatus );
		}
		else {
			$status->makeRoot();
		}

		return redirect()->route('sytrix.status')->withSuccess('Project status successfully added');
	}

	public function editStatus(ProjectStatus $status) {

		if( $status->parent_id != null || $status->parent_id != 0 ) {
			$statuses = ProjectStatus::roots()->get();
		}
		else {
			$statuses = ProjectStatus::roots()->where('id', '!=', $status->id)->get();
		}

		$currentStatus = $status;

		return view('sytrix-app.status.edit', compact('currentStatus', 'statuses'));
	}

	public function updateStatus( Request $request, ProjectStatus $status ) {

		if( $request->parent_status == null ) {
			$parentVal = 'NULL';
		}
		else {
			$parentVal = $request->parent_status;
		}

		$this->validate($request, [
			'title' => 'required|unique:project_status,title,NULL,id,parent_id,'.$parentVal			
		]);

		$parentStatus = ProjectStatus::find( $request->parent_status );

		if( $parentStatus ) {
			$status->name = str_slug($parentStatus->title . "-" . $request->title);
		}
		else {			
			$status->name = str_slug($request->title);
		}

		$status->title = $request->title;
		$status->color_status = $request->color_status;
		$status->description = $request->description;
		$status->save();	

		if( $parentStatus ) {
			$status->makeChildOf( $parentStatus );
		}
		else {
			$status->makeRoot();
		}

		return redirect()->route('sytrix.status')->withSuccess('Project Status successfully updated.');
	}

	public function deleteStatus(ProjectStatus $status) {
			$status->delete();
			return redirect()->route('sytrix.status')->withStatus('Project status deleted.');	
	}

	public function trashStatus() {	

		$statuses = ProjectStatus::onlyTrashed()->get();

		return view('sytrix-app.status.trash', compact('statuses'));
	}

	public function restoreStatus($status) {

		$projectStatus = ProjectStatus::onlyTrashed()->where('id', $status)->first();

		$projectStatus->restore();

		return redirect()->route('sytrix.status')->withStatus('Project status restored.');
	}

	public function sortStatus() {
		$statuses = ProjectStatus::roots()->get();
		return view('sytrix-app.status.sort', compact('statuses'));
	}

	public function updateStatusSort(Request $request) {
		
		$lastEntity = null;

		$sortCount = count( $request->sort_val ) - 1;

		foreach( $request->sort_val as $sortKey => $sortVal ) {

			$taskCategory = ProjectStatus::find(intval($sortVal));

			if( $lastEntity != null ) {
				$taskCategory->moveToRightOf( $lastEntity );
			}
			else {
				$taskCategory->makeRoot();
			}

			$lastEntity = $taskCategory;

		}

	}

	//AJAX FUNCTIONS
	public function changeColorStatus( Request $request ) {

		$projectStatus = ProjectStatus::find($request->id);

		$projectStatus->color_status = $request->color;

		$projectStatus->save();

	}


	// ************************ //
	// **** TASK CATEGORIES **** //
	// ************************ //
	public function taskCategories() {

		$taskCategories = TaskCategory::roots()->get();

		return view('sytrix-app/task-category/index', compact('taskCategories'));

	}

	public function createTaskCategory() {

		$taskCategories = TaskCategory::roots()->get();

		return view('sytrix-app/task-category/create', compact('taskCategories'));
	}

	public function storeTaskCategory(Request $request) {

		if( $request->parent_taskcat == null ) {
			$parentVal = 'NULL';
		}
		else {
			$parentVal = $request->parent_taskcat;
		}

		$this->validate($request, [
			'title' => 'required|unique:stx_task_categories,title,NULL,id,parent_id,'.$parentVal
		]);

		$parentTaskCat = TaskCategory::find($request->parent_taskcat);
		if( $parentTaskCat ) {
			$parentTitle = $parentTaskCat->title;
			$siblingCount = $parentTaskCat->children()->count() + 1;
		}
		else {
			$parentTitle = null;
			$siblingCount = TaskCategory::roots()->count() + 1;
		}

		$taskCategory = new TaskCategory;

		$taskCategory->name = str_slug($parentTitle . "-" . $request->title);
		$taskCategory->title = $request->title;
		$taskCategory->position = $siblingCount;
		$taskCategory->save();

		if( $parentTaskCat ) {
			$taskCategory->makeChildOf($parentTaskCat);
		}
		else {
			$taskCategory->makeRoot();
		}

		return redirect()->route('sytrix.task-categories')->withSuccess('Task Category created.');

	}

	public function editTaskCategory(TaskCategory $taskcategory) {

		$currentTaskCategory = $taskcategory;
		$taskCategories = TaskCategory::roots()->get();

		return view('sytrix-app/task-category/edit', compact('currentTaskCategory', 'taskCategories'));
	}

	public function updateTaskCategory(Request $request, TaskCategory $taskcategory) {

		if( $request->parent_taskcat == null ) {
			$parentVal = 'NULL';			
		}
		else {
			$parentVal = $request->parent_taskcat;			
		}

		$this->validate($request, [
			'title' => 'required|unique:stx_task_categories,title,NULL,id,parent_id,'.$parentVal
		]);

		$parentTaskCat = TaskCategory::find($request->parent_taskcat);
		if( $parentTaskCat ) {
			$parentTitle = $parentTaskCat->title;
			$siblingCount = $parentTaskCat->children()->count() + 1;
		}
		else {
			$parentTitle = null;
			$siblingCount = TaskCategory::roots()->count() + 1;
		}

		$taskcategory->name = str_slug($parentTitle . "-" . $request->title);
		$taskcategory->title = $request->title;
		$taskcategory->position = $siblingCount;
		$taskcategory->save();

		if( $parentTaskCat ) {
			$taskcategory->makeChildOf($parentTaskCat);
		}
		else {
			$taskcategory->makeRoot();
		}

		return redirect()->route('sytrix.task-categories')->withSuccess('Task Category updated.');

	}

	public function deleteTaskCategory(TaskCategory $taskcategory) {

		$taskcategory->delete();

		return redirect()->route('sytrix.task-categories')->withStatus('Task Category deleted.');
	}

	public function trashTaskCategory() {

		$taskCategories = TaskCategory::onlyTrashed()->get();

		return view('sytrix-app/task-category/trash', compact('taskCategories'));

	}

	public function restoreTaskCategory($taskcategory) {

		$taskCategory = TaskCategory::onlyTrashed()->where('id', $taskCategory);

		$taskCategory->restore();

		return redirect()->route('sytrix.task-categories')->withSuccess('Task Category restore.');
	}

	public function sortTaskCategory() {

		$taskCategories = TaskCategory::roots()->orderBy('position', 'desc')->get(); 

		return view('sytrix-app.task-category.sort', compact('taskCategories'));

	}

	public function updatesortTaskCategory(Request $request) {
		
		$lastEntity = null;

		foreach( $request->sort_val as $sortKey => $sortVal ) {

			$taskCategory = TaskCategory::find(intval($sortVal));

			if( $lastEntity != null ) {
				$taskCategory->moveToRightOf( $lastEntity );
			}
			else {
				$taskCategory->makeRoot();
			}

			$lastEntity = $taskCategory;

		}

	}	
}
