<?php

namespace App\Http\Controllers\Sytrix;

use Illuminate\Http\Request;
use App\Applications\Sytrix\Client;
use App\Applications\Expense\Payment;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
    public function index( Request $request ){	

        if( $request->search_client != null ) {
            $query = new Client;

            $query = $query->where('contact_person', 'like', "%$request->search_client%")->orWhere('company_name', 'like', "%$request->search_client%");

            $clients = $query->paginate(20);
        }
        else {
            $clients = Client::orderBy('id', 'desc')->paginate(20);          
        }


        return view('sytrix-app.clients.index', compact('clients', 'request'));
    }

    public function create() {

    	return view('sytrix-app.clients.create');

    }

    public function store(Request $request) {	

        $companyName = '"'.$request->company_name.'"';
        
    	$this->validate($request, [
            'logo' => 'max:20000|image',
			'company_name' => 'required|unique:clients,company_name,'.$companyName,
			'company_address' => 'required:clients,company_address,'.$request->company_address,
            'contact_person' => 'required:clients,contact_person,'.$request->company_address,
			'mobile_number' => 'required:clients,mobile_number,'.$request->mobile_number,
			'landline_number' => 'required:clients,landline_number,'.$request->landline_number,
			'email' => 'required|unique:clients,email,'.$request->email
		]);

        //IF IMAGE IS BEING UPLOADED
        $client = new Client;

        if( $request->logo ) {
            $logoPath = $this->baseUpload($request->file('logo'), 'images/profile-images', 200);
            $client->logo = $logoPath;
        }

    	$client->company_name = $request->company_name;
    	$client->company_address = $request->company_address;
    	$client->contact_person = $request->contact_person;
    	$client->mobile_number = $request->mobile_number;
    	$client->landline_number = $request->landline_number;
    	$client->email = $request->email;
    	$client->skype_id = $request->skype_id;
    	$client->payment_terms = $request->payment_terms;
    	$client->discounts = $request->discounts;
    	$client->layout = $request->layout;
    	$client->hosting = $request->hosting;
    	$client->domain = $request->domain;
    	$client->notes = $request->notes;

    	$client->save();

    	return redirect()->route('clients')->withSuccess('Client successfully added.');

    }

    public function edit(Client $client) {

    	return view('sytrix-app.clients.edit', compact('client'));

    }

    public function update(Request $request, Client $client) {

        if( $request->logo ) {
            $logoPath = $this->baseUpload($request->file('logo'), 'images/profile-images', 200);
            $client->logo = $logoPath;
        }

		$client->company_name = $request->company_name;
    	$client->company_address = $request->company_address;
    	$client->contact_person = $request->contact_person;
    	$client->mobile_number = $request->mobile_number;
    	$client->landline_number = $request->landline_number;
    	$client->email = $request->email;
    	$client->skype_id = $request->skype_id;
    	$client->payment_terms = $request->payment_terms;
    	$client->discounts = $request->discounts;
    	$client->layout = $request->layout;
    	$client->hosting = $request->hosting;
    	$client->domain = $request->domain;
    	$client->notes = $request->notes;

    	$client->save();

    	return redirect()->route('clients')->withSuccess('Client successfully updated.');
    }

    public function delete(Client $client) {

    	$client->delete();

    	return redirect()->route('clients')->withStatus('Client deleted.');
    }

    public function trash() {

        $clients = Client::onlyTrashed()->get();

        return view('sytrix-app.clients.trash', compact('clients'));

    }

    public function restore($client) {

        $client = Client::onlyTrashed()->where('id', $client)->first();
        $client->restore();

        return redirect()->route('client.trash')->withSuccess('Client restored.');
    }

}
