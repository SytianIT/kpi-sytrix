<?php

namespace App\Http\Controllers\Sytrix\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\TimeTrack;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\Client;
use App\Department;
use App\User;
use Carbon\Carbon;
use Hash;

class TimeController extends Controller
{

	// Time Nurse Login
	public function login(Request $request) {

		$exists = User::where('username', '=', $request->user)->orWhere('email', '=', $request->user)->first();

        if($exists){
        	if( Hash::check($request->pass, $exists->password) ) {
        		$message = 'match';
        		return response()->json(compact('message', 'exists'));        		
        	}
        	else {
        		$message = 'not-match';        		
        		return response()->json(compact('message'));
        	}
        }
        else {
    		$message = 'not-match';
        	return response()->json(compact('message'));
        }

	}

	public function getClients(Request $request)  {

		// Return projects, user details, current time tracks and clients
		$clients = Client::orderBy('company_name', 'asc')->whereHas('projects.cards.users', function($q) use($request){
			$q->where('id', $request->active_user);
		})->get();

		$userName = User::find($request->active_user)->username;
		$firstName = User::find($request->active_user)->firstname;
		$timeTracks = User::find($request->active_user)
							->timeTracks()
							->where('date', Carbon::now()->format('Y-m-d'))
							->orderBy('id', 'asc')
							->with('project.client')
							->get();

		$totalHours = User::find($request->active_user)
							->timeTracks()
							->where('date', Carbon::now()->format('Y-m-d'))
							->sum('hours_rendered');

		$mins = $totalHours;

		// If minutes is greater than 60
		if( $totalHours > 60 ) {
			$mins = $totalHours % 60;
			$hours = floor($totalHours / 60);
		}

		if( $clients->count() > 0 ) {
			$cli = '<option disabled selected>Select Client</option>';
			foreach( $clients as $client ) {
				$cli .= '<option value="'.$client->id.'">'.$client->company_name.'</option>';
			}
		}
		else {
			$cli = 'Sorry! no assigned projects for you.';
		}

		return response()->json(compact('cli', 'userName', 'firstName', 'timeTracks', 'mins', 'hours'));
	}

	public function getProjects(User $user, Request $request) {

		$projects = $user->projects->filter(function($project) use($request) {
			return $project->client_id === $request->client;
		});

		$proj = '';
		if( $projects->count() > 0 ) {
			$proj .= '<option disabled selected>Select Project</option>';
			foreach( $projects as $project ) {	
				$proj .= '<option value="'.$project->id.'">'.$project->name.'</option>';
			}
		}
		else {
			$proj = false;
		}

		return response()->json($proj);
	}

	public function getProjectCards(User $user, Project $project) {
		$cards = $project->cards()->orderBy('order', 'asc')->whereHas('users', function($query) use($user){
			$query->where('id', $user->id);
		})->with('tasks')->get();

		if( $cards->count() == 0 ) {
			$cards = false;
		}

		return response()->json($cards);
	}

	public function loginTask(User $user, Request $request) {

		$timeTrack = new TimeTrack;

		$timeTrack->user_id = $user->id;
		$timeTrack->project_id = $request->project_id;
		$timeTrack->timeable_id = $request->task_id;
		$timeTrack->timeable_type = ($request->task_type == "task") ? Task::class : Card::class;
		$timeTrack->timeable_type_value = $request->task_type;
		$timeTrack->hours_rendered = 0;
		$timeTrack->time_in = Carbon::now();
		$timeTrack->date = Carbon::now();

		$timeTrack->save();

		return response()->json($timeTrack->id);

	}

	public function logoutTask(Request $request) {

		$timeTrack = TimeTrack::find($request->time_id);

		$timeTrack->time_out = Carbon::now();
		$timeTrack->hours_rendered = $request->hours_rendered;

		$timeTrack->save();

	}

	public function viewDetails(Request $request) {

		$taskDetails = Client::with(['projects' => function($q) use($request){

			// If task type is card
			if( $request->task_type == 'card' ) {
				$q->with(['cards' => function($qCards) use($request){
					$qCards->with(['timeTracks' => function($qTracks) use($request){
						$qTracks->find($request->tt_id);
					}])->find($request->task_id);
				}])->find($request->project_id);
			}
			// If task type is task
			else {
				$cardID = Task::find($request->task_id)->card->id;

				$q->with(['cards' => function($qCards) use($request, $cardID){
					$qCards->with(['tasks' => function($qTasks) use($request){
						$qTasks->with(['timeTracks' => function($qTracks) use($request) {
							$qTracks->find($request->tt_id);
						}])->find($request->task_id);
					}])->find($cardID);
				}])->find($request->project_id);
			}
		}])->find($request->client_id);

		if( $request->task_type == "card" ) {
			$taskType = "card";
		}	
		else {
			$taskType = "task";
		}

		return response()->json(compact('taskDetails', 'taskType'));
	}

	public function addNote(TimeTrack $time, Request $request) {

		$time->note = $request->time_note;

		$time->save();

		$note = nl2br($time->note);

		return response()->json($note);

	}

	public function getUpdatedHoursRendered(Request $request) {

		$timeTracks = User::find($request->active_user)
							->timeTracks()
							->where('date', Carbon::now()->format('Y-m-d'))
							->orderBy('id', 'asc')
							->with('project.client')
							->get();

		$totalHours = User::find($request->active_user)
							->timeTracks()
							->where('date', Carbon::now()->format('Y-m-d'))
							->sum('hours_rendered');

		$mins = $totalHours;

		// If minutes is greater than 60
		if( $totalHours > 60 ) {
			$mins = $totalHours % 60;
			$hours = floor($totalHours / 60);
		}

		return response()->json(compact('mins', 'hours', 'timeTracks'));
	}

	public function refreshList(Request $request) {

		$clients = Client::orderBy('company_name', 'asc')->whereHas('projects.cards.users', function($q) use($request){
			$q->where('id', $request->active_user);
		})->get();

		if( $clients->count() > 0 ) {
			$cli = '<option disabled selected>Select Client</option>';
			foreach( $clients as $client ) {
				$cli .= '<option value="'.$client->id.'">'.$client->company_name.'</option>';
			}
		}
		else {
			$cli = 'Sorry! no assigned projects for you.';
		}

		return response()->json(compact('cli'));
	}

	public function getTimeTracks(Request $request) {

		$timeTracks = User::find($request->active_user)
							->timeTracks()
							->where('date', Carbon::now()->format('Y-m-d'))
							->orderBy('id', 'asc')
							->with(['timeable', 'project.client'])
							->get();

		$clients = [];

		foreach( $timeTracks as $timeTrack ) {
			if( !in_array($timeTrack->project->client->company_name, $clients) ) {
				$clients[$timeTrack->project->client->company_name] = array();
			}
		}

		foreach ($timeTracks as $timeTrack) {
			if( array_key_exists($timeTrack->project->client->company_name, $clients) ) {
				$clients[$timeTrack->project->client->company_name][] = $timeTrack;
			}
		}

		return response()->json(compact('clients'));

	}

	public function updateCurrentTask(Request $request) {

		$timeTrack = TimeTrack::find($request->time_id);

		$timeTrack->hours_rendered = $request->hours_rendered;

		$timeTrack->save();

	}

}