<?php

namespace App\Http\Controllers\Sytrix;

use Illuminate\Http\Request;
use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\ProjectCategory;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\TaskCategory;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\Client;
use App\Applications\Sytrix\ProjectStatus;
use App\Applications\Sytrix\Notification;
use App\Department;
use App\User;
use DateTime;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    
	public function index(Request $request) {

		if( $request->search_project != null ) {
            $query = new Project;

            $query = $query->where('name', 'like', "%$request->search_project%")
            			   ->orWhereHas('client', function($query) use($request){
            			   		$query->where('company_name', 'like', "%$request->search_project%");
            			   })->with('client');

            $projects = $query->orderBy('id', 'desc')->paginate(20);
        }
        else if( isset($_GET['client_id']) ) {
        	$query = Project::where('client_id', '=', $_GET['client_id'])->with(['client']);

        	$projects = $query->orderBy('id', 'desc')->paginate(20);
        }
        else {
            $projects = Project::with(['client'])->orderBy('id', 'desc')->paginate(20);          
        }

		return view('sytrix-app.project.index', compact('projects'));
	} 

	public function create() {	

		$categories = ProjectCategory::roots()->get();
		$clients = Client::all();
		$statuses = ProjectStatus::roots()->get();
		$departments = Department::all();

		return view('sytrix-app.project.create', compact('categories', 'clients', 'statuses', 'departments'));

	} 

	public function store(Request $request) {

		$this->validate($request, [
			'name' => 'required'
		]);

		$project =  new Project;

		$project->fill($request->all());
		// Check project with the same slug 
		$comparedSlug = str_slug( $request->client . '-' . $request->name );
		$projectSlug = Project::where('slug', 'like', "%$comparedSlug%")->get();
		if( $projectSlug->count() > 0 ) {
			$project->slug = str_slug( $request->name ) . Carbon::now();
		}
		else {
			$project->slug = str_slug( $request->name );
		}
		$project->start_date = new Carbon( $request->start_date );
		$project->due_date = new Carbon( $request->due_date );
		$project->client_id = $request->client_id;
		$project->pm_id = $request->pm_id;
		$project->date_completed = $request->date_completed;

		$project->save();

		$project->categories()->sync((array) $request->project_category_id);
		$project->departments()->sync((array) $request->department_id);

		// Add users to the temporary added members for the project
		$toBeMembers = User::whereIn('department_id', $request->department_id)->pluck('id')->toArray();
		$project->users()->sync((array) $toBeMembers);		

		//Create new tasks for this project
		$tasksCat = TaskCategory::roots()->get();
		if( count( $tasksCat ) != 0 ) {
				$card = new Card;
			foreach( $tasksCat as $taskCat ) {
				$card->project_id = $project->id;
				$card->title = $taskCat->title;

				// get the max order
				$maxOrder = Card::where('project_id', $project->id)->max('order');
				$card->order = $maxOrder + 1;

				$card->slug = str_slug($project->name . "-" . $taskCat->title);
				$card->save();

				// automatically add the project manager
				$card->users()->sync((array) $request->pm_id);

				// Check if the card has tasks then add tasks
				if( $taskCat->children()->count() > 0 ) {
					foreach( $taskCat->children()->get() as $taskCatChild ) {
						$childTask = new Task;

						$childTask->title = $taskCatChild->title;
						$childTask->card_id = $card->id;
						$childTask->save();
					}
				}

			}
		}

		// Create a notification
		$notification = new Notification;
		$notification->user_id = $request->pm_id;
		$notification->contextable_type = Project::class;
		$notification->contextable_id = $project->id;
		$notification->contextable_value = 'project';
		$notification->status = 'success';
		$notification->description = 'New project created under ' . $project->client->company_name.'.';
		$notification->is_read = 0;
		$notification->save();


		return redirect()->route('sytrix.projects')->withSuccess('Project successfuly added.');

	}

	public function edit(Project $project) {

		$categories = ProjectCategory::all();
		$clients = Client::all();
		$statuses = ProjectStatus::roots()->get();

		return view('sytrix-app.project.edit', compact('project', 'categories', 'clients', 'statuses'));
	}

	public function update(Project $project, Request $request) {

		$this->validate($request, [
			'name' => 'required'
		]);


		$project->fill($request->all());
		
		// Check project with the same slug 
		$comparedSlug = str_slug( $request->client . '-' . $request->name );
		$projectSlug = Project::where('slug', 'like', '%$comparedSlug%')->first();

		if( $projectSlug ) {
			if( $projectSlug->count() > 0 ) {
				$project->slug = str_slug( $request->client . '-' . $request->name . Carbon::now() );
			}
		}
		else {
			$project->slug = str_slug( $request->client . '-' . $request->name );
		}
		$project->start_date = new Carbon( $request->start_date );
		$project->due_date = new Carbon( $request->due_date );
		$project->client_id = $request->client_id;
		$project->pm_id = $request->pm_id;
		if( $request->project_status_id  == 38 ) {
			$project->date_completed = new Carbon( $request->date_completed );
		}
		else {
			$project->date_completed  = '';
		}

		$project->save();

		$project->categories()->sync((array) $request->project_category_id);
		$project->departments()->sync((array) $request->department_id);

		// Add users to the temporary added members for the project
		$toBeMembers = User::whereIn('department_id', $request->department_id)->pluck('id')->toArray();
		$project->users()->sync((array) $toBeMembers);


		return redirect()->route('sytrix.projects')->withSuccess('Project details updated.');
	}

	public function moveToArchive(Project $project) {

		$project->delete();

		return redirect()->route('sytrix.projects')->withStatus('Project moved to Arhives');
	}

	public function archive( Request $request ) {

		if( $request->search_project != null ) {
            $query = new Project;

            $query = $query->onlyTrashed()->where('name', 'like', "%$request->search_project%");

            $projects = $query->paginate(20);
        }
        else {
            $projects = Project::onlyTrashed()->paginate(20);          
        }

		return view('sytrix-app.project.archive', compact('projects'));
	}

	public function restore($projectID) {

		$project = new Project;
		$project->onlyTrashed()->where('id', $projectID)->restore();

		return redirect()->route('sytrix.project.archive')->withSuccess('Project set as active.');

	}

	public function delete($projectID) {
		$project = new Project;
		$project->onlyTrashed()->where('id', $projectID)->forceDelete();

		return redirect()->route('sytrix.project.archive')->withStatus('Archived Project deleted.');
	}


	// Users
	public function searchUser(Project $project, Request $request) {
		
		$users = User::where('id', '<>', 1)->where('firstname', 'like', "%$request->search_member%")->get();
		$selectedUsers = $project->users()->get();

		return response()->json( ['users' => $users, 'selectedUsers' => $selectedUsers] );

	}

	public function assignUser(Project $project, Request $request) {

		if( !$project->users->contains($request->member_id) ) {
			$project->users()->attach($request->member_id);	
		}		
		
		$users = $project->users()->get();

		return response()->json( $users );

	}

	public function removeUser(Project $project, Request $request) {
		$project->users()->detach($request->member_id);	

		$users = $project->users()->get();

		return response()->json( $users );
	}
}
