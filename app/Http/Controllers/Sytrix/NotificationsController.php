<?php

namespace App\Http\Controllers\Sytrix;

use App\Applications\Sytrix\Notification;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{

	public function notified(Notification $notification) {

		$notification->is_read = 1;
		$notification->save();

		$user = Auth::user();

		$notifications = $user->notifications()->where('is_read', 0)->get();
		$count = $notifications->count();

		return response()->json(compact('count', 'notifications'));

	}

}
