<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SettingsController extends Controller
{
	/**
	 * Display Settings
	 * @return view
	 */
    public function index()
    {
    	return view('modules.settings.index');
    }
}
