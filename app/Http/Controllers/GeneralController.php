<?php

namespace App\Http\Controllers;

use App\Applications\Expense\Expense;
use App\Applications\Expense\Recurring;
use App\AuditLog;
use App\Http\Requests;
use App\Syion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GeneralController extends Controller
{
	public function __construct()
	{
		Session::put('current-app', 'main');
	}

    /**
     * Dashboard Method
     * @return view
     */
    public function index()
    {
    	return view('welcome');
    }

    /**
     * Method to display Expense Application - Dashboard
     * @return View 
     */
    public function goExpense()
    {
    	Session::put('current-app', 'expense');

        $firstDayOfThisMonth = new Carbon('first day of this month');
        $lastDayOfThisMonth = new Carbon('last day of this month');
        $lastLastMon = Carbon::today()->subMonth(2);
        $lastMon = Carbon::today()->subMonth(1);
        $thisMon = Carbon::today();
        
        $recExpenses = Recurring::whereBetween('next_expense_recurring', [$firstDayOfThisMonth, $lastDayOfThisMonth])->orderBy('next_expense_recurring', 'ASC')->get();

        $expenses = Expense::recurrings(false)->whereBetween('date_occured', [$firstDayOfThisMonth, $lastDayOfThisMonth])
            ->orderBy('date_occured', 'ASC')->limit(5)->get();


        $ttlExpLastLastMon = Expense::recurrings(false)
            ->whereBetween('date_occured', [$lastLastMon->startOfMonth()->format('Y-m-d H:i:s'), $lastLastMon->endOfMonth()->format('Y-m-d H:i:s')])
            ->get()->sum('amount_in_php');
        $ttlExpLastMon = Expense::recurrings(false)
            ->whereBetween('date_occured', [$lastMon->startOfMonth()->format('Y-m-d H:i:s'), $lastMon->endOfMonth()->format('Y-m-d H:i:s')])
            ->get()->sum('amount_in_php');
        $ttlExpThisMon = Expense::recurrings(false)
            ->whereBetween('date_occured', [$thisMon->startOfMonth()->format('Y-m-d H:i:s'), $thisMon->endOfMonth()->format('Y-m-d H:i:s')])
            ->get()->sum('amount_in_php');

        $logs = AuditLog::thisApp(Syion::EXPENSE)->orderBy('created_at', 'DESC')->take(20)->get();

    	return view('expense-app.index', compact('recExpenses','expenses','logs','ttlExpLastLastMon','ttlExpLastMon','ttlExpThisMon'));
    }

    /**
     * Method to display Sytian Project Metrix Application - Dashboard
     * @return View 
     */
    public function goSytrix()
    {
    	Session::put('current-app', 'sytrix');

    	return view('sytrix-app.index');
    }
}
