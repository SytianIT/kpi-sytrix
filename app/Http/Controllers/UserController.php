<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Carbon\Carbon;
use App\Department;
use Datetime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	/**
	 * Display User Index
	 * @return view
	 */
    public function index()
    {
        $users = User::removeCurrent()->removeSuperAdmin()->paginate(20);

    	return view('modules.user.index', compact('users'));
    }

    /**
     * Method to view create form for users
     * @return view
     */
    public function create()
    {
        $roles = Role::where('name', '<>', Role::SUPERADMIN)->get();
        $departments = Department::pluck('title', 'id');

    	return view('modules.user.create', compact('roles', 'departments'));
    }

    /**
     * Method to use to store user from creation
     * @param  Request $request
     * @return view
     */
    public function store(Request $request)
    {
        $this->validateUserFields($request);

        $user = new User;
        $user = $this->updateUserRecords($request, $user);
        $user->save();
        $roles = (count($request->role_id) > 0) ? $request->role_id : [];
        $user->roles()->sync($roles); 

        return redirect()->route('users')
                         ->withSuccess("User creation of '{$user->name}' was successfull");
    }

    /**
     * Method use to view the edit form for user
     * @param  User   $user
     * @return view      
     */
    public function edit(User $user)
    {
        $roles = Role::where('name', '<>', Role::SUPERADMIN)->get();

        $userRole = $user->roles;
        $currentUserRole = array_flatten($userRole->pluck('id'));
        $departments = Department::pluck('title', 'id');

        return view('modules.user.edit', compact('user','roles','currentUserRole', 'departments'));
    }

    /**
     * Method used to view update form for user
     * @param  Request $request
     * @param  User    $user   
     * @return view
     */
    public function update(Request $request, User $user)
    {
        $this->validateUserFields($request, $user);

        $user = $this->updateUserRecords($request, $user);
        $user->save();
        $roles = (count($request->role_id) > 0) ? $request->role_id : [];
        $user->roles()->sync($roles); 
        
        return redirect()->route('users')
                         ->withSuccess("User update of '{$user->name}' was successfull");
    }

    /**
     * Method to use for deleting of user
     * @param  User   $user
     * @return view      
     */
    public function delete(User $user)
    {
        $name = $user->name;
        $user->delete();

        return redirect()->route('users')
                         ->withSuccess("Deletion of User '{$name}' was successfull");
    }

    /**
     * Method to call within this class to update or store user basic fields
     * @param  Request $request 
     * @param  User $user    
     * @return $user          
     */
    private function updateUserRecords($request, $user)
    {
        $birthdate = new Carbon($request->birthdayPicker_birthDay);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->middlename = $request->middlename;
        $user->address = $request->address;
        $user->birthdate = $birthdate;
        $user->mobile_number = $request->mobile_number;
        $user->age = $birthdate->diffInYears(Carbon::now());
        $user->job_title = $request->job_title;
        $user->department_id = $request->department_id; // To be change in the future
        $user->email = $request->email;
        $user->username = $request->username;

        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('image')){
            $user->user_picture = $this->baseUpload($request->file('image'), 'uploads/users/user-pictures', 350, 350);
        }

        return $user;
    }

    /**
     * Method use to validate request fields to create or update user records
     * @param  Request $request 
     * @param  User $user    
     * @return Boolean          
     */
    private function validateUserFields($request, $user = null)
    {
        $passwordFields = [
            'password' => 'required|max:15|min:6|confirmed',
            'password_confirmation' => 'required'
        ];

        $id = ($user != null) ? ','.$user->id : '';
        $basicFields = [
            'firstname' => 'required',
            'lastname' => 'required',
            'middlename' => 'required',
            'address' => 'required',
            'birthdayPicker_birthDay' => 'required|date',
            'mobile_number' => 'required',
            'role_id' => 'required',
            'username' => 'required|max:15|unique:users,username'.$id,
            'email' => 'required|email|unique:users,email'.$id,
        ];

        if($user != null){
            if($request->has('password')){
                $basicFields = array_merge($basicFields, $passwordFields);
            }
        }

        if($request->hasFile('image')){
            $basicFields = array_merge($basicFields, [
                'image' => 'required|image'
            ]);
        }

        if($user == null){
            $basicFields = array_merge($basicFields, $passwordFields);
        }

        $this->validate($request, $basicFields, [
            'birthdayPicker_birthDay.required' => 'Your birthdate is required.',
            'role_id.required' => 'Please assign roles for this user.',
            'firstname.required' => 'Your first name is required',
            'lastname.required' => 'Your last name is required',
            'middlename.required' => 'Your middle name is required',
        ]);
        return;
    }

}
