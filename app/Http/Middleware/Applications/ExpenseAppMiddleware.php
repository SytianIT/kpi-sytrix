<?php

namespace App\Http\Middleware\Applications;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class ExpenseAppMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Session::put('current-app', 'expense');

        if(Auth::user()->hasPermission('access_expense')){
            return $next($request);
        } else {
            return redirect()->to('/')
                             ->withWarning('You were redirected back, you are not allowed to access that page.');
        }
    }
}
