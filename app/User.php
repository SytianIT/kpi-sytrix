<?php

namespace App;

use App\Applications\Expense\Expense;
use App\Permission;
use App\Role;
use Carbon\Carbon;
use App\Applications\Sytrix\Client;
use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\Task;
use App\Applications\Sytrix\Card;
use App\Applications\Sytrix\TimeTrack;
use App\Applications\Sytrix\Notification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use SoftDeletes;
    
    const SUPERADMIN = 'superadmin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'username', 'job_title', 'mobile_number',
        'birthdate', 'age', 'address', 'department_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that cast as arrays.
     *
     * @var array
     */
    protected $casts = [
        'other_data' => 'array',
    ];

    /**
     * The attributes that declared in date formats
     *
     * @var array
     */
    protected $dates = [
        'birthdate', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Relation of many to many for Staff class
     * @return collection collection of all staffs assigned to this user
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * Relation of has many to Expense Class
     * @return collection collection of expenses created
     */
    public function expenses()
    {
        return $this->hasMany(Expense::class, 'user_id');
    }

    /**
     * Setters * Getters
     */
    public function getNameAttribute()
    {
        return $this->firstname.' '.$this->lastname;
    }

    public function scopeRemoveCurrent($query)
    {
        return $query->where('id', '<>', Auth::user()->id);
    }

    public function scopeRemoveSuperAdmin($query)
    {
        return $query->where('id', '<>', 1);
    }

    /**
     * Method to chech if the permission is permitted to this user role
     * @param  String  $permissionName
     * @return boolean 
     */
    public function hasPermission($permissionName)
    {
        $permitted = false;
        $permission = Permission::where('name', '=', $permissionName)->first();
        $assignedRole = $this->roles()->get();

        if($permission){
            $assignedRole->each(function ($item, $val) use (&$permitted, $permission){
                if($item->permissions->contains($permission)){
                    $permitted = true;
                    return false;
                }
            });
        }

        return $permitted;
    }

    /**
     * Other Methods
     */

    //Projects
    public function projects() {
        return $this->belongsToMany(Project::class);
    }

    // assigned projects
    public function assignedProjects() {


        $projects = $this->projects->filter(function($project){   

            $cards = $project->cards;
            $usersArr = [];

            foreach( $cards as $card ) {
                if( $card->users()->count() > 0 ) {
                    
                    // include in user array
                    foreach( $card->users as $user ) {
                        if( !in_array($user->id, $usersArr) ) {
                            $usersArr[] = $user->id;
                        }
                    }

                }
            }
            return in_array($this->id, $usersArr) && $project->id != 82;
        });

        return $projects;

    }

    // completed projects
    public function completedProjects($total = '') {

        $projects = $this->projects()->where(function($q) {
            $q->where('client_id', '!=', 60)->where('date_completed', '!=', null)
            ->whereHas('cards.users', function($q){
                    $q->where('id', $this->id);
                });
        })->get();

        if( $total == 'total' ) {
            $projects = $projects->count();
        }

        return $projects;

    }

    // total hours rendered
    public function totalHours() {

        $completedProjects = $this->completedProjects();
        $totalHours = 0;

        foreach( $completedProjects as $completedProject ) {

            // get the total hours of the completed project
            $totalMins = $completedProject->timeTracks;
            $totalMins = $totalMins->filter(function($t){
                return $t->user_id == $this->id;
            })->sum('hours_rendered');
            $totalMins = (int) ($totalMins / 60);

            $totalHours = $totalHours + $totalMins;

        }

        return $totalHours;

    }

    // average projects per month
    public function avgProjects($hours = '') {

        $pastMonth1 = with(Carbon::now())->subMonth();
        $pastMonth1Start = with(Carbon::now())->subMonth()->startOfMonth();
        $pastMonth1End = with(Carbon::now())->subMonth()->endOfMonth();

        $pastMonth2 = with(new Carbon($pastMonth1))->subMonth();
        $pastMonth2Start = with(new Carbon($pastMonth2))->startOfMonth();
        $pastMonth2End = with(new Carbon($pastMonth2))->endOfMonth();

        $pastMonth3 = with(new Carbon($pastMonth2))->subMonth();
        $pastMonth3Start = with(new Carbon($pastMonth3))->startOfMonth();
        $pastMonth3End = with(new Carbon($pastMonth3))->endOfMonth();

        // First past month projects
        $projectsPastMonth1 = $this->completedProjects()->filter(function($project) use($pastMonth1Start, $pastMonth1End){
            if( $project->date_completed >= $pastMonth1Start && $project->date_completed <= $pastMonth1End ) {
                return $project;
            }
        });

         // Second past month projects
        $projectsPastMonth2 = $this->completedProjects()->filter(function($project) use($pastMonth2Start, $pastMonth2End){
            if( $project->date_completed >= $pastMonth2Start && $project->date_completed <= $pastMonth2End ) {
                return $project;
            }
        });

         // Third past month projects
        $projectsPastMonth3 = $this->completedProjects()->filter(function($project) use($pastMonth3Start, $pastMonth3End){
            if( $project->date_completed >= $pastMonth3Start && $project->date_completed <= $pastMonth3End ) {
                return $project;
            }
        });


        if( $hours ) {

            $firstMonthTotalHours = 0;
            $secondMonthTotalHours = 0;
            $thirdMonthTotalHours = 0;

            // total project for the past three months
            $totalProjects = $projectsPastMonth1->count() + $projectsPastMonth2->count() + $projectsPastMonth3->count();
            
            // first month
            foreach( $projectsPastMonth1 as $project ) {
                $firstMonthTotalHours = $firstMonthTotalHours + $project->totalHours();
            }

             // second month
            foreach( $projectsPastMonth2 as $project ) {
                $secondMonthTotalHours = $secondMonthTotalHours + $project->totalHours();
            }

             // thirrd month
            foreach( $projectsPastMonth3 as $project ) {
                $thirdMonthTotalHours = $thirdMonthTotalHours + $project->totalHours();
            }

            if( $totalProjects == 0 ) {
                return false;
            }
            else {                
                $avgHours = round(($firstMonthTotalHours + $secondMonthTotalHours + $thirdMonthTotalHours) / $totalProjects);
                return $avgHours;
            }
    
        }
        else {

            $avgProject = round(($projectsPastMonth1->count() + $projectsPastMonth2->count() + $projectsPastMonth3->count()) / 3);
            return $avgProject;

        }


    }

    public function avgHoursPerProject() {
        return $this->avgProjects('hours');
    }

    public function rankPoints() {

        $pastMonth1 = with(Carbon::now())->subMonth();
        $pastMonth1Start = with(Carbon::now())->subMonth()->startOfMonth();
        $pastMonth1End = with(Carbon::now())->subMonth()->endOfMonth();

        $pastMonth2 = with(new Carbon($pastMonth1))->subMonth();
        $pastMonth2Start = with(new Carbon($pastMonth2))->startOfMonth();
        $pastMonth2End = with(new Carbon($pastMonth2))->endOfMonth();

        $pastMonth3 = with(new Carbon($pastMonth2))->subMonth();
        $pastMonth3Start = with(new Carbon($pastMonth3))->startOfMonth();
        $pastMonth3End = with(new Carbon($pastMonth3))->endOfMonth();

        // First past month projects
        $projectsPastMonth1 = $this->completedProjects()->filter(function($project) use($pastMonth1Start, $pastMonth1End){
            if( $project->date_completed >= $pastMonth1Start && $project->date_completed <= $pastMonth1End ) {
                return $project;
            }
        });

         // Second past month projects
        $projectsPastMonth2 = $this->completedProjects()->filter(function($project) use($pastMonth2Start, $pastMonth2End){
            if( $project->date_completed >= $pastMonth2Start && $project->date_completed <= $pastMonth2End ) {
                return $project;
            }
        });

         // Third past month projects
        $projectsPastMonth3 = $this->completedProjects()->filter(function($project) use($pastMonth3Start, $pastMonth3End){
            if( $project->date_completed >= $pastMonth3Start && $project->date_completed <= $pastMonth3End ) {
                return $project;
            }
        });

        $projectsPastMonth1->take(4);

        $rankPoints = 0;

        foreach ($projectsPastMonth1 as $project) {

            // get the total hours for this project
            $totalHours = $project->timeTracks()->where('user_id', $this->id)->sum('hours_rendered');
            $totalHours = (int) ($totalHours / 60);

            if( $totalHours != 0 ) {
                $rankPoint = round( 100 / ( $totalHours / $project->max_development_time ) );
                $rankPoints = $rankPoints + $rankPoint; 
            }

        }

        foreach ($projectsPastMonth2 as $project) {

            // get the total hours for this project
            $totalHours = $project->timeTracks()->where('user_id', $this->id)->sum('hours_rendered');
            $totalHours = (int) ($totalHours / 60);

            if( $totalHours != 0 ) {
                $rankPoint = round( 100 / ( $totalHours / $project->max_development_time ) );
                $rankPoints = $rankPoints + $rankPoint; 
            }

        }

        foreach ($projectsPastMonth3 as $project) {

            // get the total hours for this project
            $totalHours = $project->timeTracks()->where('user_id', $this->id)->sum('hours_rendered');
            $totalHours = (int) ($totalHours / 60);

            if( $totalHours != 0 ) {
                $rankPoint = round( 100 / ( $totalHours / $project->max_development_time ) );
                $rankPoints = $rankPoints + $rankPoint; 
            }

        }

        return $rankPoints;

    }

    // time tracks
    public function timeTracks() {
        return $this->hasMany(TimeTrack::class, 'user_id');
    }

    // clients
    public function clients() {
        return $this->hasManyThrough(Client::class, Project::class);
    }

    // notifications
    public function notifications() {
        return $this->hasMany(Notification::class);
    }
}
