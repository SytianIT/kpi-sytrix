<?php

namespace App\Console\Commands;

use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\Notification;

use Illuminate\Console\Command;

class NotifyQuota extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:nearing-quota';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify when the remaining hours is less than 20';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // get all the projects
        $projects = Project::whereHas('status', function($query) {  
            $query->where('name', '!=', 'completed');
        })->where('id', '!=', 82)->get();      

        foreach( $projects as $project ) {

            $estimatedHours = (int) $project->max_development_time + (int) $project->max_design_time + $project->max_pm_time;
            $hoursRendered = $project->totalHours();

            //get the users assigned in the project
            $usersArr = [];
            foreach( $project->cards()->get() as $card ) {
                if( $card->users()->count() > 0 ) {
                    foreach( $card->users()->get() as $user ) {
                        if( !in_array($user->id, $usersArr) ) {
                            $usersArr[] = $user->id;
                        }
                    }
                }
            }

            if( ( $estimatedHours - $hoursRendered ) <= 20 ) {
                if( count($usersArr) != 0 ) {
                    foreach( $usersArr as $user ) {

                        $notification = new Notification;
                        $notification->user_id = $user;
                        $notification->contextable_type = Project::class;
                        $notification->contextable_id = $project->id;
                        $notification->contextable_value = 'project';
                        $notification->status = 'info';
                        $notification->description = $project->name . ' under ' . $project->client->company_name . ' has ' . ($estimatedHours - $hoursRendered) . ' hours left to reach quota.';
                        $notification->is_read = 0;
                        $notification->save();

                    }
                }
            }

        }

    }
}
