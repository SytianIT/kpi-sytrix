<?php

namespace App\Console\Commands;

use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\Notification;
use Carbon\Carbon;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Console\Command;

class CheckDueDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:due-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron job for checking due dates per user';

    /**
     * Create a new command instance. *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      \Log::info('Check due Dates log');   

        // get all the projects
        $projects = Project::whereHas('status', function($query) {  
            $query->where('name', '!=', 'completed');
        })->where('id', '!=', 82)->get();

        foreach( $projects as $project ) {

            // due date
            $dueDate = new Carbon( $project->due_date );
            $nowDate = Carbon::now();
            $diff = $dueDate->diffInDays($nowDate);

            //get the users assigned in the project
            $usersArr = []; 
            foreach( $project->cards()->get() as $card ) {
                if( $card->users()->count() > 0 ) {
                    foreach( $card->users()->get() as $user ) {
                        if( !in_array($user->id, $usersArr) ) {
                            $usersArr[] = $user->id;
                        }
                    }
                }
            }

            if( $diff <= 3 ) {                
                // send notifications to assigned users
                if( count( $usersArr ) > 0 ) {                
                    foreach($usersArr as $user) {
                        $notification = new Notification;
                        $notification->user_id = $user;
                        $notification->contextable_type = Project::class;
                        $notification->contextable_id = $project->id;
                        $notification->contextable_value = 'project';
                        $notification->status = 'warning';
                        $notification->description = 'Due Date is Nearing for ' . $project->name . ' under ' . $project->client->company_name;
                        $notification->is_read = 0;

                        $notification->save();
                    }
                }
            }


        }
    }
}
