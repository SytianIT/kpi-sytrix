<?php

namespace App\Console\Commands;

use App\Applications\Sytrix\TimeTrack;
use App\Applications\Sytrix\Notification;
use App\User;
use Carbon\Carbon;

use Illuminate\Console\Command;

class NotifyTimeTrack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:undertime-tracks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check tracks with undertime log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   

        \Log::info('Check Undertime tracks');

        foreach( User::all() as $user ) {

            // get the yesterday timetracks of user
            $hoursRendered = $user->timeTracks()->whereDate('date', '=', Carbon::yesterday()->format('Y-m-d'))->sum('hours_rendered');

            $mins = (intval($hoursRendered) % 60);
            $hours = (floor($hoursRendered / 60) );

            if( $hours < 7 && $mins < 45 ) {

                $notification = new Notification;
                $notification->user_id = $user->id;
                $notification->contextable_type = TimeTrack::class;
                $notification->contextable_id = $user->id;
                $notification->contextable_value = 'timetrack';
                $notification->status = 'danger';
                $notification->description = 'Insufficient hours rendered last ' . Carbon::yesterday()->format('M d, Y') . ' with only ' . $hours . ' hours and ' . $mins . ' minutess on your log.';
                $notification->is_read = 0;

                $notification->save();

            }

        }

    }
}
