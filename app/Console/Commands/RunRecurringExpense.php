<?php

namespace App\Console\Commands;

use App\Applications\Expense\Expense;
use App\Applications\Expense\Recurring;
use App\Syion;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class RunRecurringExpense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:recurring-expense';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CRON Job for Recurring Expenses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $recurrings = Recurring::orderBy('created_at', 'DESC')->active()->get();

        foreach($recurrings as $recurring){
            $now = Carbon::today();
            if($recurring->next_expense_recurring == $now){
                $recurringTodayDate = $recurring->next_expense_recurring;
                $recurring->total_expense_recurring = $recurring->total_expense_recurring + $recurring->expense->amount;
                $recurring->count_expense_recurring = $recurring->count_expense_recurring + 1;
                if($recurring->recurring_times != 0 && $recurring->count_expense_recurring == $recurring->recurring_times){
                    $recurring->status = Recurring::EXPIRED;
                    $recurring->save();
                } else {
                    $recurring->next_expense_recurring = $recurring->generateNextExpenseRecurring($recurring->next_expense_recurring, $recurring->recurring_every, $recurring->recurring_of, $recurring->recurring_times);
                }
                $recurring->save();

                $newExpense = new Expense;
                $newExpense->title = $recurring->expense->title;
                $newExpense->description = $recurring->expense->description;
                $newExpense->amount = $recurring->expense->amount;
                $newExpense->amount_in_php = $recurring->expense->amount_in_php;
                $newExpense->attachments = $recurring->expense->attachments;
                $newExpense->date_occured = $recurringTodayDate;
                $newExpense->main_recurring_id = $recurring->id;
                $newExpense->currency()->associate($recurring->expense->currency);
                $newExpense->category()->associate($recurring->expense->category);
                $newExpense->payment()->associate($recurring->expense->payment);
                $newExpense->user()->associate($recurring->expense->user);
                $newExpense->save();
                // $newExpense->insertAuditLog('Recurring expense entitled "'.$newExpense->title.'" occured.', Syion::getThisSyion(Syion::EXPENSE), Auth::user());

                Log::info("Expense entitled '{$recurring->expense->title}' was successfully occured amounting to '{$recurring->expense->amount}'");
            }
        }
    }
}
