<?php

namespace App\Console;

use App\Applications\Sytrix\Project;
use App\Applications\Sytrix\Notification;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Command for Expense Application
        Commands\RunRecurringExpense::class,
        Commands\CheckDueDates::class,
        Commands\CheckOverdueDates::class,
        Commands\CheckQuota::class,
        Commands\NotifyQuota::class,
        Commands\NotifyTimeTrack::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /**
         * General Scheduled Commands
         */
        $schedule->command('backup:run')->dailyAt('23:00');
        $schedule->command('backup:clean')->daily()->at('01:00');

        /**
         * Scheduled Commands for Expense Application
         */
        $schedule->command('run:recurring-expense')
                 ->daily();


        /**
        * Scheduled Commands for sytrix
        **/
        // due dates
        $schedule->command('check:due-dates')->dailyAt('23:00');
        $schedule->command('check:overdue-dates')->dailyAt('11:00');

        // quotas
        $schedule->command('check:quota')->dailyAt('6:00');
        $schedule->command('notify:nearing-quota')->dailyAt('6:00');

        // Time Tracks
        $schedule->command('check:undertime-tracks')->dailyAt('6:00');
    }

}
