<?php

namespace App;

use App\Syion;
use App\User;
use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    protected $table = 'audit_logs';

    public function auditable()
    {
    	return $this->morphTo();
    }

    public function syion()
    {
    	return $this->belongsTo(Syion::class, 'syion_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeThisApp($query, $app)
    {
        return $query->whereHas('syion', function($q) use($app){
                        return $q->where('name', '=', $app);
                    });
    }
}
