@extends('templates.master')

@section('css')
<style type="text/css">
	html,body {
		height: 100%;
	}
</style>
@stop

@section('guest')
<div class="center-vertical bg-black">
	<div class="center-content">
		<div class="col-md-5 col-sm-5 col-xs-11 center-margin">
			<h3 class="text-center pad25B font-gray font-size-23">Sytian IT Solutions Project Timesheet <span class="opacity-80">v1.0</span></h3>
			<div id="login-form" class="content-box">
				{!! Form::open(['route' => 'password.reset', 'method' => 'POST', 'id' => 'form-login']) !!}
				<input type="hidden" name="token" value="{{ $token }}">
				<div class="content-box-wrapper pad20A">
					<div class="form-group">
						<label for="exampleInputEmail1">EMAIL ADDRESS:</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon addon-inside bg-white font-primary">
								<i class="glyph-icon icon-envelope-o"></i>
							</span>
							<input type="email" class="form-control {{ $errors->has('email') ? 'parsley-error' : '' }}" id="exampleInputEmail1" placeholder="Enter email" name="email" value="{{ old('email') ?: (Request::has('email')) ? Request::input('email') : null  }}">
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">ENTER HERE YOUR NEW PASSWORD:</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon addon-inside bg-white font-primary">
								<i class="glyph-icon icon-unlock-alt"></i>
							</span>
							<input type="password" class="form-control {{ $errors->has('password') ? 'parsley-error' : '' }}" id="exampleInputPassword1" name="password" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">CONFIRM PASSWORD:</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon addon-inside bg-white font-primary">
								<i class="glyph-icon icon-unlock-alt"></i>
							</span>
							<input type="password" class="form-control {{ $errors->has('password_confirmation') ? 'parsley-error' : '' }}" id="exampleInputPassword1" name="password_confirmation" placeholder="Confirm password">
						</div>
					</div>
					<div class="form-group">
						@if($errors->count() > 0)
						<div class="alert alert-danger">
							@if ($errors->has('email'))
		                    <p>{{ $errors->first('email') }}</p>
		                    @endif
							@if ($errors->has('password'))
		                    <p>{{ $errors->first('password') }}</p>
		                    @endif
		                    @if(session('status'))
                                <div class="alert confirmation alert-dismissible" role="alert">
                                    <p>{{session('status')}}</p>
                                </div>
                            @endif
		                </div>
		                @endif
					</div>
				</div>
				<div class="button-pane">
					<button type="submit" class="btn btn-block btn-success">RESET</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop