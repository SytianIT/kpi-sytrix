@extends('templates.master')

@section('css')
<style type="text/css">
	html,body {
		height: 100%;
	}
</style>
@stop

@section('guest')
<div class="center-vertical bg-black">
	<div class="center-content">
		<div class="col-md-5 col-sm-5 col-xs-11 center-margin">
			<h3 class="text-center pad25B font-gray font-size-23">SyIOn <span class="opacity-80">v1.0</span></h3>
			<div id="login-form" class="content-box">
				{!! Form::open(['route' => 'auth.login', 'method' => 'POST', 'id' => 'form-login']) !!}
				<div class="content-box-wrapper pad20A">

					<div class="form-group">
						<label for="exampleInputEmail1">EMAIL ADDRESS:</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon font-size-17 bg-white font-primary">
								<i class="glyph-icon icon-envelope-o"></i>
							</span>
							<input type="email" class="form-control {{ $errors->has('email') ? 'parsley-error' : '' }}" id="exampleInputEmail1" placeholder="Enter email" name="email" value="{{ old('email') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">PASSWORD:</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon font-size-17 bg-white font-primary">
								<i class="glyph-icon icon-unlock-alt"></i>
							</span>
							<input type="password" class="form-control {{ $errors->has('password') ? 'parsley-error' : '' }}" id="exampleInputPassword1" name="password" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						@if($errors->count() > 0)
						<div class="alert alert-danger">
							@if ($errors->has('email'))
		                    <p>{{ $errors->first('email') }}</p>
		                    @endif
							@if ($errors->has('password'))
		                    <p>{{ $errors->first('password') }}</p>
		                    @endif
		                </div>
		                @endif
					</div>
					<div class="row">
						<div class="checkbox-primary col-md-6" style="height: 20px;">
							<label>
								<input type="checkbox" id="loginCheckbox1" class="custom-checkbox" name="remember">
								Remember me
							</label>
						</div>
						<div class="text-right col-md-6">
							<a href="#" class="switch-button" switch-target="#login-forgot" switch-parent="#login-form" title="Recover password">Forgot your password?</a>
						</div>
					</div>
				</div>
				<div class="button-pane">
					<button type="submit" class="btn btn-block btn-success">Login</button>
				</div>
				{!! Form::close() !!}
			</div>
			<div id="login-forgot" class="content-box modal-content hide">
				{!! Form::open(['route' => 'send.reset.link', 'method' => 'POST', 'id' => 'form-password-reset', 'class' => 'form-validation-true']) !!}
				<div class="content-box-wrapper pad20A">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address:</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon addon-inside bg-white font-primary">
								<i class="glyph-icon icon-envelope-o"></i>
							</span>
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email">
						</div>
					</div>
					<div class="form-group hidden validation-group">
					</div>
				</div>
				<div class="button-pane text-center">
					<button type="submit" id="reset-password-btn" class="btn btn-md btn-success" data-email-validate-uri="{{ route('password.reset.validate.email') }}">Recover Password</button>
					<a href="#" class="btn btn-md btn-link switch-button" switch-target="#login-form" switch-parent="#login-forgot" title="Cancel">Cancel</a>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop