@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.categories') }}">Projext Categories</a></li>
	<li class="active">Edit</li>
@stop

@section('page-heading')
Edit Project Category
@stop

@section('main')
@include('templates.inc.notification')

	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
	            {!! Form::open(['route' => array('sytrix.category.update', $currentProjectCat->id), 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true']) !!}

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Parent Categories <span class="req">*</span></label>
	                <div class="col-sm-6">
	                	<select name="parent_id" class="form-control">
	                		<option value="" selected disabled>Select Parent Category</option>
	                		<option value="0">None</option>
	                		@forelse( $parentcategories as $parentcategory ) 
	                			@include('sytrix-app.category.option-edit-subcat')
	                		@empty
	                		@endforelse
	                	</select>
	                </div>
	            </div>

	          	<div class="form-group">
	                <label class="col-sm-3 control-label">Name <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <input type="text" name="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $currentProjectCat->title }}">
	                    @if ($errors->has('title'))
	                    <label class="error">{{ $errors->first('title') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Description</label>
	                <div class="col-sm-6">
	                    <textarea name="description" class="form-control {{ ($errors->has('description')) ? 'parsley-error' : '' }}">{{ $currentProjectCat->description }}</textarea>
	                    @if ($errors->has('description'))
	                    <label class="error">{{ $errors->first('description') }}</p>
	                    @endif
	                </div>
	            </div>

	             <div class="form-group">
	                <label class="col-sm-3 control-label">&nbsp;</label>
	                <div class="col-sm-6 text-right">
	                    <a href="{{ route('sytrix.categories') }}" class="btn btn-sm btn-danger">CANCEL</a>
	                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
	                </div>
	            </div>

	            {!! Form::close() !!}
	        </div>
		</div>
	</div>

@stop