<tr>
	<td>
		@if( $projectCategory->depth ==0 )
			<strong>
		@endif
		{{ str_repeat('-', $projectCategory->depth) . $projectCategory->title }}
		@if( $projectCategory->depth ==0 )
			</strong>
		@endif
	</td>
	<td>{{ $projectCategory->description }}</td>
	<td class="text-center">	
		<a href="{{ route('sytrix.category.edit', $projectCategory->id) }}" class="btn btn-xs btn-info">EDIT</a>
		@if( $projectCategory->children()->count() == 0 ) 
			<a href="{{ route('sytrix.category.delete', $projectCategory->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
		@else 
			<button data-confirm-message="Deletion not allowed" data-confirm-text="Deletion of categorie(s) with subcategorie(s) is not permitted" class="btn btn-xs btn-danger deleteAlert">DELETE</button>
		@endif
	</td>
</tr>

@if( $projectCategory->children()->count() > 0 )
	@foreach( $projectCategory->children()->get() as $projectCategory )	
		@include('sytrix-app.category.subcat', ['parentCat' => $projectCategory])
	@endforeach
@endif