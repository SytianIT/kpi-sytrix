<option value="{{ $parentcategory->id }}" {{ ($parentcategory->depth == 0) ? 'class=font-bold' : null }} >{{ str_repeat("-", $parentcategory->depth).$parentcategory->title }}</option>

@if( $parentcategory->children()->count() > 0 ) 
	@forelse( $parentcategory->children()->get() as $parentcategory )
		@include('sytrix-app.category.options-subcat')
	@empty
	@endforelse
@endif