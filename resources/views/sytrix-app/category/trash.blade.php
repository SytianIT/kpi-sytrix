@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.categories') }}">Project Categories</a></li>
	<li class="active">Trash</li>
@stop

@section('page-heading')
Project Categories - Trash
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<a href="{{ route('sytrix.category.create') }}" class="btn btn-sm btn-success">Create Category</a>

					<a href="{{ route('sytrix.category.trash') }}" class="view-trash">
						<span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span>
					</a>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Title</th>
				    		<th>Description</th>
				    		<th class="text-center">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@forelse($projectCategories as $projectCategory)
					    	<tr>
								<td>{{ str_repeat('-', $projectCategory->depth) . $projectCategory->title }}</td>
								<td>{{ $projectCategory->description }}</td>
								<td class="text-center">	
									<a href="{{ route('sytrix.category.restore', $projectCategory->id) }}" class="btn btn-xs btn-success confirmAlert">RESTORE</a>
								</td>
							</tr>
				    	@empty
					    	<tr>
					    		<td colspan="6">No records found!</td>
					    	</tr>
				    	@endforelse
				    </tbody>
			    </table>
			</div>
		</div>
	</div>
@stop