@if( $currentProjectCat->id != $parentcategory->id )
	<option value="{{ $parentcategory->id }}" {{( $currentProjectCat->parent_id == $parentcategory->id ) ? 'selected' : null}}  {{( $parentcategory->depth == 0 ) ? 'class=font-bold' : null }}>{{  str_repeat('-', $parentcategory->depth) . $parentcategory->title }}</option>
@endif

@if( $parentcategory->children()->count() > 0 )
	@foreach( $parentcategory->children()->get() as $parentcategory )
		@if( $currentProjectCat->id != $parentcategory->id )
			@include('sytrix-app.category.option-edit-subcat')
		@endif
	@endforeach
@endif