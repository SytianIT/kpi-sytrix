<option	
	@if( isset( $project->status ) )
		@if( ( $status->id == $project->status->id) ) 
			{{ 'selected' }}
		@endif
	@else 
		@if( $status->id == 25 )
			{{ 'selected' }}
		@endif
	@endif

value="{{ $status->id }}" data-status="{{ $status->name }}" {{ ($status->depth == 0) ? "class=font-bold" : "" }}>
{{  str_repeat('-', $status->depth) . " " . $status->title }}
</option>

@if( $status->children()->count() > 0 ) 
	@forelse( $status->children()->get() as $status )
		@include('sytrix-app.project.subcat')
	@empty
	@endforelse
@endif