@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li class="active">Projects</li>
@stop

@section('page-heading')
	Projects
@stop

@section('main')
@include('templates.inc.notification')

	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">

				<div>{{ $projects->links() }}</div>
			
				<div class="size-md">
					<a href="{{ route('sytrix.project.create') }}" class="btn btn-sm btn-success">Add Project</a>
					{!! Form::open(['route' => 'sytrix.projects', 'method' => 'GET', 'class' => 'form-horizontal form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
						 		<input type="text" name="search_project" placeholder="Search" class="form-control"> 		
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
					 <a href="{{ route('sytrix.project.archive') }}" class="view-archives">
						<span class="glyph-icon tooltip-button demo-icon icon-archive" data-original-title="View Archives" title=""></span>
					</a>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover {{ ($projects->count() > 1) ? 'dataTable' : '' }}" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Client</th>
				    		<th>Name</th>
				    		<th>Start Date</th>
				    		<th>Due Date</th>
				    		<th>Category</th>
				    		<th>Status</th>
				    		<th class="nosort">Total Hours</th>
				    		<th class="text-center nosort">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
					    @forelse( $projects as $project )
					    	<tr>					    
					    		<td>{{ ($project->client()->count() > 0) ? $project->client->company_name : '' }}</td>		
						    	<td>{{ $project->name }}</td>
						    	<td>{{ date('M d, Y', strtotime($project->start_date)) }}</td>
						    	<td>{{ date('M d, Y', strtotime($project->due_date)) }}</td>
						    	<td>
						    		@foreach( $project->categories as $category ) 
						    			{{ $category->title }} <br>
						    		@endforeach
						    	</td>
						    	<td>
						    		@if( $project->returnStatus() ) 
						    			@if( isset( $project->returnStatus()['parentStatus'] ) )
								    		<strong>{{ $project->returnStatus()['parentStatus']->title }}</strong><br>
								    		<small>-{{ $project->returnStatus()['currentStatus']->title }}</small>
								    	@else 
								    		{{  $project->returnStatus()['currentStatus']->title }}
						    			@endif
						    		@endif
						    	</td>
						    	<td>
						    		{{ $project->totalHoursHTML() }}
						    	</td>
						    	<td class="text-center">
						    		@if( $project->client )
							    		<a href="{{ route('client.edit', $project->client->id) }}" class="btn btn-xs btn-azure">Client</a>
						    		@endif
						    		<a href="{{ route('sytrix.project.tasks', $project->id) }}" class="btn btn-xs btn-primary">Tasks</a>
					    			<a href="{{ route('sytrix.project.edit', $project->id) }}" class="btn btn-xs btn-info">Edit</a>
					    			<a href="{{ route('sytrix.project.movetoarchive', $project->id) }}" class="btn btn-xs btn-danger confirmAlert" data-confirm-message="Move to Archives?">Archive</a>
					    		</td>
					    	</tr>
					    @empty
					    	<tr><td colspan="8">No records found!</td></tr>
					    @endforelse 
				    </tbody>
			    </table>		
			    <div>{{ $projects->links() }}</div>	    
			</div>
		</div>
	</div>
@stop