@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.projects') }}">Projects</a></li>
	<li class="active">Archives</li>
@stop

@section('page-heading')
	Projects - Archives
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<a href="{{ route('sytrix.project.create') }}" class="btn btn-sm btn-azure">Add Project</a>
					{!! Form::open(['route' => 'sytrix.project.archive', 'method' => 'GET', 'class' => 'form-horizontal form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
						 		<input type="text" name="search_project" placeholder="Search Project Name" class="form-control">
						 	</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
					 <a href="{{ route('sytrix.project.archive') }}" class="view-archives">
						<span class="glyph-icon tooltip-button demo-icon icon-archive" data-original-title="View Archives" title=""></span>
					</a>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered {{ $projects->count() > 0 ? 'dataTable' : '' }}" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Name</th>
				    		<th class="nosort">Description</th>
				    		<th>Start Date</th>
				    		<th>Due Date</th>
				    		<th class="nosort">Category</th>
				    		<th class="nosort">Status</th>
				    		<th class="text-center nosort">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
				  		 @forelse( $projects as $project )
					    	<tr>					    		
						    	<td>{{ $project->name }}</td>
						    	<td>{{ $project->description }}</td>
						    	<td>{{ date('M d, Y', strtotime($project->start_date)) }}</td>
						    	<td>{{ date('M d, Y', strtotime($project->due_date)) }}</td>
						    	<td>
						    		@foreach( $project->categories as $category ) 
						    			{{ $category->title }} <br>
						    		@endforeach
						    	</td>
						    	<td>
						    		@if( $project->returnStatus() ) 
						    			@if( isset( $project->returnStatus()['parentStatus'] ) )
								    		<strong>{{ $project->returnStatus()['parentStatus']->title }}</strong><br>
								    		<small>-{{ $project->returnStatus()['currentStatus']->title }}</small>
								    	@else 
								    		<small>{{  $project->returnStatus()['currentStatus']->title }}</small>
						    			@endif
						    		@endif
						    	</td>
						    	<td class="text-center">
					    			<a href="{{ route('sytrix.project.restore', $project->id) }}" class="btn btn-xs btn-success confirmAlert">RESTORE</a>
					    			<a href="{{ route('sytrix.project.delete', $project->id) }}" class="btn btn-xs btn-danger confirmAlert" data-confirm-message="Permanently Delete Project?">DELETE</a>
					    		</td>
					    	</tr>
						    @empty
						    	<tr><td colspan="8">No records found!</td></tr>
						    @endforelse
				    </tbody>
			    </table>
			</div>
		</div>
	</div>
@stop