<option>{{ str_repeat('-', $projcatChild->depth) . $projcatChild->title }}</option>

@if( $projcatChild->children()->count() > 0 )
	@foreach( $projcatChild->chilren()->get() as $projcatChild )
		@include('sytrix-app/project/projcat-sub')
	@endforeach
@endif