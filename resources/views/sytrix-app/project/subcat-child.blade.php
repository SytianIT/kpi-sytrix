<option value="{{ $status->id }}">{{  str_repeat('-', $status->depth) . " " . $status->title }}</option>

@if( $status->children()->count() > 0 ) 
	@forelse( $status->children()->get() as $status )
		@include('sytrix-app.project.subcat-child')
	@empty
	@endforelse
@endif