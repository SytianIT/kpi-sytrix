@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.projects') }}">Projects</a></li>
	<li class="active">Edit</li>
@stop

@section('page-heading')
Projects - Edit
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
	            {!! Form::open(['route' => array('sytrix.project.update', $project->id), 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true']) !!}

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Client <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <select name="client_id" class="form-control select2 {{ ($errors->has('client_id')) ? 'parsley-error' : '' }}">
	                    	<option value="" selected disabled>Select Client</option>
	                    	@forelse( $clients as $client )
	                    		<option value="{{ $client->id }}" {{ ($client->id == $project->client_id) ? 'selected' : null }}>{{ $client->company_name }}</option>
	                    	@empty
	                    	@endforelse
	                    </select>
	                    @if ($errors->has('client_id'))
	                    <label class="error">{{ $errors->first('client_id') }}</p>
	                    @endif
	                </div>
	            </div>

	          	 <div class="form-group">
	                <label class="col-sm-3 control-label">Project Name <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <input type="text" name="name" class="form-control {{ ($errors->has('name')) ? 'parsley-error' : '' }}" value="{{ $project->name }}">
	                    @if ($errors->has('name'))
	                    <label class="error">{{ $errors->first('name') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Description</label>
	                <div class="col-sm-6">
	                    <textarea name="description" class="form-control {{ ($errors->has('description')) ? 'parsley-error' : '' }}">{{ $project->description }}</textarea>
	                    @if ($errors->has('description'))
	                    <label class="error">{{ $errors->first('description') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Dates <span class="req">*</span></label>
	                <div class="col-sm-3">
	                    <input type="text" name="start_date" id="startDate" class="form-control date-range-picker {{ ($errors->has('start_date')) ? 'parsley-error' : '' }}" placeholder="Start Date" value="{{ date('m/d/o', strtotime( $project->start_date )) }}">
	                    @if ($errors->has('start_date'))
	                    <label class="error">{{ $errors->first('start_date') }}</p>
	                    @endif
	                </div>
	                <div class="col-sm-3">
	                    <input type="text" name="due_date" id="dueDate" class="form-control date-range-picker {{ ($errors->has('due_date')) ? 'parsley-error' : '' }}" placeholder="Due Date" value="{{ date('m/d/o', strtotime( $project->due_date )) }}">
	                    <input type="hidden" id="store_dueDate">
	                    @if ($errors->has('due_date'))
	                    <label class="error">{{ $errors->first('due_date') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Durations (Hours)<span class="req">*</span></label>
	                <div class="col-sm-2">
	                    <input type="number" name="max_development_time" keyup="return isNumberKey(event);" class="numericInput form-control project-time {{ ($errors->has('max_development_time')) ? 'parsley-error' : '' }}" placeholder="Max Development Time" value="{{ $project->max_development_time }}">
	                    @if ($errors->has('max_development_time'))
	                    <label class="error">{{ $errors->first('max_development_time') }}</p>
	                    @endif
	                </div>
	                 <div class="col-sm-2">
	                    <input type="number" name="max_design_time" class="numericInput form-control project-time {{ ($errors->has('max_design_time')) ? 'parsley-error' : '' }}" placeholder="Max Design Time" value="{{ $project->max_design_time }}">
	                    @if ($errors->has('max_design_time'))
	                    <label class="error">{{ $errors->first('max_design_time') }}</p>
	                    @endif
	                </div>
	                <div class="col-sm-2">
	                    <input type="number" name="max_pm_time" class="numericInput form-control project-time {{ ($errors->has('max_pm_time')) ? 'parsley-error' : '' }}" placeholder="Max PM Time" value="{{ $project->max_pm_time }}">
	                    @if ($errors->has('max_pm_time'))
	                    <label class="error">{{ $errors->first('max_pm_time') }}</p>
	                    @endif
	                </div>
	            </div>
	       		
	             <div class="form-group">
	                <label class="col-sm-3 control-label">Category <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <select name="project_category_id[]" multiple class="form-control multi-select {{ ($errors->has('project_category_id')) ? 'parsley-error' : '' }}">
	                    	<option value="" selected disabled>Selected Categories</option>
	                    	@forelse( $categories as $category ) 
	                    		<option {{ $project->categories->contains($category->id) ? 'selected' : null }} value="{{ $category->id }}">{{ $category->title }}</option>
	                    	@empty
	                    		Categories not found..
	                    	@endforelse
	                    </select>
	                    @if ($errors->has('project_category_id'))
	                    <label class="error">{{ $errors->first('project_category_id') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Departments <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <select name="department_id[]" multiple class="form-control multi-select {{ ($errors->has('department_id')) ? 'parsley-error' : '' }}">
	                    	<option value="" selected disabled>Selected Departments</option>
	                    	@forelse( $departments as $department ) 
	                    		<option {{ $project->departments->contains($department->id) ? 'selected' : null }} value="{{ $department->id }}">{{ $department->title }}</option>
	                    	@empty
	                    		Departments not found..
	                    	@endforelse
	                    </select>
	                    @if ($errors->has('department_id'))
	                    <label class="error">{{ $errors->first('department_id') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Project Manager</label>
					<div class="col-sm-6">
						<select name="pm_id" class="form-control select2">
							<option selected disabled></option>
							@forelse( $departments as $department )
								<optgroup label="{{ $department->title }}">
									@forelse( $department->users()->get() as $employee )
										<option {{ ($employee->id == $project->pm_id) ? 'selected' : null }} value="{{ $employee->id }}">{{ $employee->firstname . '&nbsp;' . $employee->lastname }}</option>
									@empty
										<option disabled>Employees not found</option>
									@endforelse
								</optgroup>
							@empty
								@forelse( $employees as $employee )
									<option value="{{ $employee->id }}">{{ $employee->firstname . '&nbsp;' . $employee->lastname }}</option>
								@empty
									<option disabled>Employees not found</option>
								@endforelse
							@endforelse
						</select>
					</div>
	            </div>

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Status <span class="req">*</span></label>
					<div class="col-sm-6">
						<select name="project_status_id" class="form-control select2 {{ ($errors->has('project_status_id')) ? 'parsley-error' : '' }}">
							<option value="" selected disabled>Project Status</option>
							@forelse( $statuses as $status )
						    	@include('sytrix-app.project.subcat')
							@empty
							@endforelse
						</select>
						@if ($errors->has('project_status_id'))
						<label class="error">{{ $errors->first('project_status_id') }}</p>
						@endif
					</div>
	            </div>

	            <div class="form-group form-project-status {{ ($project->project_status_id == 38) ? 'completed' : ''}}">
	            	<label class="col-sm-3 control-label">Date Completed <span class="req">*</span></label>
					<div class="col-sm-6">
					<input type="text" name="date_completed" id="dateCompleted" class="form-control date-picker {{ ($errors->has('date_completed')) ? 'parsley-error' : '' }}" value="{{ date('m/d/o', strtotime($project->date_completed)) }}">
						@if ($errors->has('date_completed'))
						<label class="error">{{ $errors->first('date_completed') }}</p>
						@endif
					</div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">&nbsp;</label>
	                <div class="col-sm-6 text-right">
	                    <a href="{{ route('sytrix.projects') }}" class="btn btn-sm btn-danger">CANCEL</a>
	                    <button type="submit" class="btn btn-success btn-sm">UPDATE</button>
	                </div>
	            </div>

	            {!! Form::close() !!}
	        </div>
		</div>
	</div>
@stop