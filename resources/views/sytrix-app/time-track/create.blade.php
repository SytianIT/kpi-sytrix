@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li class="active">Time Tracks</li>
@stop

@section('page-heading')
Time Tracks - Create
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
	            {!! Form::open(['route' => 'sytrix.timetrack.store', 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true']) !!}	         

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Employee <span class="req">*</span></label>
					<div class="col-sm-6">
						<select name="user" class="form-control select2 tt-member {{ ($errors->has('user')) ? 'parsley-error' : '' }}">
								<option selected disabeled></option>
							@foreach( $departments as $department )
								@if( $department->users()->count() > 0 )
									<optgroup label="{{ $department->title }}">
										@forelse( $department->users()->get() as $user )
											<option value="{{ $user->id }}" {{ ( old('user') == $user->id ) ? 'selected' : '' }}>{{ $user->firstname . " " . $user->lastname }}</option>
										@empty
											<option disabled>Members not found</option>
										@endforelse
									</optgroup>
								@endif
							@endforeach
						</select>
						@if ($errors->has('user'))
						<label class="error">{{ $errors->first('user') }}</p>
						@endif
					</div>
	            </div>  

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Client <span class="req">*</span></label>
					<div class="col-sm-6">
						<select name="client" class="form-control select2 tt-client {{ ($errors->has('client')) ? 'parsley-error' : '' }}">
							<option selected disabled></option>
							@forelse( $clients as $client )
								<option value="{{$client->id}}">{{$client->company_name}}</option>
							@empty
								<option disabled>Clients not found.</option>
							@endforelse
						</select>
						@if ($errors->has('client'))
						<label class="error">{{ $errors->first('client') }}</p>
						@endif
					</div>
	            </div>

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Project <span class="req">*</span></label>
					<div class="col-sm-6">
						<select name="project" class="form-control select2 tt-project {{ ($errors->has('project')) ? 'parsley-error' : '' }}">
						</select>
						@if ($errors->has('project'))
						<label class="error">{{ $errors->first('project') }}</p>
						@endif
					</div>
	            </div>

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Task <span class="req">*</span></label>
					<div class="col-sm-6">
						<select name="task" class="form-control select2 tt-task {{ ($errors->has('task')) ? 'parsley-error' : '' }}">
						</select>
						@if ($errors->has('task'))
						<label class="error">{{ $errors->first('task') }}</p>
						@endif
					</div>
	            </div>

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Date Rendered <span class="req">*</span></label>
					<div class="col-sm-6">
					<input type="text" class="form-control date-picker date-rendered {{ ($errors->has('date')) ? 'parsley-error' : '' }}" placeholder="Date" value="{{ old('date') }}" name="date">
						@if ($errors->has('date'))
						<label class="error">{{ $errors->first('date') }}</p>
						@endif
					</div>
	            </div>

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Time Log <span class="req">*</span></label>
					<div class="col-sm-3">
						<input type="text" class="form-control clock-picker time-in {{ ($errors->has('time_in')) ? 'parsley-error' : '' }}" placeholder="Time In" name="time_in" value="{{ old('time_in') }}">
						@if ($errors->has('time_in'))
						<label class="error">{{ $errors->first('time_in') }}</p>
						@endif
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control clock-picker time-out {{ ($errors->has('time_out')) ? 'parsley-error' : '' }}" placeholder="Time Out" name="time_out" value="{{ old('time_out') }}">
						@if ($errors->has('time_out'))
						<label class="error">{{ $errors->first('time_out') }}</p>
						@endif
					</div>
	            </div>      

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Hours Rendered <span class="req">*</span></label>
					<div class="col-sm-6">
						<div class="row">
							<div class="col-xs-6">
								<input type="text" name="hours" placeholder="Hours" value="{{ old('hours') }}" class="numericInput form-control {{ ($errors->has('hours')) ? 'parsley-error' : '' }}">
							</div>
							<div class="col-xs-6">
								<input type="text" name="mins" placeholder="Mins" value="{{ old('mins') }}" class="numericInput form-control {{ ($errors->has('mins')) ? 'parsley-error' : '' }}">	
							</div>
						</div>
						@if ($errors->has('mins') || $errors->has('hours'))
							@if( $errors->has('mins') )
								<label class="error">{{ $errors->first('mins') }}</p>
							@else
								<label class="error">{{ $errors->first('hours') }}</p>
							@endif
						@endif
					</div>
	            </div>	            

	            <div class="form-group">
	            	<label class="col-sm-3 control-label">Reason for manual entry <span class="req">*</span></label>
					<div class="col-sm-6">
						<textarea name="note" class="form-control note {{ ($errors->has('note')) ? 'parsley-error' : '' }}">{{ old('note') }}</textarea>
						@if ($errors->has('note'))
						<label class="error">{{ $errors->first('note') }}</p>
						@endif
					</div>
	            </div>    

	            <div class="form-group">
	            	<div class="hidden timeables">
	            		<input type="hidden" class="time-id" name="time_id">
	            		<input type="hidden" class="time-type" name="time_type">
	            	</div>
	                <label class="col-sm-3 control-label">&nbsp;</label>
	                <div class="col-sm-6 text-right">
	                    <a href="javascript:history.back()" class="btn btn-sm btn-danger">CANCEL</a>
	                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
	                </div>
	            </div>  

	            {!! Form::close() !!}
	        </div>
		</div>
	</div>
@stop