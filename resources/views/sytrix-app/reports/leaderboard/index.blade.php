@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.reports.tracks') }}">Reports</a></li>
	<li class="active">Leaderboard</li>
@stop

@section('page-heading')
	Leaderboard
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<ul class="nav nav-tabs">
                    <li class="active"><a href="javascript:void">Employee<div class="ripple-wrapper"></div></a></li>
                    <li><a href="">Team<div class="ripple-wrapper"></div></a></li>
                </ul>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Name</th>
				    		<th>Total Projects</th>
				    		<th>Total Hours</th>
				    		<th>Projects / Month (avg)</th>
				    		<th>Hours / Project (avg)</th>
				    		<th>Rank Points</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@forelse( $users as $user )
				    		<tr>
				    			<td>{{ $user->firstname . ' ' . $user->lastname }}</td>
				    			<td>{{ $user->completedProjects('total') }}</td>
				    			<td>{{ $user->totalHours() }}</td>
				    			<td>{{ $user->avgProjects() }}</td>
				    			<td>{{ $user->avgHoursPerProject() }}</td>
				    			<td>{{ $user->rankPoints() }}</td>
				    		</tr>
				    	@empty
				    	@endforelse
				    </tbody>
			    </table>
			</div>
		</div>
	</div>
@stop