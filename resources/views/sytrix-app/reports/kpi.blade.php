@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.reports.tracks') }}">Reports</a></li>
	<li class="active">KPI</li>
@stop

@section('page-heading')
	Key Performance Indicator
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md table-filter project-filter">
					{!! Form::open(['route' => 'sytrix.reports.kpi', 'method' => 'GET', 'class' => 'form-horizontal pull-left form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
					 			<input type="text" name="project_count" class="form-control" placeholder="Number of Projects">
					 		</div>
					 		<div class="form-group">
					 			<select class="select2 form-control" name="kpi_user">
					 				<option selected disabled>by Employee</option>
					 				@foreach( $employees as $employee )	
					 					<option value="{{ $employee->id }}">{{ $employee->firstname . '&nbsp;' . $employee->lastname }}</option>
					 				@endforeach
					 			</select>
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
				</div>

				@forelse( $employees as $key => $employee )		

					<div>
						<h4>{{ $employee->firstname . '  ' . $employee->lastname }}</h4>
						<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
						    <thead>
						    	<tr>
						    		<th class="nosort">Client</th>
						    		<th class="nosort">Project</th>
						    		<th class="nosort">Start Date</th>
						    		<th class="nosort">Due Date</th>
						    		<th class="nosort">Date Completed</th>
						    		<th class="nosort">Estimated Hours</th>
						    		<th class="nosort">Hours Rendered</th>
						    		<th class="nosort">Actions</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	@forelse( $employee->completedProjects()->take(3) as $project ) 

						    		<tr>
						    			<td>{{ $project->client->company_name }}</td>
						    			<td>{{ $project->name }}</td>
						    			<td>@formatDate( $project->start_date )</td>
						    			<td>@formatDate( $project->due_date )</td>
						    			<td>@formatDate( $project->updated_at )</td>
						    			<td>{{ $project->estimatedHours() }}</td>
						    			<td>@hoursRendered($employee->timeTracks()->where('project_id', $project->id)->sum('hours_rendered'))</td>
						    			<td>Actions</td>
						    		</tr>

						    	@empty
							    	<tr><td colspan="8">Projects not found..</td></tr>
						    	@endforelse
						    </tbody>
					    </table>



				    </div>
			    @empty

			    	<p class="lead">Employees not found..</p>

			    @endforelse

			</div>
		</div>
	</div>
@stop