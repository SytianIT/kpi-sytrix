@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.reports.tracks') }}">Reports</a></li>
	<li class="active">Time Tracks</li>
@stop

@section('page-heading')
	Time Tracks
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div>{{ $timeTracks->links() }}</div>
				<div class="size-md">
					{!! Form::open(['route' => 'sytrix.reports.tracks', 'method' => 'GET', 'class' => 'form-horizontal pull-left form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
					 			<input type="text" name="track_range" class="form-control daterange-picker" placeholder="From - To">
					 		</div>
					 		<div class="form-group">
								<select class="select2 form-control" name="track_employee">
									<option disabled selected>Select Employee</option>
									@foreach( $employees as $employee )
										<option value="{{ $employee->id }}">{{ $employee->firstname . ' ' . $employee->lastname  }}</option>
									@endforeach
								</select>					 		
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
					<a href="{{ route('sytrix.timetrack.create') }}" class="btn btn-sm btn-success pull-right">Add Log</a>
				</div>

				<ul class="nav nav-tabs">
                    <li class="active"><a href="javascript:void">All<div class="ripple-wrapper"></div></a></li>
                    <li><a href="{{ route('sytrix.reports.manual-tracks') }}">Manually Added / Edited<div class="ripple-wrapper"></div></a></li>
                    <li><a href="{{ route('sytrix.reports.employee-tracks') }}">By Employee<div class="ripple-wrapper"></div></a></li>
                </ul>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th class="nosort">Client Name</th>
				    		<th class="nosort">Project Name</th>
				    		<th class="nosort">Assigned Member</th>
				    		<th class="nosort">Task</th>
				    		<th class="nosort">Time In</th>
				    		<th class="nosort">Time Out</th>
				    		<th class="nosort">Hours Rendered</th>
				    		<th class="nosort">Date</th>
				    		<th class="text-center nosort">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@forelse( $timeTracks as $timeTrack )
				    		<tr>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>{{ $timeTrack->project->client->company_name }}</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>{{ $timeTrack->project->name }}</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>{{ $timeTrack->user->firstname . " " . $timeTrack->user->lastname }}</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>{{ $timeTrack->timeable ? $timeTrack->timeable->title : ''  }}</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>@formatTime($timeTrack->time_in)</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>@if( $timeTrack->time_out != null )  @formatTime($timeTrack->time_out) @else {{''}} @endif</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>@theHours($timeTrack->hours_rendered):@theMins($timeTrack->hours_rendered)</td>
				    			<td{{ ($timeTrack->manual_entry != null) ? ' class=edited' : '' }}>@formatDate($timeTrack->date)</td>
				    			<td class="text-center {{ ($timeTrack->manual_entry != null) ? 'edited' : '' }}">
					    			<a href="{{ route('sytrix.timetrack.edit', $timeTrack->id) }}" class="btn btn-xs btn-info">EDIT</a>
					    			<a href="" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
				    			</td>
				    		</tr>
				    	@empty
					    	<tr><td colspan="8">No records found!</td></tr>
				    	@endforelse
				    </tbody>
			    </table>

			    <div>{{ $timeTracks->links() }}</div>
			</div>
		</div>
	</div>
@stop