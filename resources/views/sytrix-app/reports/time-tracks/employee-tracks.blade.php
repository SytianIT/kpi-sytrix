@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.reports.tracks') }}">Reports</a></li>
	<li class="active">Time Tracks</li>
@stop

@section('page-heading')
	Time Tracks
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					{!! Form::open(['route' => 'sytrix.reports.employee-tracks', 'method' => 'GET', 'class' => 'form-horizontal pull-left form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
					 			<input type="text" name="track_range" class="form-control daterange-picker" placeholder="From - To">
					 		</div>
					 		<div class="form-group">
						 		<select name="track_employee" class="select2 form-control">
									<option disabled selected>Select Employee</option>
									@foreach( $employees as $employee )
										<option value="{{ $employee->id }}">{{ $employee->firstname . ' ' . $employee->lastname  }}</option>
									@endforeach
								</select>					 		
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
					<a href="{{ route('sytrix.timetrack.create') }}" class="btn btn-sm btn-success pull-right">Add Log</a>
				</div>

				<ul class="nav nav-tabs">
                    <li><a href="{{route('sytrix.reports.tracks')}}">All<div class="ripple-wrapper"></div></a></li>
                    <li><a href="{{ route('sytrix.reports.manual-tracks') }}">Manually Added / Edited<div class="ripple-wrapper"></div></a></li>
                    <li class="active"><a href="#">By Employee<div class="ripple-wrapper"></div></a></li>
                </ul>

                <div class="employee-tracks-wrapper">
	                @foreach( $usersTracks as $userName => $userTrack )
	                	<h4>{{ $userName }}</h4>
		                	
	                	<table id="user-{{ $userName }}" cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover {{ ( count($userTrack) > 0 ) ? 'data-with-paginate' : '' }}" id="datatable-example">
						    <thead>
						    	<tr>
						    		<th class="nosort">Date</th>
						    		<th class="nosort">Rendered Hours</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	@foreach( $userTrack as $dateTrack => $hours )
						    		<tr>
						    			<td>@formatDate($dateTrack)</td>
						    			<td><span {{ ( $hours < 465 ) ? 'class=inc' : '' }}>@hoursRendered($hours)</span></td>
						    		</tr>
						    	@endforeach
						    </tbody>
					    </table>			
	                @endforeach				
                </div>    
			</div>
		</div>
	</div>
@stop