<option class="font-bold" value="{{ $projectStatus->id }}">{{ str_repeat('-', $projectStatus->depth) . $projectStatus->title }}</option>

@if( $projectStatus->children()->count() > 0  )
	
	@foreach( $projectStatus->children()->get() as $projectStatus )
		@include('sytrix-app.reports.projects.projstat-sub')
	@endforeach

@endif