@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.reports.tracks') }}">Reports</a></li>
	<li class="active">Projects</li>
@stop

@section('page-heading')
	Projects
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div>{{ $projects->links() }}</div>
				<div class="size-md table-filter project-filter">
					{!! Form::open(['route' => 'sytrix.reports.projects', 'method' => 'GET', 'class' => 'form-horizontal pull-left form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
					 			<input type="text" name="project_search" class="form-control" placeholder="Search Project">
					 		</div>
					 		<div class="form-group">
								<select class="select2 form-control" name="project_status">
									<option disabled selected>Select Project Status</option>
									<option value="0">All</option>
									@foreach( $projectStatuses as $projectStatus )
										
										@include('sytrix-app.reports.projects.projstat-sub');

									@endforeach
								</select>					 		
					 		</div>
					 		<div class="form-group">
					 			<select class="select2 form-control" name="project_due">
					 				<option disabled selected>Project Due</option>
					 				<option value="on-time">On Time</option>
					 				<option value="delayed">Delayed</option>				
					 			</select>
					 		</div>
					 		<div class="form-group">
					 			<select class="select2 form-control" name="project_quota">
					 				<option disabled selected>Project Quota</option>
					 				<option value="exceeded">Exceeded</option>
					 				<option value="met">Met</option>				
					 			</select>
					 		</div>
					 		<div class="form-group">
					 			<select class="select2 form-control" name="project_user">
					 				<option selected disabled>by Employee</option>
					 				@foreach( $employees as $employee )	
					 					<option value="{{ $employee->id }}">{{ $employee->firstname . '&nbsp;' . $employee->lastname }}</option>
					 				@endforeach
					 			</select>
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
				</div>

				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th class="nosort">Client</th>
				    		<th class="nosort">Name</th>
				    		<th class="nosort">Project Manager</th>
				    		<th class="nosort">Estimated Hours</th>
				    		<th class="nosort">Hours Rendered (Development)</th>
				    		<th class="nosort">Hours Rendered (Design)</th>
				    		<th class="nosort">Start Date</th>
				    		<th class="nosort">Due Date</th>
				    		<th class="nosort">Status</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@forelse( $projects as $project )
				    		<tr>
				    			<td>{{ $project->client->company_name }}</td>
				    			<td>{{ $project->name }}</td>
				    			<td>
				    				@if( $project->pm()->count() > 0 ) 
				    					{{ $project->pm()->first()->firstname . '&nbsp;' . $project->pm()->first()->lastname }}
				    				@endif
				    			</td>
				    			<td>{{ $project->estimatedHours() }}</td>
				    			<td>{{ $project->timeTracksSumDev() }}</td>
				    			<td>{{ $project->timeTracksSumDesign() }}</td>
				    			<td>@formatDate($project->start_date)</td>
				    			<td>@formatDate($project->due_date)</td>
				    			<td>
				    				@if( $project->returnStatus() ) 
						    			@if( isset( $project->returnStatus()['parentStatus'] ) )
								    		<strong>{{ $project->returnStatus()['parentStatus']->title }}</strong><br>
								    		<small>-{{ $project->returnStatus()['currentStatus']->title }}</small>
								    	@else 
								    		{{  $project->returnStatus()['currentStatus']->title }}
						    			@endif
						    		@endif
				    			</td>
				    		</tr>
				    	@empty 
				    		<tr><td colspan="100">Projects not found!</td></tr>
				    	@endforelse
				    </tbody>
			    </table>
			    <div>{{ $projects->links() }}</div>
			</div>
		</div>
	</div>
@stop