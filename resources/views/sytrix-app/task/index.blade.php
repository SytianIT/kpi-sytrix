@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.projects') }}">Projects</a></li>
	<li class="active">{{ $project->name }}</li>
@stop


@section('page-heading')
	{{ $project->client->company_name }} - {{ $project->name }}
	<a href="{{ route('sytrix.card.trash', $project->id) }}" class="glyph-icon tooltip-button icon-archive pull-right card-archives" title="Task Archives"></a>
	<button id="inline-status" class="btn btn-sm btn-primary addchange-member pull-right popup-editable" data-title="Members" data-placement="left" data-contenttarget=".members-content" data-type="html">Users</button>
	<a href="{{ route('sytrix.project.edit', $project->id) }}" class="btn btn-sm btn-info pull-right">Project Details</a>
@stop

@section('main')
@include('templates.inc.notification')
	
	<div class="card-column-wrapper">
		<ol class="card-columns list-unstyled" data-projectid="{{ $project->id }}">				
			@foreach( $cards as $card )
				<li id="{{ $card->id }}" class="card-column" data-cardid="{{ $card->id }}">
					<div class="panel">
						<div class="panel-body">								
							<h3 class="title-hero">
								<a href="{{ route('sytrix.card.archive', $card->id) }}" title="Archive Card" class="confirmAlert-self pull-right" data-confirm-message="Archive Card?"><span class="glyph-icon icon-archive archive-card cursor-pointer"></span></a>

								<span class="glyph-icon icon-users tooltip-button pull-right cursor-pointer popup-editable" data-contenttarget=".selected-members-content" data-placement="bottom" data-type="html" data-title="Assign Member(s)" title="Assign Member(s)"></span>

								<span class="glyph-icon icon-edit pull-right edit-card cursor-pointer" data-title="Edit Card Label" title="Edit Card Label"></span>
									<strong class="card-label">{{  $card->title }}</strong>
							</h3>		
							<div class="card-container">
								@forelse( $card->tasks as $cardTask )
									<div class="card-item-wrapper" data-taskid="{{ $cardTask->id }}">								
										<a href="{{ route('sytrix.task.delete', $cardTask->id) }}" class="glyph-icon confirmAlert-ajax icon-remove task-remove cursor-pointer"></a>		
										<div class="card-item cursor-pointer item-hover">
											<h5 class="card-item-title">{{ $cardTask->title }}</h5>
										</div>
									</div>
								@empty								
								@endforelse
							</div>
						</div>
						<div class="panel-footer">
							<button class="btn btn-xs btn-success add-task">Add Task</button>	
							<div class="task-members pull-right">
								@if( $card->users->count() > 0 )
									<ul class="task-member-list user-list list-inline">
										@foreach( $card->users as $taskMember )
											<li class="task-member-item user-item">
												@if( $taskMember->user_picture != null )
													<img src="{{ asset( $taskMember->user_picture ) }}" data-options='{"remove_task_member":"Remove Member", "view_profile":"View Profile"}' data-actionid="{{ $taskMember->id }}" title="{{ $taskMember->firstname }}" class="img-responsive member-assigned has-contextmenu">
												@else 
													<img src="{{ asset('images/profile-default.jpg') }}" title="{{ $taskMember->firstname }}" data-options='{"remove_task_member":"Remove Member", "view_profile":"View Profile"}' data-actionid="{{ $taskMember->id }}" class="img-responsive member-assigned has-contextmenu">
												@endif
											</li>
										@endforeach
									</ul>
								@endif
							</div>			
							<div class="clearfix"></div>		
						</div>
					</div>
				</li>
			@endforeach
		</ol>
	</div>

	<div class="clearfix">
		<button class="btn btn-sm btn-success pull-left add-card">Add Card</button>		
	</div>

	<div class="hidden members-content">
		<ul class="selected-users user-list list-inline">
			@foreach( $selectedUsers as $selectedUser )
				<li class="user-item text-center">
					@if( $selectedUser->user_picture != null )
						<img src="{{ asset( $selectedUser->user_picture ) }}" title="{{ $selectedUser->firstname }}" data-projectid="{{ $project->id }}" data-userid="{{ $selectedUser->id }}" class="img-responsive cursor-pointer">
					@else 
						<img src="{{ asset('images/profile-default.jpg') }}" title="{{ $selectedUser->firstname }}" data-projectid="{{ $project->id }}" data-userid="{{ $selectedUser->id }}" class="img-responsive cursor-pointer">
					@endif
					<small class="text-center">{{ $selectedUser->firstname }}</small>
				</li>
			@endforeach
		</ul>
		<hr>
		<input type="text" class="search-member form-control" data-projectid="{{ $project->id }}" placeholder="Search member">
		<div class="search-members">
			<ul class="user-list list-inline">
				@forelse( $users as $user )
					<li class="user-item text-center {{ ( in_array($user->id, $selectedUsersArray) ) ? 'active' : '' }}">
						@if( $user->user_picture != null )
							<img class="img-responsive cursor-pointer member" src="{{ asset( $user->user_picture ) }}" data-userid="{{ $user->id }}" data-projectid="{{ $project->id }}">
						@else 
							<img class="img-responsive cursor-pointer member" src="{{ asset('images/profile-default.jpg') }}" data-userid="{{ $user->id }}" data-projectid="{{ $project->id }}">
						@endif
						<small>{{ $user->firstname }}</small>
					</li>
				@empty
					<p>Members not found..</p>
				@endforelse
			</ul>
		</div>		
	</div>

	<div class="hidden selected-members-content">
		<ul class="selected-members project-task user-list list-inline">
			@foreach( $selectedUsers as $selectedUser )
				<li class="user-item text-center">
					@if( $selectedUser->user_picture != null )
						<img src="{{ asset( $selectedUser->user_picture ) }}" title="{{ $selectedUser->firstname }}" data-userid="{{ $selectedUser->id }}" class="img-responsive cursor-pointer task-member">
					@else
						<img src="{{ asset('images/profile-default.jpg') }}" title="{{ $selectedUser->firstname }}" data-userid="{{ $selectedUser->id }}" class="img-responsive cursor-pointer task-member">
					@endif
					<small class="text-center">{{ $selectedUser->firstname }}</small>
				</li>
			@endforeach
		</ul>
	</div>
@stop