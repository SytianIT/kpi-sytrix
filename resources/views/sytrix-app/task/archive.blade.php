@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.projects') }}">Projects</a></li>
	<li><a href="{{ route('sytrix.project.tasks', $project->id) }}">{{ $project->name }}</a></li>
	<li class="active">Archive</li>
@stop


@section('page-heading')
	Projects - {{ $project->name }}
	<a href="{{ route('sytrix.card.trash', $project->id) }}" class="glyph-icon tooltip-button icon-archive pull-right card-archives" title="Task Archives"></a>
	<a href="{{ route('sytrix.project.edit', $project->id) }}" class="btn btn-sm btn-info pull-right">Project Details</a>
@stop

@section('main')
@include('templates.inc.notification')
	
	<div class="card-column-wrapper">
		<div class="card-columns" data-projectid="{{ $project->id }}">				
			@foreach( $cards as $card )
				<div class="card-column" data-cardid="{{ $card->id }}">
					<div class="panel">
						<div class="panel-body">								
							<h3 class="title-hero">								
								<strong class="card-label">{{  $card->title }}</strong>
							</h3>		
							<div class="card-container">
								@forelse( $card->tasks as $cardTask )
									<div class="card-item-wrapper" data-taskid="{{ $cardTask->id }}">								
										<span class="glyph-icon icon-remove task-remove cursor-pointer"></span>		
										<div class="card-item cursor-pointer item-hover">
											<h5 class="card-item-title">{{ $cardTask->title }}</h5>
										</div>
									</div>
								@empty								
								@endforelse
							</div>
						</div>
						<div class="panel-footer">
							<a href="{{ route('sytrix.card.restore', array($project->id, $card->id)) }}" class="btn btn-xs btn-success confirmAlert" data-confirm-message="Restore Card?">Restore</a>
							<div class="task-members pull-right">
								@if( $card->users->count() > 0 )
									<ul class="task-member-list user-list list-inline">
										@foreach( $card->users as $taskMember )
											<li class="task-member-item user-item">
												<img src="{{ asset( $taskMember->user_picture ) }}" data-options='{"remove_task_member":"Remove Member", "view_profile":"View Profile"}' data-actionid="{{ $taskMember->id }}" class="img-responsive member-assigned has-contextmenu">
											</li>
										@endforeach
									</ul>
								@endif
							</div>			
							<div class="clearfix"></div>		
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@stop