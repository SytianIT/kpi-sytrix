@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.task-categories') }}">Task Categorories</a></li>
	<li class="active">Create Task Category</li>
@stop

@section('page-heading')
Add Task Category
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
	            {!! Form::open(['route' => 'sytrix.task-category.store', 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true']) !!}

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Parent Task Category </label>
	                <div class="col-sm-6">
	                	<select name="parent_taskcat" class="form-control">
	                		<option selected disabled>Select Parent Category</option>
	                		<option value="">None</option>
	                		@forelse( $taskCategories as $taskCategory ) 
	                			@include('sytrix-app.task-category.option-subcat')
	                		@empty
	                		@endforelse
	                	</select>
	                </div>
	            </div>

	          	<div class="form-group">
	                <label class="col-sm-3 control-label">Title <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <input type="text" name="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ old('title') }}">
	                    @if ($errors->has('title'))
		                    <label class="error">{{ $errors->first('title') }}</p>
	                    @endif
	                </div>
	            </div>

             	<div class="form-group">
	                <label class="col-sm-3 control-label">&nbsp;</label>
	                <div class="col-sm-6 text-right">
	                    <a href="{{ route('sytrix.task-categories') }}" class="btn btn-sm btn-danger">CANCEL</a>
	                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
	                </div>
	            </div>

	            {!! Form::close() !!}
	        </div>
		</div>
	</div>
@stop