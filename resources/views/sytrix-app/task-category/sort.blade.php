@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.task-categories') }}">Task Categories</a></li>
	<li class="acttive">Sort</li>
@stop

@section('page-heading')
	Task Categories
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<div class="size-md">
					<a href="{{ route('sytrix.task-category.create') }}" class="btn btn-sm btn-success">Create Task Category</a>

						<a href="{{ route('sytrix.task-category.sort') }}" class="btn btn-sm btn-warning">Sort</a>
						<a href="{{ route('sytrix.task-category.trash') }}" class="view-trash">
							<span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span>
						</a>
					</div>
				</div>			
			</div>			
		</div>
	</div>

	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="panel-body">
					<ol class="list-unstyled sort-list sortable tasks-sort">
						@forelse( $taskCategories as $taskCategory ) 
							@include('sytrix-app.task-category.sort-subcat')
						@empty

						@endforelse
					</ol>
				</div>
			</div>
		</div>
	</div>
@stop