@if( $currentTaskCategory->id != $taskCategory->id )
	<option value="{{ $taskCategory->id }}" {{ ( $currentTaskCategory->parent_id == $taskCategory->id ) ? 'selected' : '' }} {{ ($taskCategory->depth == 0 ) ? 'class=font-bold' : null }} >{{  str_repeat('-', $taskCategory->depth) . $taskCategory->title }}</option>

	@if( $taskCategory->children()->count() > 0 )	
		@foreach( $taskCategory->children()->get() as $taskCategory )
			@include('sytrix-app.task-category.option-edit-subcat')
		@endforeach
	@endif
@endif