<li id="{{ $taskCategory->id }}" data-taskid="{{ $taskCategory->id }}">
	{{ $taskCategory->title }}
	@if( $taskCategory->children()->count() > 0 )
		<div class="task-subcat">
			@foreach( $taskCategory->children as $taskCategory )
				@include('sytrix-app.task-category.sort-childsubcat')
			@endforeach
		</div>		
	@endif
</li>