<tr>					    	
	<td {{ ($taskCategory->depth == 0) ? 'class=font-bold' : null }}>{{ str_repeat("-", $taskCategory->depth) . $taskCategory->title }}</td>				    	
	<td class="text-center">
		<a href="{{ route('sytrix.task-category.edit', $taskCategory->id) }}" class="btn btn-xs btn-info">EDIT</a>
		@if( $taskCategory->children()->count() > 0 )
			<button data-confirm-message="Deletion not allowed" data-confirm-text="Deletion of categorie(s) with subcategorie(s) is not permitted" class="btn btn-xs btn-danger deleteAlert">DELETE</button>
		@else
			<a href="{{ route('sytrix.task-category.delete', $taskCategory->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
		@endif
	</td>		    	
</tr>

@if( $taskCategory->children()->count() > 0 ) 
	@foreach( $taskCategory->children()->get() as $taskCategory )
		@include('sytrix-app.task-category.subcat')
	@endforeach
@endif