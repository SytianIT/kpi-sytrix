@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.task-categories') }}">Task Categories</a></li>
	<li class="active">Trash</li>
@stop

@section('page-heading')
	Task Categories - Trash
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<div class="size-md">
					<a href="{{ route('sytrix.task-category.create') }}" class="btn btn-sm btn-success">Create Task Category</a>

						<a href="{{ route('sytrix.task-category.trash') }}" class="view-trash">
							<span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span>
						</a>
					</div>
				</div>

				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Title</th>
				    		<th class="text-center" width="1%">Actions</th>
				    	</tr>
				    </thead>

				    <tbody>
					    @forelse( $taskCategories as $taskCategory )
					    	<tr>					    	
								<td>{{ $taskCategory->title }}</td>				    	
								<td class="text-center" width="1%">
									<a href="{{ route('sytrix.task-category.restore', $taskCategory->id) }}" class="btn btn-xs btn-success confirmAlert">RESTORE</a>
								</td>		    	
							</tr>
					    @empty
					    	<tr><td colspan="8">Empty Trash.</td></tr>
					    @endforelse 
				    </tbody>
			    </table>
			</div>
		</div>
	</div>
@stop