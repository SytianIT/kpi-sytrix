{{ str_repeat('-', $status->depth) . $status->title }}<br />

@if( $status->children()->count() > 0 )
		@foreach( $status->children as $status )
			@include('sytrix-app.status.sort-childsubcat')
		@endforeach
@endif