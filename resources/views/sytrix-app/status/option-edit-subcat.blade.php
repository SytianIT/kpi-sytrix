@if( $currentStatus->id != $status->id )
	<option value="{{ $status->id }}" @if( $currentStatus->parent_id == $status->id ) selected  @endif  @if( $status->depth == 0 ) class="font-bold" @endif >{{  str_repeat('-', $status->depth) . $status->title }}</option>

	@if( $status->children()->count() > 0 )
		@foreach( $status->children()->get() as $status )
				@include('sytrix-app.status.option-edit-subcat')
		@endforeach
	@endif
@endif