@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li class="active">Project Status</li>
@stop

@section('page-heading')
Project Statuses
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<a href="{{ route('sytrix.status.create') }}" class="btn btn-sm btn-success">Create Project Status</a>
					<a href="{{ route('sytrix.status.sort') }}" class="btn btn-sm btn-warning">Sort</a>

					<a href="{{ route('sytrix.status.trash') }}" class="view-trash">
						<span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span>
					</a>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Color</th>
				    		<th>Title</th>
				    		<th>Description</th>
				    		<th class="text-center">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@forelse($statuses as $statusKey => $status)
				    		@include('sytrix-app.status.subcat', ['status' => $status])
				    	@empty
				    	<tr><td colspan="6">No records found!</td></tr>
				    	@endforelse
				    </tbody>
			    </table>
			</div>
		</div>
	</div>
@stop