@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.status') }}">Project Status</a></li>
	<li class="active">Trash</li>
@stop

@section('page-heading')
Project Statuses - Trash
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<a href="{{ route('sytrix.status.create') }}" class="btn btn-sm btn-success">Create Rroject Status</a>

					<a href="{{ route('sytrix.status.trash') }}" class="view-trash">
						<span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span>
					</a>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th width="8%">&nbsp;</th>
				    		<th>Title</th>
				    		<th>Description</th>
				    		<th class="text-center">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@forelse($statuses as $statusKey => $status)
					    	<tr>
					    		<th width="8%">&nbsp;</th>
					    		<td>{{ $status->title }}</td>
					    		<td>{{ $status->description }}</td>
					    		<td class="text-center">
				    				<a href="{{ route('sytrix.status.restore', $status->id) }}" class="btn btn-xs btn-success confirmAlert">Restore</a>
					    		</td>
					    	</tr>
				    	@empty
				    	<tr><td colspan="6">Empty trash.</td></tr>
				    	@endforelse
				    </tbody>
			    </table>
			</div>
		</div>
	</div>
@stop