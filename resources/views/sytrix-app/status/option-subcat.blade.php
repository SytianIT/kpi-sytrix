<option value="{{ $status->id }}" {{ ($status->depth == 0 ) ? 'class=font-bold' : null }} >{{  str_repeat('-', $status->depth) . $status->title }}</option>

@if( $status->children()->count() > 0 )
	@foreach( $status->children()->get() as $status )
		@include('sytrix-app.status.option-subcat')
	@endforeach
@endif