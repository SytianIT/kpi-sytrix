<tr>
	<td>
		<div class="color-pick change-color" data-id="{{ $status->id }}" data-value="{{ $status->color_status }}"></div>
	</td>
	<td>
		@if( $status->depth == 0 ) 
			<strong>
		@endif 
			{{ str_repeat('-', $status->depth) . $status->title }}
		@if( $status->depth == 0 ) 
			</strong>
		@endif
	</td>
	<td>{{ $status->description }}</td>
	<td class="text-center">
		<a href="{{ route('sytrix.status.edit', $status->id) }}" class="btn btn-xs btn-info">EDIT</a>
	
		@if( $status->children()->count() == 0 )
			<a data-confirm-message="Delete Status?" href="{{ route('sytrix.status.delete', $status->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
		@else 
			<button data-confirm-message="Deletion not allowed" data-confirm-text="Deletion of categorie(s) with subcategorie(s) is not permitted" class="btn btn-xs btn-danger deleteAlert">DELETE</button>
		@endif
	</td>
</tr>

@if( $status->children()->count() > 0 )
	@foreach( $status->children()->get() as $status )	
		@include('sytrix-app.status.subcat', ['status' => $status])
	@endforeach
@endif