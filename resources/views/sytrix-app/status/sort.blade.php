@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.status') }}">Project Status</a></li>
	<li class="active">Sort</li>
@stop

@section('page-heading')
Project Statuses
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div class="size-md">
					<a href="{{ route('sytrix.status.create') }}" class="btn btn-sm btn-success">Create Rroject Status</a>
					<a href="{{ route('sytrix.status.sort') }}" class="btn btn-sm btn-warning">Sort</a>

					<a href="{{ route('sytrix.status.trash') }}" class="view-trash">
						<span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span>
					</a>
				</div>
				
				<div class="panel">
					<div class="panel-body">
						<div class="example-box-wrapper">
							<div class="panel-body">
								<ol class="list-unstyled sort-list sortable status-sort">
									@forelse( $statuses as $status ) 
										@include('sytrix-app.status.sort-subcat')
									@empty

									@endforelse
								</ol>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@stop