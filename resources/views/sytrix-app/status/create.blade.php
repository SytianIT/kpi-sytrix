@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('sytrix.status') }}">Projext Status</a></li>
	<li class="active">Create Project Status</li>
@stop

@section('page-heading')
Add Project Status
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
	            {!! Form::open(['route' => 'sytrix.status.store', 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true']) !!}

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Parent / Color </label>
	                <div class="col-sm-3">
	                	<select name="parent_status" class="form-control">
	                		<option selected disabled>Select Parent Status</option>
	                		<option value="">None</option>
	                		@forelse( $statuses as $status )
	                			@include('sytrix-app.status.option-subcat')
	                		@empty
	                		@endforelse
	                	</select>
	                </div>
	                <div class="col-sm-3">
	                    <input type="text" name="color_status" class="form-control color-pick">
	                </div>
	            </div>

	          	<div class="form-group">
	                <label class="col-sm-3 control-label">Title <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <input type="text" name="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ old('title') }}">
	                    @if ($errors->has('title'))
		                    <label class="error">{{ $errors->first('title') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Description</label>
	                <div class="col-sm-6">
	                    <textarea name="description" class="form-control {{ ($errors->has('description')) ? 'parsley-error' : '' }}">{{ old('description') }}</textarea>
	                    @if ($errors->has('description'))
	                   		<label class="error">{{ $errors->first('description') }}</p>
	                    @endif
	                </div>
	            </div>

	             <div class="form-group">
	                <label class="col-sm-3 control-label">&nbsp;</label>
	                <div class="col-sm-6 text-right">
	                    <a href="{{ route('sytrix.status') }}" class="btn btn-sm btn-danger">CANCEL</a>
	                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
	                </div>
	            </div>

	            {!! Form::close() !!}
	        </div>
		</div>
	</div>
@stop