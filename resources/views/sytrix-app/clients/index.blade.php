@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li class="active">Clients</li>
@stop

@section('page-heading', 'Clients')

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				<div>{{ $clients->links() }}</div>
				<div class="size-md table-actions">
					<a href="{{ route('client.create') }}" class="btn btn-sm btn-success">Add Client</a>
					
					 {!! Form::open(['route' => 'clients', 'method' => 'GET', 'class' => 'form-horizontal form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
						 		<input type="text" name="search_client" placeholder="(e.g. Company, Contact Person)" class="form-control">					 			
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
				</div>

				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover {{ $clients->count() > 0 ? 'dataTable' : '' }}" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th>Company Name</th>
				    		<th>Contact Person</th>
				    		<th class="nosort">Mobile</th>
				    		<th class="nosort">Email</th>
				    		<th class="text-center nosort">Actions</th>
				    	</tr>
				    </thead>
				    <tbody>
					    @forelse( $clients as $client )
					    	<tr>					    
						    	<td>{{ $client->company_name }}</td>				    	
						    	<td>{{ $client->contact_person }}</td>	
						    	<td>{{ $client->mobile_number }}</td>			    	
						    	<td>{{ $client->email }}</td>				    	
						    	<td class="text-center">
					    			<a href="{{ route('sytrix.projects', array('client_id' => $client->id)) }}" class="btn btn-xs btn-primary">PROJECTS</a>
					    			<a href="{{ route('client.edit', $client->id) }}" class="btn btn-xs btn-info">EDIT</a>
					    			<a href="{{ route('client.delete', $client->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
					    		</td>		    	
					    	</tr>
					    @empty
					    <tr><td colspan="8">No records found.</td></tr>
					    @endforelse 
				    </tbody>
			    </table>

			    <a href="{{ route('client.trash') }}" class="view-trash"><span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span></a>

			  	<div>{{ $clients->links() }}</div>
			</div>
		</div>
	</div>
@stop