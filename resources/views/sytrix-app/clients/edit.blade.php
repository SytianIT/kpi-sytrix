@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('clients') }}">Clients</a></li>
	<li class="active">Edit Client</li>
@stop

@section('page-heading')
Edit Client
@stop

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
	            {!! Form::open(['route' => array('client.update', $client->id), 'files' => true, 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true']) !!}

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Company Logo</label>
	                <div class="col-sm-6">
	                	@if( $client->logo ) 
	                		<img src="{{ asset($client->logo) }}" alt="{{ $client->company_name }}" class="img-responsive">
	                	@else 
	                		<img src="{{ asset('images/profile-default.jpg') }}" alt="{{ $client->company_name }}" class="img-responsive">
	                	@endif

	                    <input type="file" name="logo" class="form-control {{ ($errors->has('company_logo')) ? 'parsley-error' : '' }}">
	                    @if ($errors->has('logo'))
		                    <label class="error">{{ $errors->first('logo') }}</p>
	                    @endif
	                </div>
	            </div>

	          	<div class="form-group">
	                <label class="col-sm-3 control-label">Company Name <span class="req">*</span></label>
	                <div class="col-sm-6">
	                    <input type="text" name="company_name" class="form-control {{ ($errors->has('company_name')) ? 'parsley-error' : '' }}" value="{{ $client->company_name }}">
	                    @if ($errors->has('company_name'))
		                    <label class="error">{{ $errors->first('company_name') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Company Address <span class="req">*</span></label>
	                <div class="col-sm-6">
	                	<textarea name="company_address" class="form-control {{ ($errors->has('company_address')) ? 'parsley-error' : '' }}">{{ $client->company_name }}</textarea>
	                    @if ($errors->has('company_address'))
		                    <label class="error">{{ $errors->first('company_address') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Contact Person</label>
	                <div class="col-sm-6">
	                	<input type="text" name="contact_person" class="form-control {{ ($errors->has('contact_person')) ? 'parsley-error' : '' }}" value="{{ $client->contact_person }}">
	                    @if ($errors->has('contact_person'))
		                    <label class="error">{{ $errors->first('contact_person') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Contact Numbers <span class="req">*</span></label>
	                <div class="col-sm-3">
	                	<input type="text" name="mobile_number" class="form-control {{ ($errors->has('mobile_number')) ? 'parsley-error' : '' }}" placeholder="Mobile" value="{{ $client->mobile_number }}">
	                    @if ($errors->has('mobile_number'))
		                    <label class="error">{{ $errors->first('mobile_number') }}</p>
	                    @endif
	                </div>
	                <div class="col-sm-3">
	                	<input type="text" name="landline_number" class="form-control {{ ($errors->has('landline_number')) ? 'parsley-error' : '' }}" placeholder="Landline" value="{{ $client->landline_number }}">
	                    @if ($errors->has('landline_number'))
		                    <label class="error">{{ $errors->first('landline_number') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
	                <div class="col-sm-6">
	                	<input type="email" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" value="{{ $client->email }}">
	                    @if ($errors->has('email'))
		                    <label class="error">{{ $errors->first('email') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Skype ID</label>
	                <div class="col-sm-6">
	                	<input type="text" name="skype_id" class="form-control {{ ($errors->has('skype_id')) ? 'parsley-error' : '' }}" value="{{ $client->skype_id }}">
	                    @if ($errors->has('skype_id'))
		                    <label class="error">{{ $errors->first('skype_id') }}</p>
	                    @endif
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Payment Terms</label>
	                <div class="col-sm-6">
						<textarea name="payment_terms" class="form-control {{ ($errors->has('payment_terms')) ? 'parsley-error' : '' }}">{{ $client->payment_terms }}</textarea>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Discounts</label>
	                <div class="col-sm-6">
	                	<textarea name="discounts" class="form-control {{ ($errors->has('discounts')) ? 'parsley-error' : '' }}">{{ $client->discounts }}</textarea>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Layout</label>
	                <div class="col-sm-6">
	                	<textarea name="layout" class="form-control {{ ($errors->has('layout')) ? 'parsley-error' : '' }}">{{ $client->layout }}</textarea>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Hosting</label>
	                <div class="col-sm-6">
	                	<textarea name="hosting" class="form-control {{ ($errors->has('hosting')) ? 'parsley-error' : '' }}">{{ $client->hosting }}</textarea>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Domain</label>
	                <div class="col-sm-6">
	                	<textarea name="domain" class="form-control {{ ($errors->has('domain')) ? 'parsley-error' : '' }}">{{ $client->domain }}</textarea>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">Notes</label>
	                <div class="col-sm-6">
	                	<textarea name="notes" class="form-control {{ ($errors->has('notes')) ? 'parsley-error' : '' }}">{{ $client->notes }}</textarea>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-sm-3 control-label">&nbsp;</label>
	                <div class="col-sm-6 text-right">
	                    <a href="{{ route('clients') }}" class="btn btn-sm btn-danger">CANCEL</a>
	                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
	                </div>
	            </div>

	            {!! Form::close() !!}
	        </div>		
		</div>
	</div>
@stop