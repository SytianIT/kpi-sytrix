@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('sytrix-app') }}">Home</a></li>
	<li><a href="{{ route('clients') }}">Clients</a></li>
	<li class="active">Trash</li>
@stop

@section('page-heading', 'Clients - Trash')

@section('main')
@include('templates.inc.notification')
	<div class="panel">
		<div class="panel-body">
			<div class="example-box-wrapper">
				
				<div class="size-md table-actions">
					<a href="{{ route('client.create') }}" class="btn btn-sm btn-success">Add Client</a>
					
					 {!! Form::open(['route' => 'clients', 'method' => 'GET', 'class' => 'form-horizontal form-search bordered-row form-validation-true']) !!}
					 	<div class="form-inline">
					 		<div class="form-group">
						 		<input type="text" name="search_client" placeholder="(e.g. Company, Contact Person)" class="form-control">					 			
					 		</div>
					 		<div class="form-group">					 			
						 		<button type="submit" class="btn btn-success glyph-icon tooltip-button demo-icon icon-search"></button>
					 		</div>
					 	</div>
					 {!! Form::close() !!}
				</div>
				
				<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered" id="datatable-example">
				    <thead>
				    	<tr>
				    		<th width="8%">&nbsp;</th>
				    		<th>Company Name</th>
				    		<th>Contact Person</th>
				    		<th>Email</th>
				    		<th class="text-center">Actions</th>
				    	</tr>
				    </thead>

				    @forelse( $clients as $client )
				    <tbody>
				    	<th width="8%">&nbsp;</th>
				    	<td>{{ $client->company_name }}</td>				    	
				    	<td>{{ $client->contact_person }}</td>				    	
				    	<td>{{ $client->email }}</td>				    	
				    	<td class="text-center">
			    			<a href="{{ route('client.restore', $client->id) }}" class="btn btn-xs btn-success confirmAlert">RESTORE</a>
			    		</td>		    	
				    </tbody>
				    @empty
				    <tr><td colspan="8">Trash Empty.</td></tr>
				    @endforelse 
			    </table>
				
				<a href="{{ route('client.trash') }}" class="view-trash"><span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span></a>
			</div>
		</div>
	</div>
@stop