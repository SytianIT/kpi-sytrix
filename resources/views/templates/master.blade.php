<!DOCTYPE html> 
<html ng-app="syionApp">
<head>
    <meta charset="UTF-8">
    <title>@yield('title', (isset($title) && $title != null) ? $title : 'Sytian Management System')</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="base-url" content="{{ url('/') }}">
    <meta name="description" content="{{ isset($description) ? $description : 'no description' }}">
    <meta name="keywords" content="{{ isset($keywords) ? $keywords : 'no keywords' }}">
    
    @section('meta')
    {{-- Section for other Meta --}}
    @show

    {{-- Favicons --}}
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/favicon/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/favicon/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/favicon/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/favicon/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon/favicon.png') }}">

    {{-- Import Delight Admin Template CSS --}}
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/delight.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    @section('css')
    @show
    
    {{-- Section for importing scripts that needs to loaded first --}}
    @section('head-scripts')
    @show

</head>
<body>
    <div id="loading">
        <div class="svg-icon-loader">
            <img src="{{ asset('/images/svg-loaders/bars.svg') }}" width="40" alt="">
        </div>
    </div>

    {{-- Layout Intended for Guest --}}
    @section('guest')
    @show

    {{-- Layout Intended for Admin --}}
    @if(Auth::user())
    <div id="page-wrapper">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        </div>
        <aside id="page-sidebar">
            <div id="header-logo" class="logo-bg">
                <a href="{{ route(Session::get('current-app').'-app') }}" class="logo-content-big" title="DelightUI" style="background: url({{ Session::has('current-app') ? asset('images/logo-'.Session::get('current-app').'.png') : '' }}) left 50% no-repeat;">
                    Delight <i>UI</i>
                    <span>Material Design Dashboard Template</span>
                </a>
                <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-outdent"></i>
                </a>
            </div>
            <div class="scroll-sidebar">
                @include('templates.inc.main-sidebar')                
            </div>
        </aside>
        <main id="page-content-wrapper">
            <div id="page-content">                
                <header id="page-header"> 
                    <div id="header-nav-left">
                        <div id="notifications-btn" class="dropdown">                            
                            <a href="#" data-toggle="dropdown">
                                @if( $notifications->count() > 0 )
                                   <span class="small-badge bg-danger notif-count">
                                        @if( $notifications->count() > 20 )
                                            {{ '20+' }}
                                        @else 
                                           {{ $notifications->count() }}
                                        @endif 
                                   </span>
                                @endif
                                <i class="glyph-icon icon-linecons-megaphone"></i>
                            </a>
                            <div class="dropdown-menu box-md float-right"> 
                                <div class="popover-title display-block clearfix pad10A">Notifications</div>
                                <div class="slimScrollDiv">                                
                                    <div class="scrollable-content scrollable-slim-box">
                                        <ul class="no-border notifications-box">                                    
                                            @forelse( $notifications->take(20) as $notification )

                                                @php
                                                    switch( $notification->contextable_value ) {
                                                        case 'project' :
                                                            $icon = 'icon-tasks';
                                                            break;
                                                        case 'timetrack' :
                                                            $icon = 'icon-clock-o';
                                                            break;
                                                        default:
                                                    }
                                                @endphp

                                                <li>                                                    
                                                    <span class="glyph-icon icon-notification {{ $icon }} bg-{{ $notification->status }}"></span> 
                                                    <span class="notification-text">{{ $notification->description }}</span>
                                                    <span title="clear" data-id="{{ $notification->id }}" class="clear-notif glyph-icon icon-close pull-right cursor-pointer"></span>
                                                    <div class="clearfix"></div>
                                                    <div class="notification-time">
                                                        {{ $notification->elapsed_time }}
                                                        <span class="glyph-icon icon-clock-o"></span>
                                                    </div>
                                                </li>
                                            @empty
                                                <li>{{ 'Notifications empty..' }}</li>
                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                                <div class="button-pane button-pane-alt pad5T pad5L pad5R text-center">
                                    <a href="#" class="btn btn-flat btn-primary" title="View all notifications">
                                        View all notifications
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="user-account-btn dropdown">                            
                            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                                <img width="28" src="{{ $authUser->user_picture != null ? asset($authUser->user_picture) : asset('images/gravatar.jpg') }}" alt="Profile image">
                                <span>{{ $authUser->username }}</span>
                                <i class="glyph-icon icon-angle-down"></i>
                            </a>
                            <div class="dropdown-menu float-right">
                                <div class="box-sm">
                                    <div class="login-box clearfix">
                                        <div class="user-img">
                                            <a href="{{ route('profile') }}" title="" class="change-img">Change photo</a>
                                            <img src="{{ $authUser->user_picture != null ? asset($authUser->user_picture) : asset('images/gravatar.jpg') }}" alt="">
                                        </div>
                                        <div class="user-info">
                                            <span>
                                                <a href="{{ route('profile') }}">{{ $authUser->name }}</a>
                                                <i>{{ $authUser->job_title }}</i>
                                            </span>
                                            {{-- <a href="#" title="Edit profile">Edit profile</a> --}}
                                            {{-- <a href="#" title="View notifications">View notifications</a> --}}
                                        </div>
                                    </div>
                                    <div class="button-pane button-pane-alt pad5L pad5R text-center">
                                        <a href="{{ route('auth.logout') }}" class="btn btn-flat display-block font-normal btn-success">
                                            <i class="glyph-icon icon-power-off"></i>
                                            Logout
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- #header-nav-left -->

                    <div id="header-nav-right">
                        <div class="select-syion">
                            <select data-placeholder="Select Application..." name="application" id="change-application" class="chosen-select form-control chosen-select">
                                <option value="{{ route('home') }}">Main App</option>

                               <!--  @if($authUser->hasPermission('access_expense'))
                                <option value="{{ route('expense-app') }}" {{ session()->get('current-app') == 'expense' ? 'selected' : '' }}>Expense App</option>
                                @endif -->

                                @if($authUser->hasPermission('access_sytrix'))
                                <option value="{{ route('sytrix-app') }}" {{ session()->get('current-app') == 'sytrix' ? 'selected' : '' }}>Sytrix App</option>
                                @endif
                            </select>
                        </div>
                    </div><!-- #header-nav-right -->
                    @section('header')
                    @show
                </header>
                <section id="page-wrapper">
                    <div id="page-title" class="pad0T">
                        <ul class="breadcrumb">
                            @section('crumbs')
                            @show
                        </ul>
                        <hr>
                        <h2>@yield('page-heading', 'Page Title Here')</h2>
                        @section('sub-heading')
                        @show
                    </div>
                    @section('main')
                    @show
                </section>
                @include('templates.inc.notification')
                {{-- Modal for modal preview of image --}}
                <div class="modal fade" tabindex="-1" id="modal-preview-image" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header text-right">
                                <a href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="glyph-icon icon-times font-size-18 font-red"></i></a>
                            </div>
                            <div class="modal-body">
                                <img src="" alt="Preview Modal" class="modal-preview-img img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

    @endif
    <script src="{{ asset('js/delight.js') }}"></script>
    {{-- <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script> --}}
    <script src="{{ asset('js/all.js') }}"></script>
    @section('scripts')
    @show
</body>
</html>
