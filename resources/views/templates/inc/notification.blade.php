@if(session()->has('success'))
<div id="notification-show" data-type="success" data-message="{{ '<i class="glyph-icon icon-check"></i>&nbsp;&nbsp;'.session('success') }}" data-layout="top"></div>
@endif

@if(session()->has('status'))
<div id="notification-show" data-type="notification" data-message="{{ '<i class="glyph-icon icon-bell"></i>&nbsp;&nbsp;'.session('status') }}" data-layout="top"></div>
@endif

@if(session()->has('error'))
<div id="notification-show" data-type="error" data-message="{{ '<i class="glyph-icon icon-close"></i>&nbsp;&nbsp;'.session('error') }}" data-layout="top"></div>
@endif

@if(session()->has('warning'))
<div id="notification-show" data-type="warning" data-message="{{ '<i class="glyph-icon icon-exclamation"></i>&nbsp;&nbsp;'.session('warning') }}" data-layout="top"></div>
@endif