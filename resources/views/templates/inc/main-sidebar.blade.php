<ul id="sidebar-menu">
    <li class="header"><span>Overview</span></li>
    <li>
        <?php $currentMainUrl = (Session::get('current-app') == 'main') ? 'home' : Session::get('current-app').'-app'; ?>
        <a href="{{ route($currentMainUrl) }}" title="Admin Dashboard">
            <i class="glyph-icon icon-linecons-tv"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="header"><span>Components</span></li>
    @if(session()->get('current-app') == 'expense')
        @if($authUser->hasPermission('access_expense'))
        <li>
            <a href="javascript:;" title="Expenses">
                <i class="glyph-icon icon-area-chart" title=""></i>
                <span>Expenses</span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    @if($authUser->hasPermission('view_expenses'))
                    <li><a href="{{ route('expense') }}" title="List of Expense"><span>Expense</span></a></li>
                    @endif
                    @if($authUser->hasPermission('create_expense'))
                    <li><a href="{{ route('expense.add') }}" title="Create Expense"><span>Add Expense</span></a></li>
                    @endif
                    @if($authUser->hasPermission('view_recurring_expenses'))
                    <li><a href="{{ route('expense-recurring') }}" title="List of Recurring Expense"><span>Recurring Expense</span></a></li>
                    @endif
                    @if($authUser->hasPermission('create_recurring_expense'))
                    <li><a href="{{ route('expense-recurring.add') }}" title="Create Recurring Expense"><span>Add Recurring Expense</span></a></li>
                    @endif
                </ul>
            </div>
        </li>
        <li>
            <a href="javascript:;" title="Expenses">
                <i class="glyph-icon icon-list" title=""></i>
                <span>Reports</span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    @if($authUser->hasPermission('view_expense_syion_reports'))
                    <li><a href="{{ route('expense.reports.annual') }}" title="Annual Report"><span>Annual</span></a></li>
                    <li><a href="{{ route('expense.reports.monthly') }}" title="Monthly"><span>Monthly</span></a></li>
                    <li><a href="{{ route('expense.reports.pie-chart', 'category') }}" title="Pie Chart Report"><span>Chart - Category</span></a></li>
                    <li><a href="{{ route('expense.reports.pie-chart', 'payment-mode') }}" title="Pie Chart Report"><span>Chart - Payment Mode</span></a></li>
                    <li><a href="{{ route('expense.reports.date-range') }}" title="Date Range"><span>Date Range</span></a></li>
                    @endif
                    @if($authUser->hasPermission('view_expense_syion_log'))
                    <li><a href="{{ route('expense.logs') }}" title="View Users"><span>Logs</span></a></li>
                    @endif
                </ul>
            </div>
        </li>
        @endif
    @endif

    @if( session()->get('current-app') == "sytrix" ) 
        <li>
            <a href="{{ route('sytrix.projects') }}" title="Projects"><i class="glyph-icon icon-archive"></i> <span>Projects</span></a>
        </li>
    @endif

    @if( session()->get('current-app') == "sytrix" )

        <li>
            <a href="javascript:;" title="Reports"><i class="glyph-icon tooltip-button icon-bar-chart"></i> <span>Reports</span></a>
            <div class="sidebar-submenu">
                <ul>
                    <li><a href="{{ route('sytrix.reports.tracks') }}">Time Tracks</a></li>
                    <li><a href="{{ route('sytrix.reports.projects') }}">Projects</a></li>
                    <li><a href="{{ route('sytrix.reports.kpi') }}">KPI</a></li>
                    <li><a href="{{ route('sytrix.reports.leaderboard') }}">Leaderboard</a></li>
                </ul>
            </div>
        </li>

    @endif

    @if($authUser->hasPermission('access_site_settings'))
    <li>
        <a href="javascript:;" title="Settings">
            <i class="glyph-icon icon-linecons-cog"></i>
            <span>Settings</span>
        </a>
        <div class="sidebar-submenu">
            <ul>
                @if(session()->get('current-app') == 'main')
                    <li><a href="{{ route('settings') }}" title="General Settings"><span>General Settings</span></a></li>

                    @if( $authUser->hasPermission('manage_departments') )
                     <li><a href="{{ route('departments') }}" title="View Departments"><span>Departments</span></a></li>
                    @endif

                    @if($authUser->hasPermission('manage_users'))
                    <li><a href="{{ route('users') }}" title="View Employees"><span>Employees</span></a></li>
                    @endif
                    @if($authUser->hasPermission('manage_roles'))
                    <li><a href="{{ route('roles') }}" title="View Users"><span>Roles</span></a></li>
                    @endif
                @endif
                @if(session()->get('current-app') == 'expense')
                    @if($authUser->hasPermission('access_expense_settings'))
                    <li><a href="{{ route('expense.categories') }}" title="View Users"><span>Expenses Category</span></a></li>
                    <li><a href="{{ route('payments') }}" title="View Users"><span>Mode of Payment</span></a></li>
                    <li><a href="{{ route('currencies') }}" title="View Users"><span>Currencies</span></a></li>
                    @endif
                @endif
                @if(session()->get('current-app') == 'sytrix')
                    <li><a href="{{ route('sytrix.categories') }}">Project Categories</a></li>
                    <li><a href="{{ route('sytrix.status') }}">Project Status</a></li>    
                    <li><a href="{{ route('sytrix.task-categories') }}">Task Categories</a></li>
                    <li><a href="{{ route('clients') }}">Clients</a></li>
                @endif
            </ul>
        </div>
    </li>
    @endif

    @if( session()->get('current-app') == "sytrix" )
        <li>
            <a href="{{ route('sytrix.timetrack.create') }}" title="Settings">
                <i class="glyph-icon icon-linecons-cog"></i>
                <span>Add Time Entry</span>
            </a>
        </li>
    @endif
</ul><!-- #sidebar-menu -->