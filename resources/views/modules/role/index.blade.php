@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('home') }}">Home</a></li>
<li class="active">Roles</li>
@stop

@section('page-heading', 'Role')

@section('main')
<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero">
			List of Created Roles
		</h3>
		<div class="example-box-wrapper">
			<div class="size-md">
				@if($authUser->hasPermission('create_roles'))
				<a href="{{ route('role.create') }}" class="btn btn-sm btn-azure">CREATE ROLE</a>
				@endif
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered" id="datatable-example">
			    <thead>
			    	<tr>
			    		<th width="30%">Name</th>
			    		<th width="20%">User Assigned</th>
			    		<th width="30%">Number of Permissions Granted</th>
			    		<th width="20%" class="text-center">Action</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@forelse($roles as $role)
			    	<tr>
			    		<td>{{ $role->title }}</td>
			    		<td>{{ $role->users()->count() }}</td>
			    		<td>{{ $role->permissions()->count() }}</td>
			    		<td class="text-center">
			    			@if($authUser->hasPermission('update_roles'))
			    			<a href="{{ route('role.edit', $role->id) }}" class="btn btn-xs btn-success">EDIT</a>
			    			@endif
			    			@if($authUser->hasPermission('delete_roles'))
			    			<a href="{{ route('role.delete', $role->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
			    			@endif
			    		</td>
			    	</tr>
			    	@empty
			    	<tr><td colspan="4">No records found!</td></tr>
			    	@endforelse
			    </tbody>
		    </table>
		</div>
	</div>
</div>
@stop