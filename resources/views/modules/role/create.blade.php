@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('home') }}">Home</a></li>
<li><a href="{{ route('roles') }}">Roles</a></li>
<li>Create Role</li>
@stop

@section('page-heading', 'Create Role')

@section('main')
@include('templates.inc.form-errors')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			{!! Form::open(['route' => 'role.store', 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
                <div class="form-group">
                    <label class="col-sm-3 control-label">Title <span class="req">*</span></label>
                    <div class="col-sm-6">
                        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Role Title']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Description</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('description', null, ['class' => 'form-control textarea-no-resize', 'placeholder' => 'Description', 'rows' => 5]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Permissions</label>
                    <div class="col-sm-6">
                        @foreach($syions as $syion)
                        <div class="mrg15T">
                        <h5 class="font-size-13 font-bold bs-label label-blue-alt btn-block">{{ $syion->title }}</h5>
                        <?php $countPermission = $syion->permissions->count(); $division = round($countPermission / 2); $ctr = 1; ?>
                        <div class="row mrg10T">
                        @foreach($syion->permissions as $permission)
                            @if($ctr == 1)
                            <div class="col-xs-6">
                            @endif
                            <div class="checkbox checkbox-info">
                                <label>
                                    <input type="checkbox" id="{{ $permission->name }}" checked name="permissions[]" value="{{ $permission->id }}" class="custom-checkbox">
                                    {{ $permission->title }}
                                </label>
                            </div>
                            @if($ctr == $division)
                            </div>
                            <div class="col-xs-6">
                            @endif
                            <?php $ctr++; ?>
                            @endforeach
                            </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">&nbsp;</label>
                    <div class="col-sm-6 text-right">
                        <a href="{{ route('roles') }}" class="btn btn-sm btn-danger">CANCEL</a>
                        <button type="submit" class="btn btn-success btn-sm">SAVE</button>
                    </div>
                </div>
            {!! Form::close() !!}
		</div>
	</div>
</div>
@stop