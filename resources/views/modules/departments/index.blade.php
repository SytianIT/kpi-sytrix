@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('home') }}">Home</a></li>
	<li {{ (!$trash) ? 'class="active"' : null }}>
		@if( $trash )
			<a href="{{ route('departments') }}">
		@endif
			Departments
		@if( $trash )
			</a>
		@endif
	</li>
	@if( $trash )
	<li class="active">Trash</li>
	@endif
@stop

@section('page-heading')
	Departments @if( $trash ) - Trash @endif
@stop

@section('main')
@include('templates.inc.notification')

<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			<div class="size-md">
				<a href="{{ route('department.create') }}" class="btn btn-sm btn-success">Create Department</a>

				<a href="{{ route('department.trash', 'true') }}" class="view-trash"><span class="glyph-icon tooltip-button demo-icon icon-trash-o"></span></a>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered table-hover" id="datatable-example">
			    <thead>
			    	<tr>
			    		<th>Name</th>
			    		<th class="text-center">Actions</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@forelse($departments as $department)
			    	<tr>
			    		<td>{{ $department->title }}</td>
			    		<td class="text-center" width="20%">
			    			@if( $trash == true )
			    				@if( $authUser->hasPermission('manage_departments') ) 
			    					<a href="{{ route('department.restore', $department->id) }}" class="btn btn-xs btn-success confirmAlert">RESTORE</a>
			    				@endif
			    			@else 
			    				@if($authUser->hasPermission('manage_departments'))
					    			<a href="{{ route('department.edit', $department->id) }}" class="btn btn-xs btn-info">EDIT</a>
				    			@endif
				    			@if($authUser->hasPermission('manage_departments'))
				    				<a href="{{ route('department.delete', $department->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
				    			@endif
			    			@endif
			    		</td>
			    	</tr>
			    	@empty
			    	<tr><td colspan="6">No records found!</td></tr>
			    	@endforelse
			    </tbody>
		    </table>
		</div>
	</div>
</div>
@stop