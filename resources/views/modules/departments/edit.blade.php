@extends('templates.master')

@section('crumbs')
	<li><a href="{{ route('home') }}">Home</a></li>
	<li class="active">Departments</li>
@stop

@section('page-heading', 'Edit Department')

@section('main')
@include('templates.inc.notification')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
            {!! Form::open(['route' => array('department.update', $department->id), 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true', 'enctype' => 'multipart/form-data']) !!}

          	 <div class="form-group">
                <label class="col-sm-3 control-label">Department Name <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $department->title }}">
                    @if ($errors->has('title'))
                    <label class="error">{{ $errors->first('title') }}</p>
                    @endif
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('departments') }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">UPDATE</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
	</div>
</div>
@stop