@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('home') }}">Home</a></li>
<li class="active">Users</li>
@stop

@section('page-heading', 'Users')

@section('main')
@include('templates.inc.notification')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			<div class="size-md">
				@if($authUser->hasPermission('manage_users'))
				<a href="{{ route('user.create') }}" class="btn btn-sm btn-success">Create User</a>
				@endif
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-bordered {{ ($users->count() > 1) ? 'dataTable' : '' }}" id="datatable-example">
			    <thead>
			    	<tr>
			    		<th width="8%" class="nosort">&nbsp;</th>
			    		<th>Last Name</th>
			    		<th>First Name</th>
			    		<th>Job Description</th>
			    		<th class="nosort">Email</th>
			    		<th class="text-center nosort">Actions</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@forelse($users as $user)
			    	<tr>
			    		<td style="max-width:100px;"><img src="{{ $user->user_picture != null ? asset($user->user_picture) : asset('images/gravatar.jpg') }}" alt="{{ $user->name }}" class="img-responsive"></td>
			    		<td>{{ $user->lastname }}</td>
			    		<td>{{ $user->firstname }}</td>
			    		<td>{{ $user->job_title }}</td>
			    		<td>{{ $user->email }}</td>
			    		<td class="text-center">
			    			<a href="{{ route('user.edit', $user->id) }}" class="btn btn-xs btn-info">EDIT</a>
			    			<a href="{{ route('user.delete', $user->id) }}" class="btn btn-xs btn-danger">DELETE</a>
			    		</td>
			    	</tr>
			    	@empty
			    	<tr><td colspan="6">No records found!</td></tr>
			    	@endforelse
			    </tbody>
		    </table>
		    <div>{{ $users->links() }}</div>
		</div>
	</div>
</div>
@stop