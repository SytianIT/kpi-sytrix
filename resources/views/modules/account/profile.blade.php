@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('home') }}">Home</a></li>
<li class="active">Profile</li>
@stop

@section('page-heading', 'My Profile')

@section('main')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
            {!! Form::model($authUser, ['route' => 'profile.update', 'method' => 'POST', 'class' => 'form-horizontal bordered-row form-validation-true', 'enctype' => 'multipart/form-data']) !!}
                <div class="form-group">
                    <label class="col-sm-3 control-label">Profile Image</label>
                    <div class="col-sm-6">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail {{ $authUser->user_picture == null ? 'hidden' : '' }}" data-trigger="fileinput" style="max-width: 350px; max-height: 350px;"><img src="{{ $authUser->user_picture != null ? asset($authUser->user_picture) : '' }}" class="img-responsive"></div>
                            <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <input name="image" type="file" id="file-browse">
                                <div class="ripple-wrapper"></div></span>
                                <em class="font-size-11">For better results, please provide an image with a resolution of 350 x 350.</em>
                                @if ($errors->has('firstname'))
                                <label class="error">{{ $errors->first('image') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">First Name <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="firstname" class="form-control {{ ($errors->has('firstname')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ old('firstname') ?: $authUser->firstname }}">
                        @if ($errors->has('firstname'))
                        <label class="error">{{ $errors->first('firstname') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Last Name <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="lastname" class="form-control {{ ($errors->has('lastname')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ old('lastname') ?: $authUser->lastname }}">
                        @if ($errors->has('lastname'))
                        <label class="error">{{ $errors->first('lastname') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Middle Name <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="middlename" class="form-control {{ ($errors->has('middlename')) ? 'parsley-error' : '' }}" placeholder="Middle Name" value="{{ old('middlename') ?: $authUser->middlename }}">
                        @if ($errors->has('middlename'))
                        <label class="error">{{ $errors->first('middlename') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Birthdate <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="hidden" class="old-birthday" value="{{ $authUser->birthdate->format('m/d/Y') ?: old('birthdayPicker_birthDay') }}">
                        <div id="birthdayPicker"></div>
                        @if ($errors->has('birthdayPicker_birthDay'))
                        <label class="error">{{ $errors->first('birthdayPicker_birthDay') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="address" class="form-control {{ ($errors->has('address')) ? 'parsley-error' : '' }}" placeholder="Address" value="{{ old('address') ?: $authUser->address }}">
                        @if ($errors->has('address'))
                        <label class="error">{{ $errors->first('address') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mobile Number <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="mobile_number" class="form-control {{ ($errors->has('mobile_number')) ? 'parsley-error' : '' }}" placeholder="Mobile Number" value="{{ old('mobile_number') ?: $authUser->mobile_number }}">
                        @if ($errors->has('mobile_number'))
                        <label class="error">{{ $errors->first('mobile_number') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Title</label>
                    <div class="col-sm-6">
                        {!! Form::text('job_title', null, ['class' => 'form-control', 'placeholder' => 'Job Title']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Username <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ old('username') ?: $authUser->username }}">
                        @if ($errors->has('username'))
                        <label class="error">{{ $errors->first('username') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ old('email') ?: $authUser->email }}">
                        @if ($errors->has('email'))
                        <label class="error">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <span class="small">To change password, type here the new password.</span>
                        <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password">
                        @if ($errors->has('password'))
                        <label class="error">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password Confirmation <span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password">
                        @if ($errors->has('password_confirmation'))
                        <label class="error">{{ $errors->first('password_confirmation') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">&nbsp;</label>
                    <div class="col-sm-6 text-right">
                        <a href="{{ route('users') }}" class="btn btn-sm btn-danger">CANCEL</a>
                        <button type="submit" class="btn btn-success btn-sm">SAVE</button>
                    </div>
                </div>
            {!! Form::close() !!}
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ asset('vendor/birthday-picker-master/js/jquery-birthday-picker.js') }}"></script>
<script>
	$(document).ready(function(){

        var oldDate = ($(".old-birthday").val() != '') ? $(".old-birthday").val() : null;
		$("#birthdayPicker").birthdayPicker({
			"monthFormat" : "long",
            "defaultDate" : oldDate
		});

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.fileinput-preview').find('img').attr('src', e.target.result).closest('div').removeClass('hidden');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $('#file-browse').on('change', function(){
            readURL(this);
        });
	});
</script>
@stop
