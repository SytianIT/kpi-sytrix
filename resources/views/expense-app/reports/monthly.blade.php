@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Reports - Monthly</li>
@stop

@section('page-heading')
Reports - Monthly
@stop

@section('main')
@if($authUser->hasPermission('view_expense_syion_reports'))
<div class="panel">
	<div class="panel-body">
		<div class="filter-options filter-expenses-reports">
			{!! Form::open(['method' => 'GET']) !!}
			<div class="form form-inline">
				<div class="form-group">
					<label for="month" class="control-label font-size-11">MONTH</label>
					<select name="month" id="month" class="form-control">
						@foreach($monthsList as $key => $month)
						<option value="{{ $key }}" {{ ((Request::has('month') && Request::input('month') == $key) ? 'selected' : (!Request::has('month') && Carbon\Carbon::now()->format('F') == $month) ? 'selected' : '') }}>{{ $month }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="year" class="control-label font-size-11">YEAR</label>
					<select name="year" id="year" class="form-control">
						<?php 
							$yearsList = array_unique(array_sort($yearsList, function ($value){
														return $value;
													})); 
						?>
						@foreach($yearsList as $list)
						<option value="{{ $list }}" {{ (Request::has('year') && Request::input('year') == $list) ? 'selected' : ($firstExpense != null && intval($firstExpense->date_occured->format('Y')) == $list) ? 'selected' : Carbon\Carbon::now()->format('Y') == $list ? 'selected' : '' }}>{{ $list }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group go-btn-group">
					<label for="" class="control-label">&nbsp;</label>
					<button type="submit" class="btn btn-azure">GO</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<h3 class="title-hero font-bold">Expenses Monthly Breakdown</h3>
		<table class="table table-hover table-expense-list">
			<thead>
				<tr>
					<th class="text-center" width="5%">ID</th>
					<th class="text-center">Date</th>
					<th class="text-center">Name</th>
					<th class="text-center">Category</th>
					<th class="text-center">Mode of Payment</th>
					<th class="text-center">Total</th>
					<th class="text-center">Added by</th>
					<th width="10%" class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($curExpenses as $expense)
				<tr>
					<td class="text-center">@if($authUser->hasPermission('update_recurring_expense'))<a href="{{ route('expense.edit', $expense->id) }}">{{ $expense->id }}</a>@else{{ $expense->id }}@endif</td>
					<td class="text-center">{{ $expense->date_occured->format('Y/m/d') }}</td>
					<td class="text-center">{{ $expense->title }}</td>
					<td class="text-center">{{ $expense->category->title }}</td>
					<td class="text-center">{{ $expense->payment->title }}</td>
					<td class="text-center">PHP @money($expense->amount_in_php)</td>
					{{-- {{ $expense->currency->prefix }} @money($expense->amount) --}}
					<td class="text-center">{{ $expense->user->name }}</td>
					<td class="text-center">
						@if($authUser->hasPermission('update_expense'))
						<a href="{{ route('expense.edit', $expense->id) }}" class="btn btn-success btn-xs">EDIT</a>
						@endif
						@if($authUser->hasPermission('delete_expense'))
						<a href="{{ route('expense.delete', $expense->id) }}" class="confirmAlert btn btn-danger btn-xs confirmAlert" data-confirm-text="This action cannot be undone.">DELETE</a>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="5">No expenses found for this month!</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		<br>
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="{{ route('expense.reports.monthly')."?month={$intLastMonth}&year={$lastMonth->format('Y')}" }}" class="btn btn-primary"><i class="glyph-icon icon-angle-double-left" aria-hidden="true"></i> {{ $lastMonth->format('F Y') }}</a>
			</div>
			<div class="col-xs-12 col-sm-4 text-center">
				<p class="font-bold font-size-17">Total Expense: @money($totalExpense)</p>
			</div>
			<div class="col-xs-12 col-sm-4 text-right">
				<a href="{{ route('expense.reports.monthly')."?month={$intNextMonth}&year={$nextMonth->format('Y')}" }}" class="btn btn-primary"><i class="glyph-icon icon-angle-double-left" aria-hidden="true"></i> {{ $nextMonth->format('F Y') }}</a>
			</div>
		</div>
	</div>
</div>
@else
<h5>Lack of permission to view expense reports</h5>
@endif
@stop

@if($curExpenses->count() > 0)
@section('scripts')
<script>
	$(document).ready(function(){
		$('.table-expense-list').dataTable({
			searching: false,
            "paging": false,
            "columnDefs": [
				{ "orderable": false, "targets": 7 }
			]
		});
	});
</script>
@stop
@endif