@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Expense {{ $title }} Chart</li>
@stop

@section('page-heading')
Expense {{ $title }} Pie Chart
@stop

@section('main')
@if($authUser->hasPermission('view_expense_syion_reports'))
<div class="row page-pie-chart">
	<div class="col-xs-4">
		<div class="panel">
			<div class="panel-body">
				<div class="filter-options">
					<div class="form-group">
						{!! Form::open(['method' => 'GET']) !!}
						<label for="date-range" class="control-label font-size-11">DATE RANGE</label>
						<div class="input-group">
							<div class="input-prepend input-group">
								<span class="add-on input-group-addon">
									<i class="glyph-icon icon-calendar"></i>
								</span>
								<input type="text" name="filter_daterange" class="date-range-picker form-control" value="{{ Request::input('filter_daterange') }}" placeholder="...">
							</div>
							<span class="input-group-btn">
								<button class="btn btn-primary" type="submit">Go!</button>
							</span>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
				<h3 class="title-hero font-bold clear">Expenses by {{ $title }}</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="5%">Legend</th>
							<th>{{ $title }}</th>
							<th>Total Expense</th>
						</tr>
					</thead>
					<tbody>
						<?php $total = 0; ?>
						@foreach($itemList as $key => $catItem)
						<tr>
							<td class="text-center"><span id="legend-{{ $key }}" class="bs-label label-primary">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
							<td>{{ $catItem['title'] }}</td>
							<td>PHP @money($catItem['expense_total'])</td>
						</tr>
						<?php $total += $catItem['expense_total']; ?>
						@endforeach
					</tbody>
				</table>
				<div class="font-size-15 font-black font-bold">Balance: PHP @money($total)</div>
			</div>
		</div>
	</div>
	<div class="col-xs-8">
		<div class="panel">
			<div class="panel-body">
				<div class="panel">
					<div class="panel-body">
						<h3 class="title-hero font-bold">
							Graphical representation of expenses by <span class="text-transform-low">{{ $title }}</span> from <span class="font-red">{{ $startDay->toFormattedDateString() }}</span> to <span class="font-red">{{ $endDay->toFormattedDateString() }}</span>
						</h3>
						<div class="example-box-wrapper">
						@if($total < 1)
						<h4>No expenses found for current span of days.</h4>
						@endif
						<canvas id="chart-area-2" class="center-margin" width="300" height="150"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<h5>Lack of permission to view expense reports</h5>
@endif
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		/* Pie chart */

		function generateHexCode(num){
			function c() {
				var hex = Math.floor(Math.random()*256).toString(16);
				return ("0"+String(hex)).substr(-2); // pad with zero
			}
			
			var hexCode = "#"+c()+c()+c();
			$('#legend-'+num).css('background-color', hexCode);

			return hexCode;
		}

		var pieData = [
			<?php $ctr = 1; ?>
			@foreach($itemList as $key => $catItem)
			{
				value : {{ $catItem['expense_total'] }},
				color : generateHexCode({{ $key }}),
				label : "{{ $catItem['title'] }}",
			},
			<?php $ctr++; ?>
			@endforeach
		];

		var ctx = document.getElementById("chart-area-2").getContext("2d");
		window.myPie = new Chart(ctx).Pie(pieData, {
			responsive: true,
			hoverBorderColor : '#666'
		});
	});
</script>
@stop