@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Reports - Annual</li>
@stop

@section('page-heading')
Reports - Annual
@stop

@section('main')
@if($authUser->hasPermission('view_expense_syion_reports'))
<div class="row page-annual-report">
	<div class="col-xs-4">
		<div class="panel">
			<div class="panel-body">
				<h3 class="font-bold title-hero">
					This Year Monthly Expense
				</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Month</th>
							<th>Expenses</th>
						</tr>
					</thead>
					<tbody>
						<?php $total = 0; ?>
						@foreach($curExpenses as $key => $expense)
						<tr>
							<td>{{ $key.' '.$currentYear }}</td>
							<td>PHP @money($expense)</td>
						</tr>
						<?php $total += $expense; ?>
						@endforeach
					</tbody>
				</table>
				<div class="font-size-15 font-black font-bold">Balance: @money($total)</div>
			</div>
		</div>
	</div>
	<div class="col-xs-8">
		<div class="panel">
			<div class="panel-body">
				<h3 class="font-bold title-hero">
					Annual Bars Report &nbsp;&nbsp;&nbsp;&nbsp;
					<span class="legend-year legend-year-previous"></span>&nbsp;{{ $lastYear }}&nbsp;&nbsp;
					<span class="legend-year legend-year-current"></span>&nbsp;{{ $currentYear }}
				</h3>
				<div class="example-box-wrapper">
					<div id="labels-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						@if($lastYear != false)
						<a href="{{ route('expense.reports.annual') }}?year={{ $lastYear }}" class="btn btn-primary btn-xs"><i class="glyph-icon icon-angle-double-left" aria-hidden="true"></i>&nbsp;{{ $lastYear }}</a>
						@endif
					</div>
					<div class="col-xs-6 text-right">
						@if($nextYear != false)
						<a href="{{ route('expense.reports.annual') }}?year={{ $nextYear }}" class="btn btn-primary btn-xs">{{ $nextYear }}&nbsp;<i class="glyph-icon icon-angle-double-right" aria-hidden="true"></i></a>
						@endif
					</div>
				</div>
				<div class="text-left">
					
				</div>
			</div>
		</div>
	</div>
</div>
@else
<h5>Lack of permission to view expense reports</h5>
@endif
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		$(function() {
	   		"use strict";
		    var day_data = [
		    @if($curExpenses != false && $lastExpenses != false)
			@foreach($curExpenses as $key => $item)
				{
					"period": "{{ $key }}",
					"{{ $lastYear }}": {{ $lastExpenses[$key] }},
		        	"{{ $currentYear }}": {{ $item }}
				},
			@endforeach
			@endif
		    ];
		    Morris.Bar({
		        element: 'labels-bar',
		        data: day_data,
		        xkey: 'period',
		        ykeys: ['{{ $lastYear }}', '{{ $currentYear }}'],
		        labels: ['{{ $lastYear }}', '{{ $currentYear }}'],
		        xLabelAngle: 60,
		        barColors: ['#98B1D3', '#3070CF'],
		    });
		});
	});
</script>
@stop