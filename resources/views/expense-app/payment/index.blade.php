@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Mode of Payment</li>
@stop

@section('page-heading')
Mode of Payment
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
<div class="panel">
	<div class="panel-body" ng-controller="PaymentController as paymentController">
		<h3 class="title-hero">
			List of Mode of Payment
		</h3>
		@if($authUser->hasPermission('access_expense_settings'))
		<div class="example-box-wrapper">
			<div class="size-md row">
				<div class="col-xs-1">
					<button class="btn btn-azure btn-md" ng-click="paymentController.startCreating();">CREATE<div class="ripple-wrapper"></div></button>
				</div>
				<div class="col-xs-11 creating-payment-col" ng-show="paymentController.isCreating">
					<div class="validation-inputs form form-inline">
						<div class="form-group">
							<input id="creating" type="text" class="payment-name-input form-control input-keypress" data-click-target="#submit-creation-payment" placeholder="Payment Title..." ng-my-focus="paymentController.isCreating" ng-model="paymentController.paymentTitle" required>
						</div>
						<div class="form-group">
							<button id="submit-creation-payment" class="btn btn-success btn-submit-angular" ng-click="paymentController.createPayment()">Go!</button>
						</div>
					</div>
					<div class="validation-response" ng-show="paymentController.isNotValid">
						<div class="alert alert-close alert-danger mrg0B pad5A">
							<div class="alert-content">
								<p>Payment title cannot be empty.</p>
							</div>
						</div>
					</div>
					<div class="validation-response" ng-show="paymentController.noDuplicate">
						<div class="alert alert-close alert-danger mrg0B pad5A">
							<div class="alert-content">
								<p>Payment title already exist.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
				<thead>
					<tr>
						<th width="85%">Name</th>
						<th width="10%" class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="payment in paymentController.servicePayment.paymentList track by $index">
						<td>
							<span ng-if="payment != paymentController.editing" class="font-size-13 font-black">
								[[payment.title]]
							</span> 
							<input type="text" class="form-control" ng-show="payment == paymentController.editing" ng-my-focus="payment == paymentController.editing" ng-model="paymentController.title" ng-value="paymentController.editing.title" ng-my-enter="paymentController.updatePayment()" ng-blur="paymentController.updatePayment()">
						</td>
						<td class="text-center">
							<a href="javascript:;" class="btn btn-xs btn-success" ng-click="paymentController.setEditing(payment)">EDIT</a>
							<a href="javascript:;" ng-show="payment.hasExpenses != true" class="btn btn-xs btn-danger" confirmed-click="paymentController.deletePayment(payment)" ng-confirm-click="">DELETE</a>
							<a href="javascript:;" ng-show="payment.hasExpenses == true" class="btn btn-xs btn-danger" ng-cannot-click="" ng-alert-text="Please be aware that this payment was used by [[payment.countExpenses]] expense entry, delete these records first before you can delete this; [[payment.messageExpense]]">DELETE</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		@else
		<h5>Lack of permission to view list of payment method.</h5>
		@endif
	</div>
</div>
@stop

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-resource.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-sanitize.js"></script>
<script src="{{ asset('js/ng-app.js') }}"></script>
@stop