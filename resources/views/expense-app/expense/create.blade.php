@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Create Expense</li>
@stop

@section('page-heading')
Create Expense
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
@if($authUser->hasPermission('create_expense') || $authUser->hasPermission('create_recurring_expense'))
<div class="row">
	{!! Form::open(['route' => 'expense.store', 'method' => 'POST', 'id' => 'form-creating-expense', 'enctype' => 'multipart/form-data']) !!}
	<div class="col-xs-12 col-sm-9">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold">
					Details
				</h3>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="">Date <span class="req">*</span></label>
							<div class="input-prepend input-group">
								<span class="add-on input-group-addon">
									<i class="glyph-icon icon-calendar"></i>
								</span>
								{!! Form::text('date_occured', Carbon\Carbon::now()->format('m/d/Y'), ['class' => 'bootstrap-datepicker form-control', 'id' => 'date-months-picker']) !!}
							</div>
						</div>
						<div class="form-group">
							<label>Name <span class="req">*</span></label>
							<div class="">
								{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="category">Category</label>
							<select name="category" id="category" class="form-control">
								<option value="">Choose category...</option>
								@foreach($category as $item)
								<?php
								$hyp = '';
								if($item->child_level > 0){
									for($i = 1; $i <= $item->child_level; $i++){
										$hyp .= '-';
									}
								}
								?>
								<option value="{{ $item->id }}">{{ $hyp.' '.$item->title }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6 col-sm-3">
									<label for="currency">Currency <span class="req">*</span></label>
									<select name="currency" id="currency" class="form-control">
										@forelse($currencies as $currency)
										<option value="{{ $currency->id }}">{{ $currency->prefix }}</option>
										@empty
										<option value="">No currency found</option>
										@endforelse
									</select>
								</div>
								<div class="col-xs-5">
									<label for="">Amount <span class="req">*</span></label>
									{!! Form::text('amount', null, ['class' => 'form-control', 'id' => 'amount']) !!}
								</div>
								<div class="col-xs-4">
									<label for="payment">Payment Method <span class="req">*</span></label>
									{!! Form::select('payment', ($payments->isEmpty()) ? ['' => 'No payments found'] : $payments, null, ['class' => 'form-control','id' => 'payment']) !!}
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<label for="">Description</label>
							{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
						</div>
					</div>
				</div>
				<div class="form-group row">
					@if(isset($isRecurring))
					<div class="col-xs-12 mrg20T">
						<h3 class="title-hero font-bold">
							Recurring Settings
						</h3>
						<div class="form form-inline">
							<div class="form-group">
								<label for="">Recur Every</label>&nbsp;
								{!! Form::text('recur_every', null, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::select('recur_of', App\Applications\Expense\Recurring::$recurEveryEntities, null, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								&nbsp;<label for="">for</label>&nbsp;
								{!! Form::text('recur_times', null, ['class' => 'form-control']) !!}
								&nbsp;<label for="">Times (0 for unlimited)</label>&nbsp;
							</div>
							{!! Form::hidden('is_recurring', true) !!}
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-3">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold">
					Options
				</h3>
				@if(isset($isRecurring))
				<div class="form-group">
					<label for="status">Status</label>
					{!! Form::select('status', $status, null, ['class' => 'form-control','id' => 'status']) !!}
				</div>
				@endif
				<div class="form-group form-file-inputs">
					<label class="control-label mrg10B">Attachments:&nbsp;<a href="javascript:;" class="btn-add-image-row btn btn-success btn-xs">ADD IMAGE</a></label>
					<input type="hidden" name="attachments_count" id="attachments_count" class="attachments_count">
					<div class="file-row thumbnail-box" data-counter="1">
						<div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                    	<a href="javascript:;" data-toggle="modal" data-target="#modal-preview-image" data-path="" class="btn-preview-modal-image btn btn-lg btn-round btn-success" title="View"><i class="glyph-icon icon-eye"></i></a>
                                        <a href="javascript:;" class="btn-remove-attachment btn btn-lg btn-round btn-danger new-attachment" title="Delete"><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-black"></div>
						<img src="" alt="" class="file-preview-image img-responsive">
						<input type="file" name="attachment_1" class="hidden file-browse-input" autocomplete="off">
					</div>
				</div>
				<div class="form-group text-right">
					<a href="javascript:;" onclick="window.history.back();" class="btn btn-sm btn-danger">CANCEL</a>
					<button type="submit" class="btn btn-sm btn-success">CREATE</button>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@else
<h5>Lack of permission to create expense</h5>
@endif
@stop

@section('scripts')
@include('expense-app.expense.inc.scripts')
@stop