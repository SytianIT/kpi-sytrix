<script>
	$(document).ready(function(){

		var currentDate = new Date(),
			earlierDate = new Date(currentDate.setMonth(currentDate.getMonth() - 2));

		$("#date-months-picker").bsdatepicker({
            autoclose : true,
        });

        $('#amount').on('change', function(){
        	var $val = $(this).val(),
        		$newVal = $val.replace(/[^0-9a-zA-Z.]/g , "");

        	$(this).val($newVal);
        });

        $('.btn-add-image-row').on('click', function(){
            var row = $('.file-row:last-child'),
                counter = $('.file-row:last-child').data('counter'),
                newCounter = parseInt(counter + 1),
                cloned = row.clone();

            if($('.form-file-inputs .attachments_count + .file-row input').val() != ''){
                cloned.attr('data-counter', newCounter);
                cloned.find('img').attr('src', null);
                cloned.find('input').val('');
                cloned.find('input').attr('name', 'attachment_'+newCounter);
                $('.file-row:last-child').after(cloned);
                cloned.find('input').click();
                cloned.show();
            } else {
                $('.form-file-inputs .file-row').show();
                $('.form-file-inputs .file-row').find('input').click();
            }
            $('#attachments_count').val($('.file-row').length);
        });

        function updateCounterAndClearThisAttachmentInputs(box, counter, empty){
            box.attr('data-counter', counter);
            box.find('input').attr('name', 'attachment_'+counter);
            if(empty){
                $('#attachments_count').val(0);
                box.find('img').attr('src', null);
                box.find('input').val('');
            } else {
                $('#attachments_count').val(counter);
            }
        }

        $('body').on('click', '.btn-remove-attachment', function(){
            var imgPath = $(this).data('path'),
                currVal = $('#for-deletion-attachments').val();

            if(!$(this).hasClass('new-attachment')){
                if(currVal == ''){
                    currVal = imgPath;
                } else {
                    currVal += ', '+imgPath;
                }

                $('#for-deletion-attachments').val(currVal);
            }

            if($(this).hasClass('new-attachment')){
                if($('.file-row.thumbnail-box').length < 2){
                    $(this).closest('.thumbnail-box').fadeOut();
                    updateCounterAndClearThisAttachmentInputs($(this).closest('.thumbnail-box'), 1, true);
                } else {
                    $(this).closest('.thumbnail-box').fadeOut(function(){
                        $(this).remove();
                        $i = 1;
                        $('.file-row').each(function(){
                            updateCounterAndClearThisAttachmentInputs($(this), $i, false);
                            $i++;
                        });
                    });
                }
            } else {
                $(this).closest('.thumbnail-box').fadeOut(function(){
                    $(this).remove();
                });
            }
           
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).siblings('.file-preview-image').attr('src', e.target.result);
                    $(input).closest('.thumbnail-box').find('.btn-preview-modal-image').attr('data-path', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('body').on('change', '.file-browse-input', function(){
            readURL(this);
        });

        $('#btn-submit-expense').on('click', function(e){
            e.preventDefault();

            $i = 1;
            $('.file-row.thumbnail-box').each(function(){
                if($(this).find('input').val() != ''){
                    updateCounterAndClearThisAttachmentInputs($(this), $i, false);
                    $i++;
                } else {
                    $(this).remove();
                }
            });

            $('#form-creating-expense').submit();
        });
	});
</script>