<div class="filter-options filter-expenses-list">
	{!! Form::open(['method' => 'GET']) !!}
	<div class="form form-inline">
		<div class="form-group">
			<label for="date-range" class="control-label font-size-11">SEARCH KEY</label>
			<input type="text" name="filter_key" class="form-control" value="{{ Request::input('filter_key') }}" placeholder="...">
		</div>
		@if(!isset($recurring))
		<div class="form-group">
			<label for="date-range" class="control-label font-size-11">DATE RANGE</label>
			<div class="input-prepend input-group">
				<span class="add-on input-group-addon">
					<i class="glyph-icon icon-calendar"></i>
				</span>
				<input type="text" name="filter_daterange" class="date-range-picker form-control" value="{{ Request::input('filter_daterange') }}" placeholder="...">
			</div>
		</div>
		@endif
		<div class="form-group">
			<label for="date-range" class="control-label font-size-11">CATEGORY</label>
			<select name="filter_category" id="filter-category" class="form-control">
				<option value="">Select category...</option>
				@foreach($category as $item)
				<?php
				$hyp = '';
				if($item->child_level > 0){
					for($i = 1; $i <= $item->child_level; $i++){
						$hyp .= '-';
					}
				}
				?>
				<option value="{{ $item->id }}" {{ (Request::input('filter_category') == $item->id) ? 'selected' : '' }}>{{ $hyp.' '.$item->title }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="date-range" class="control-label font-size-11">PAYMENT METHOD</label>
			<select name="filter_payment_method" id="filter-user" class="form-control">
				<option value="">Select payment...</option>
				@foreach($payments as $key => $payment)
				<option value="{{ $key }}" {{ (Request::input('filter_payment_method') == $key) ? 'selected' : '' }}>{{ $payment }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group go-btn-group">
			<label for="" class="control-label">&nbsp;</label>
			<button type="submit" class="btn btn-azure">GO</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>