@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Recurring Expenses</li>
@stop

@section('page-heading')
Recurring Expenses
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
<div class="panel">
	<div class="panel-body">
		<?php $recurring = true; ?>
		@if($authUser->hasPermission('create_recurring_expense'))
		<div class="pull-right">
			<a href="{{ route('expense-recurring.add') }}" class="btn btn-success">ADD RECURRING EXPENSE</a>
		</div>
		@endif
		@if($authUser->hasPermission('view_recurring_expenses'))
		@include('expense-app.expense.inc.filter-options')
		<table class="table table-hover table-bordered table-expense-list" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th class="text-center" width="5%">ID</th>
			    <th width="20%">Category</th>
				<th width="18%">Name</th>
				<th width="8%">Amount</th>
				<th width="12%">Next Recurring Date</th>
				<th width="12%">Payment method</th>
				<th width="15%">Added by</th>
				<th width="8%">Status</th>
				<th class="text-center" width="12%" style="min-width: 110px">Actions</th>
			</tr>
			</thead>
			<tbody>
				@forelse($expenses as $expense)
				<tr>
					<td class="text-center">@if($authUser->hasPermission('update_recurring_expense'))<a href="{{ route('expense.edit', $expense->id) }}">{{ $expense->id }}</a>@else{{ $expense->id }}@endif</td>
					<td>{{ $expense->category->title }}</td>
					<td>{{ $expense->title }}</td>
					<td>{{ $expense->currency->prefix }}&nbsp;@money($expense->amount)</td>
					<td>{{ ($expense->recurring->next_expense_recurring) ? $expense->recurring->next_expense_recurring->format('m/d/Y') : 'n/a' }}</td>
					<td>{{ $expense->payment->title }}</td>
					<td>{{ $expense->user->name }}</td>
					<td class="text-center"><span class="bs-label {{ App\Applications\Expense\Recurring::$statusClass[$expense->recurring->status] }} text-uppercase">{{ $expense->recurring->status }}</span></td>
					<td class="text-center">
						@if($authUser->hasPermission('update_recurring_expense'))
						<a href="{{ route('expense.edit', $expense->id) }}" class="btn btn-success btn-xs">EDIT</a>
						@endif
						@if($authUser->hasPermission('delete_recurring_expense'))
						<a href="{{ route('expense.delete', $expense->id) }}" class="btn btn-danger btn-xs confirmAlert" data-confirm-text="This action cannot be undone.">DELETE</a>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="7">No results found!</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		@if($expenses->count() > 0)
		<div class="text-right">
		@if(isset($filterDate))
		{!! $expenses->appends($filterDate)->render() !!}
		@else
		{!! $expenses->render() !!}
		@endif
		</div>
		@endif
		@else
		<h5>Lack of permission to view list of recurring expenses.</h5>
		@endif
	</div>
</div>
@stop

@if($expenses->count() > 0)
@section('scripts')
<script>
	$(document).ready(function(){
		$('.table-expense-list').dataTable({
			searching: false,
            "paging": false,
            "columnDefs": [
				{ "orderable": false, "targets": 7 }
			]
		});
	});
</script>
@stop
@endif