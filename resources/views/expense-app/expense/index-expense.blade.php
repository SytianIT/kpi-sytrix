@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Expenses</li>
@stop

@section('page-heading')
Expenses
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
<div class="panel">
	<div class="panel-body">
		@if($authUser->hasPermission('create_expense'))
		<div class="pull-right">
			<a href="{{ route('expense.add') }}" class="btn btn-success">ADD EXPENSE</a>
		</div>
		@endif
		@if($authUser->hasPermission('view_expenses'))
		@include('expense-app.expense.inc.filter-options')
		<table class="table table-hover table-bordered table-expense-list" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th class="text-center" width="5%">ID</th>
				<th width="10%">Date</th>
			    <th width="20%">Category</th>
				<th width="18%">Name</th>
				<th width="12%">Amount</th>
				<th width="12%">Payment method</th>
				<th width="15%">Added by</th>
				<th class="text-center" width="15%">Actions</th>
			</tr>
			</thead>
			<tbody>
				@forelse($expenses as $expense)
				<tr>
					<td class="text-center">@if($authUser->hasPermission('update_recurring_expense'))<a href="{{ route('expense.edit', $expense->id) }}">{{ $expense->id }}</a>@else{{ $expense->id }}@endif</td>
					<td>{{ $expense->date_occured->format('m/d/Y') }}</td>
					<td>{{ $expense->category->title }}</td>
					<td>{{ $expense->title }}</td>
					<td>{{ $expense->currency->prefix }}&nbsp;@money($expense->amount)</td>
					<td>{{ $expense->payment->title }}</td>
					<td>{{ $expense->user->name }}</td>
					<td class="text-center">
						@if($authUser->hasPermission('update_expense'))
						<a href="{{ route('expense.edit', $expense->id) }}" class="btn btn-success btn-xs">EDIT</a>
						@endif
						@if($authUser->hasPermission('delete_expense'))
						<a href="{{ route('expense.delete', $expense->id) }}" class="confirmAlert btn btn-danger btn-xs confirmAlert" data-confirm-text="This action cannot be undone.">DELETE</a>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="7">No results found!</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		@if($expenses->count() > 0)
		<div class="text-right">
		@if(isset($filterDate))
		{!! $expenses->appends($filterDate)->render() !!}
		@else
		{!! $expenses->render() !!}
		@endif
		</div>
		@endif
		@else
		<h5>Lack of permission to view list of expenses.</h5>
		@endif
	</div>
</div>
@stop

@if($expenses->count() > 0)
@section('scripts')
<script>
	$(document).ready(function(){
		$('.table-expense-list').dataTable({
			searching: false,
            "paging": false,
            "columnDefs": [
				{ "orderable": false, "targets": 7 }
			]
		});
	});
</script>
@stop
@endif