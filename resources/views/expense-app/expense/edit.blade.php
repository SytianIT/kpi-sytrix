@extends('templates.master')
<?php $isRecurring = $expense->recurring; ?>

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li><a href="{{ ($expense->recurring) ? route('expense-recurring') : route('expense') }}">Expenses</a></li>
<li class="active">Edit Expense</li>
@stop

@section('page-heading')
{{ $isRecurring ? 'Edit Recurring Expense' : 'Edit Expense' }}
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
@if($authUser->hasPermission('update_expense') || $authUser->hasPermission('update_recurring_expense'))
<div class="row">
	{!! Form::model($expense, ['route' => ['expense.update', $expense->id], 'method' => 'POST', 'id' => 'form-creating-expense', 'enctype' => 'multipart/form-data']) !!}
	<div class="col-xs-12 col-sm-9">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold">
					Details
				</h3>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Date <span class="req">*</span></label>
							<div class="input-prepend input-group">
								<span class="add-on input-group-addon">
									<i class="glyph-icon icon-calendar"></i>
								</span>
								{!! Form::text('date_occured', $expense->date_occured->format('m/d/Y'), ['class' => 'form-control', 'id' => 'date-months-picker']) !!}
							</div>
						</div>
						<div class="form-group">
							<label>Name <span class="req">*</span></label>
							<div class="">
								{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="category">Category</label>
							<select name="category" id="category" class="form-control">
								<option value="">Choose category...</option>
								@foreach($category as $item)
								<?php
								$hyp = '';
								if($item->child_level > 0){
									for($i = 1; $i <= $item->child_level; $i++){
										$hyp .= '-';
									}
								}
								?>
								<option value="{{ $item->id }}" {{ $expense->category_id == $item->id ? 'selected' : '' }}>{{ $hyp.' '.$item->title }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6 col-sm-3">
									<label for="currency">Currency <span class="req">*</span></label>
									<select name="currency" id="currency" class="form-control">
										@forelse($currencies as $currency)
										<option value="{{ $currency->id }}" {{ $expense->currency->id == $currency->id ? 'selected' : '' }}>{{ $currency->prefix }}</option>
										@empty
										<option value="">No currency found</option>
										@endforelse
									</select>
								</div>
								<div class="col-xs-5">
									<label for="">Amount <span class="req">*</span></label>
									{!! Form::text('amount', $expense->amount, ['class' => 'form-control', 'id' => 'amount']) !!}
								</div>
								<div class="col-xs-4">
									<label for="payment">Payment Method <span class="req">*</span></label>
									{!! Form::select('payment', ($payments->isEmpty()) ? ['' => 'No payments found'] : $payments, $expense->payment->id, ['class' => 'form-control','id' => 'payment']) !!}
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<label for="">Description</label>
						{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
					</div>
				</div>
				<div class="form-group row">
					@if($expense->recurring)
					<div class="col-xs-12 mrg20T">
						<h3 class="title-hero font-bold">
							Recurring Settings
						</h3>
						<div class="alert alert-warning pad5A font-size-13">
							<p>Changes in recurring settings will result in reset of recurring status (EX: next recurring date.)</p>
						</div>
						<div class="form form-inline">
							<div class="form-group">
								<label for="">Recur Every</label>&nbsp;
								{!! Form::text('recur_every', $expense->recurring->recurring_every, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::select('recur_of', App\Applications\Expense\Recurring::$recurEveryEntities, $expense->recurring->recurring_of, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								&nbsp;<label for="">for</label>&nbsp;
								{!! Form::text('recur_times', $expense->recurring->recurring_times, ['class' => 'form-control']) !!}
								&nbsp;<label for="">Times (0 for unlimited)</label>&nbsp;
							</div>
							{!! Form::hidden('is_recurring', true) !!}
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-3">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold">
					Options
				</h3>
				@if($expense->recurring)
				<div class="form-group">
					<label for="status">Status</label>
					{!! Form::select('status', $status, $expense->recurring != null ? $expense->recurring->status : null, ['class' => 'form-control','id' => 'status']) !!}
				</div>
				<div class="row form-group">
					<label class="col-sm-4 control-label">Next Recurring:</label><p class="col-sm-8 text-right">{{ ($expense->recurring->next_expense_recurring) ? $expense->recurring->next_expense_recurring->format('F d, Y') : 'n/a' }}</p>
				</div>
				<div class="row form-group">
					<label class="col-sm-5 control-label">Recurring Count:</label><p class="col-sm-7 text-right">{{ $expense->recurring->count_expense_recurring }}</p>
				</div>
				<div class="row form-group">
					<label class="col-sm-6 control-label">Total Amount Occured:</label><p class="col-sm-6 text-right">@money($expense->recurring->total_expense_recurring)</p>
				</div>
				@endif
				<div class="row form-group">
					<label class="col-sm-4 control-label">Added by:</label><p class="col-sm-8 text-right">{{ $expense->user->name }}</p>
				</div>
				<div class="row form-group">
					<label class="col-sm-4 control-label">Created:</label><p class="col-sm-8 text-right">{{ $expense->created_at->format('m/d/Y @ h:m A') }}</p>
				</div>
				<div class="row form-group">
					<label class="col-sm-4 control-label">Updated last:</label><p class="col-sm-8 text-right">{{ $expense->updated_at->format('m/d/Y @ h:m A') }}</p>
				</div>
				<div class="form-group form-file-inputs attachments">
					<label class="control-label mrg10B">Attachments:&nbsp;<a href="javascript:;" class="btn-add-image-row btn btn-success btn-xs">ADD IMAGE</a></label>
					@if(count($expense->attachments) > 0)
					@foreach($expense->attachments as $attachment)
					<div class="thumbnail-box box-attachment">
                        <div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                        <a href="javascript:;" data-toggle="modal" data-target="#modal-preview-image" data-path="{{ asset($attachment) }}" class="btn-preview-modal-image btn btn-lg btn-round btn-success" title="View"><i class="glyph-icon icon-eye"></i></a>
                                        <a href="javascript:;" class="btn-remove-attachment btn btn-lg btn-round btn-danger" data-path="{{ $attachment }}" title="Delete"><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-black"></div>
                        <img src="{{ asset($attachment) }}" alt="{{ $expense->title }}" class="img-responsive">
                    </div>
					@endforeach
					@endif
					<input type="hidden" name="for_deletion" id="for-deletion-attachments">
					<input type="hidden" name="attachments_count" id="attachments_count" class="attachments_count">
					<div class="file-row thumbnail-box" data-counter="1">
						<div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                    	<a href="javascript:;" data-toggle="modal" data-target="#modal-preview-image" class="btn-preview-modal-image btn btn-lg btn-round btn-success" title="View"><i class="glyph-icon icon-eye"></i></a>
                                        <a href="javascript:;" class="btn-remove-attachment btn btn-lg btn-round btn-danger new-attachment" title="Delete"><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-black"></div>
						<img src="" alt="" class="file-preview-image img-responsive">
						<input type="file" name="attachment_1" class="hidden file-browse-input" autocomplete="off">
					</div>
				</div>
				<div class="form-group text-right">
					<a href="javascript:;" onclick="window.history.back();" class="btn btn-sm btn-danger">CANCEL</a>
					<button type="submit" id="btn-submit-expense" class="btn btn-sm btn-success">UPDATE</button>
					@if($authUser->hasPermission('delete_recurring_expense') || $authUser->hasPermission('delete_expense'))
					<a href="{{ route('expense.delete', $expense->id) }}" class="confirmAlert btn btn-sm btn-danger">DELETE</a>
					@endif
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@else
<h5>Lack of permission to update expense details.</h5>
@endif
@stop

@section('scripts')
@include('expense-app.expense.inc.scripts')
@stop