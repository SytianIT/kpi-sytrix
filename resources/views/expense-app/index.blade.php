@extends('templates.master')

@section('page-heading')
Expense Dashboard
@stop

@section('main')
@if($authUser->hasPermission('access_expense'))
<div class="row">
	<div class="col-xs-7">
		@if($authUser->hasPermission('view_expense_syion_reports'))
		<div class="row mrg20B">
			<div class="col-xs-4">
				<div class="tile-box bg-info no-border">
					<div class="tile-header">
						{{ Carbon\Carbon::today()->subMonth(2)->format('F Y') }} Expenses
					</div>
					<div class="tile-content-wrapper">
						<i class="glyph-icon icon-dashboard"></i>
						<div class="tile-content">
							<span>Php</span>
							@money($ttlExpLastLastMon)
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="tile-box bg-azure">
					<div class="tile-header">
						{{ Carbon\Carbon::today()->subMonth(1)->format('F Y') }} Expenses
					</div>
					<div class="tile-content-wrapper">
						<i class="glyph-icon icon-dashboard"></i>
						<div class="tile-content">
							<span>Php</span>
							@money($ttlExpLastMon)
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="tile-box bg-blue">
					<div class="tile-header">
						{{ Carbon\Carbon::now()->format('F Y') }} Expenses
					</div>
					<div class="tile-content-wrapper">
						<i class="glyph-icon icon-dashboard"></i>
						<div class="tile-content">
							<span>Php</span>
							@money($ttlExpThisMon)
						</div>
					</div>
				</div>
			</div>
		</div>
		@else
		<h5>Lack of permission to view expense reports.</h5>
		@endif
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold">Recurring expenses for this month: <strong class="font-red">{{ strtoupper(Carbon\Carbon::now()->format('F')) }}</strong></h3>
				@if($authUser->hasPermission('view_recurring_expenses'))
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="40%">NAME</th>
							<th width="30%">DATE OCCURANCE</th>
							<th width="20%">AMOUNT</th>
						</tr>
					</thead>
					<tbody>
						@forelse($recExpenses as $recExpense)
						<tr>
							<td><a href="{{ route('expense.edit', $recExpense->expense->id) }}">{{ $recExpense->expense->title }}</a></td>
							<td>{{ $recExpense->next_expense_recurring->format('F d, Y') }}</td>
							<td>{{ $recExpense->expense->currency->prefix }}&nbsp;@money($recExpense->expense->amount)</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No results to display!</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				@else
				<h5>Lack of permission to view list of recurring expense.</h5>
				@endif
			</div>
		</div>
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold">Expenses for this month: <strong class="font-red">{{ strtoupper(Carbon\Carbon::now()->format('F')) }}</strong>
					<div class="float-right">
						<?php $dateRange = htmlentities(Carbon\Carbon::today()->startOfMonth()->format('m/d/Y').' - '.Carbon\Carbon::today()->endOfMonth()->format('m/d/Y')); ?>
						<a href="{{ route('expense') }}?filter_daterange={{ $dateRange }}" class="btn btn-primary btn-xs">VIEW ALL</a>
					</div>
				</h3>
				@if($authUser->hasPermission('view_expenses'))
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="40%">NAME</th>
							<th width="30%">DATE OCCURANCE</th>
							<th width="20%">AMOUNT</th>
						</tr>
					</thead>
					<tbody>
						@forelse($expenses as $expense)
						<tr>
							<td><a href="{{ route('expense.edit', $expense->id) }}">{{ $expense->title }}</a></td>
							<td>{{ $expense->date_occured->format('F d, Y') }}</td>
							<td>{{ $expense->currency->prefix }}&nbsp;@money($expense->amount)</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No results to display!</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				@else
				<h5>Lack of permission to view expense list.</h5>
				@endif
			</div>
		</div>
	</div>
	<div class="col-xs-5">
		<div class="panel">
			<div class="panel-body">
				<h3 class="title-hero font-bold clearfix">
					Audit Logs
					<div class="float-right">
						<a href="{{ route('expense.logs') }}" class="btn btn-primary btn-xs">VIEW ALL</a>
					</div>
				</h3>
				@if($authUser->hasPermission('view_expense_syion_log'))
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="20%">User</th>
							<th width="40%">Activity</th>
							<th width="30%" class="text-right">Timestamp</th>
						</tr>
					</thead>
					<tbody>
						@forelse($logs as $log)
						<tr>
							<td>{{ $log->user->name }}</td>
							<td>{{ $log->activity_desc }}</td>
							<td class="text-right">{{ $log->created_at->format('Y-m-d h:m A') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@else
				<h5>Lack of permission to view expense logs.</h5>
				@endif
			</div>
		</div>
	</div>
</div>
@else
<h5>Lack of permission view expense records</h5>
@endif
@stop