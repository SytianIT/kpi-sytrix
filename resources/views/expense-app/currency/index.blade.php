@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Currencies</li>
@stop

@section('page-heading')
Currencies
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
<div class="panel">
	<div class="panel-body" ng-controller="CurrencyController as currencyController">
		<h3 class="title-hero">
			List of Currency
		</h3>
		@if($authUser->hasPermission('access_expense_settings'))
		<div class="example-box-wrapper">
			<div class="size-md row">
				<div class="col-xs-1">
					<button class="btn btn-azure btn-md" ng-click="currencyController.startCreating();">CREATE<div class="ripple-wrapper"></div></button>
				</div>
				<div class="col-xs-11 creating-currency-col" ng-show="currencyController.isCreating">
					<div class="validation-inputs form form-inline">
						<div class="form-group">
							<input id="creating-prefix" type="text" class="currency-prefix-input form-control input-keypress" placeholder="Currency Prefix..." ng-my-focus="currencyController.isCreating" ng-model="currencyController.currencyPrefix" required>
						</div>
						<div class="form-group">
							<input id="creating" type="text" class="currency-name-input form-control input-keypress" data-click-target="#submit-creation-currency" placeholder="Currency Title..." ng-model="currencyController.currencyTitle" required>
						</div>
						<div class="form-group">
							<input id="creating-conversion" type="text" class="currency-name-input form-control input-keypress tooltip-button" data-toggle="tooltip" data-placement="top" title="Conversion rate from PHP to this currency" data-click-target="#submit-creation-currency" placeholder="Currency Conversion..." ng-model="currencyController.currencyRate" required>
						</div>
						<div class="form-group">
							<button id="submit-creation-currency" class="btn btn-success btn-submit-angular" ng-click="currencyController.createCurrency()">Go!</button>
						</div>
					</div>
					<div class="validation-response" ng-show="currencyController.isNotValid">
						<div class="alert alert-close alert-danger mrg0B pad5A">
							<div class="alert-content">
								<p>Currency prefix, conversion rate or title cannot be empty.</p>
							</div>
						</div>
					</div>
					<div class="validation-response" ng-show="currencyController.noDuplicate">
						<div class="alert alert-close alert-danger mrg0B pad5A">
							<div class="alert-content">
								<p>Currency prefix or title is already exist.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
				<thead>
					<tr>
						<th width="15%">Prefix</th>
						<th width="20%">Name</th>
						<th width="30%">Conversion Rate</th>
						<th width="10%" class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="currency in currencyController.serviceCurrency.currencyList track by $index">
						<td>
							<span ng-if="currency != currencyController.editing" class="font-size-13 font-black">
								[[currency.prefix]]
							</span> 
							<input type="text" class="form-control" ng-show="currency == currencyController.editing" ng-my-focus="currency == currencyController.editing" ng-model="currencyController.prefix" ng-value="currencyController.editing.prefix" ng-my-enter="currencyController.updateCurrency()">
						</td>
						<td>
							<span ng-if="currency != currencyController.editing" class="font-size-13 font-black">
								[[currency.title]]
							</span> 
							<input type="text" class="form-control" ng-show="currency == currencyController.editing" ng-my-focus="currency == currencyController.editing" ng-model="currencyController.title" ng-value="currencyController.editing.title" ng-my-enter="currencyController.updateCurrency()">
						</td>
						<td>
							<span ng-if="currency != currencyController.editing" class="font-size-13 font-black tooltip-button">
								[[currency.conversion_rate]]
							</span> 
							<input type="text" class="form-control tooltip-button" ng-show="currency == currencyController.editing" ng-my-focus="currency == currencyController.editing" ng-model="currencyController.conversion_rate" ng-value="currencyController.editing.conversion_rate" ng-my-enter="currencyController.updateCurrency()">
						</td>
						<td class="text-center">
							<a href="javascript:;" class="btn btn-xs btn-success" ng-click="currencyController.setEditing(currency)">EDIT</a>
							<a href="javascript:;" ng-hide="currency.id == 1" ng-show="currency.hasExpenses != true" class="btn btn-xs btn-danger" confirmed-click="currencyController.deleteCurrency(currency)" ng-confirm-click="">DELETE</a>
							<a href="javascript:;" ng-show="currency.hasExpenses == true" class="btn btn-xs btn-danger" ng-cannot-click="" ng-alert-text="Please be aware that this currency was used by [[currency.countExpenses]] expense entry, delete these records first before you can delete this; [[currency.messageExpenses]]">DELETE</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		@else
		<h5>Lack of permission to view list of currencies.</h5>
		@endif
	</div>
</div>
@stop

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-resource.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-sanitize.js"></script>
<script src="{{ asset('js/ng-app.js') }}"></script>
@stop