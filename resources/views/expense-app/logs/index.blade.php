@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Logs</li>
@stop

@section('page-heading')
Logs
@stop

@section('main')
<div class="panel">
	<div class="panel-body">
		<div class="filter-options filter-expenses-logs">
			{!! Form::open(['method' => 'GET']) !!}
			<div class="form form-inline">
				<div class="form-group">
					<div class="form-group">
						<label for="date-range" class="control-label font-size-11">DATE RANGE</label>
						<div class="input-prepend input-group">
							<span class="add-on input-group-addon">
								<i class="glyph-icon icon-calendar"></i>
							</span>
							<input type="text" name="filter_daterange" class="date-range-picker form-control" value="{{ Request::input('filter_daterange') ?: old('filter_daterange') }}" placeholder="...">
						</div>
					</div>
					<div class="form-group">
						<label for="filter_user_name" class="control-label font-size-11">USER NAME</label>
						<input type="text" name="filter_user_name" id="filter_user_name" class="form-control" value="{{ Request::input('filter_user_name') ?: old('filter_user_name') }}" placeholder="User name...">
					</div>
					<div class="form-group">
						<label for="filter_activity" class="control-label font-size-11">ACTIVITY</label>
						<input type="text" name="filter_activity" id="filter_activity" class="form-control" value="{{ Request::input('filter_activity') ?: old('filter_activity') }}" placeholder="Activity description...">
					</div>
				</div>
				<div class="form-group go-btn-group">
					<label for="" class="control-label">&nbsp;</label>
					<button type="submit" class="btn btn-azure">GO</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<h3 class="title-hero font-bold">Audit Logs</h3>
		@if($authUser->hasPermission('view_expense_syion_log'))
		<table class="table table-hover">
			<thead>
				<tr>
					<th width="20%">User</th>
					<th width="60%">Activity</th>
					<th class="text-right" width="20%">Timestamp</th>
				</tr>
			</thead>
			<tbody>
				@forelse($logs as $log)
				<tr>
					<td>{{ $log->user->name }}</td>
					<td>{{ $log->activity_desc }}</td>
					<td class="text-right">{{ $log->created_at->format('Y-m-d h:m A') }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="5">No logs found for this application!</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		<div class="text-center">
		@if($logs->count() > 0)
			@if(isset($filters))
			{!! $logs->appends($filters)->render() !!}
			@else
			{{ $logs->render() }}
			@endif
		@endif
		</div>
		@else
		<h5>Lack of permission to view expense logs.</h5>
		@endif
	</div>
</div>
@stop