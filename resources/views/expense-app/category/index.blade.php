@extends('templates.master')

@section('crumbs')
<li><a href="{{ route('expense-app') }}">Expense Dashboard</a></li>
<li class="active">Expense Categories</li>
@stop

@section('page-heading')
Expense Categories
@stop

@section('main')
@include('templates.inc.notification')
@include('templates.inc.form-errors')
<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero">
			List of Categories
		</h3>
		@if($authUser->hasPermission('access_expense_settings'))
		<div class="example-box-wrapper">
			<div class="size-md row">
				<div class="col-xs-1">
					<a href="javascript:;" class="btn btn-azure btn-md" data-toggle="modal" data-target="#creating-category-modal">CREATE</a>
				</div>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-bordered table-hover table-categories" id="datatable-example">
				<thead>
					<tr>
						<th width="85%">Name</th>
						<th width="10%" class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $category)
					<?php
					$hyp = '';
					if($category->child_level > 0){
						for($i = 1; $i <= $category->child_level; $i++){
							$hyp .= '-';
						}
					}
					$confirmText = '';
					if($category->hasSub){
						 $confirmText = "This category contains sub categories.";
					}

					if($category->expenses->count() > 0){
						$message = '<ul class="mrg5T text-left">';
		                foreach($category->expenses as $item){
		                    $url = route('expense.edit', $item->id);
		                    $message .= '<li><a href="'.$url.'">Entry: '.$item->title.'</a></li>';
		                };
		                $message .= '</ul>';
		                $confirmText = "There are {$category->expenses->count()} expense entry that use this category, delete these records first to continue delete this category;{$message}";
	                }
					?>
					<tr>
						<td>{{ $hyp.' '.$category->title }}</td>
						<td class="text-center">
							<a href="{{ route('expense.category.edit', $category->id) }}" class="edit-category-btn btn btn-success btn-xs" data-toggle="modal" data-target="#editing-category-modal">EDIT</a>
							<a href="{{ route('expense.category.delete', $category->id) }}" data-confirm-text="{{ ($confirmText != '') ? $confirmText : '' }}" class="{{ $category->expenses->count() > 0 ? 'deleteAlert' : 'confirmAlert' }} {{ $category->hasSub ? 'deleteAlert' : '' }} btn btn-danger btn-xs">DELETE</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@else
		<h5>Lack of permission to view list of expense categories.</h5>
		@endif
	</div>
</div>
<div class="modal fade" id="creating-category-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content form-validation-true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Creating Category</h4>
			</div>
			{!! Form::open(['route' => 'expense.category.store', 'method' => 'POST', 'id' => 'creating-expense-category-form']) !!}
			<div class="modal-body">
				<div class="form-group">
					<label for="category-title" class="control-label">Title</label>
					<input type="text" name="title" id="category-title" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="category-parent" class="control-label">Parent Category</label>
					<select name="parent_id" id="category-parent" class="form-control">
						<option value="">Select parent category...</option>
						@foreach($categoryList as $item)
						<?php
						$hyp = '';
						if($item->child_level > 0){
							for($i = 1; $i <= $item->child_level; $i++){
								$hyp .= '-';
							}
						}
						?>
						<option value="{{ $item->id }}">{{ $hyp.' '.$item->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="create-category-btn btn btn-primary">Save</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div class="modal fade" id="editing-category-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content form-validation-true" id="edit-category-form">
			
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('document').ready(function(){
		$('.edit-category-btn').on('click', function(e){
			e.preventDefault();
			var url = $(this).attr('href');

			$.ajax({
				url : url,
				success : function(data){
					$('#edit-category-form').html(data);
				}
			})
		});
	});
</script>
@stop