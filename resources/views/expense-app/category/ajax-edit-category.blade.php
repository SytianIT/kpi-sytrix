<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Edit Category</h4>
</div>
{!! Form::model($category, ['route' => ['expense.category.update', $category->id], 'method' => 'POST', 'id' => 'updating-expense-category-form']) !!}
<div class="modal-body">
	<div class="form-group">
		<label for="category-title" class="control-label">Title</label>
		<input type="text" name="title" id="category-title" class="form-control" value="{{ $category->title }}" required>
	</div>
	<div class="form-group">
		<label for="category-parent" class="control-label">Parent Category</label>
		<select name="parent_id" id="category-parent" class="form-control">
			<option value="">Select parent category...</option>
			@foreach($categoryList as $item)
			<?php
			$hyp = '';
			if($item->child_level > 0){
				for($i = 1; $i <= $item->child_level; $i++){
					$hyp .= '-';
				}
			}
			?>
			<option value="{{ $item->id }}" {{ $category->parent_id == $item->id ? 'selected' : '' }}>{{ $hyp.' '.$item->title }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="submit" data-category="{{ $category->id }}" class="update-category-btn btn btn-primary">Save</button>
</div>
{!! Form::close() !!}