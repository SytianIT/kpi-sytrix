var baseUrl = 'http://webserver/junjun/kpi/public/api';

angular.module('syionApp', ['ngSanitize'])
	.controller('ExpenseCategoryController', ExpenseCategoryController)
	.controller('PaymentController', PaymentController)
	.controller('CurrencyController', CurrencyController)
	.factory('ExpenseCategory', ExpenseCategory)
	.factory('Payment', Payment)
	.factory('Currency', Currency)
	.directive('ngConfirmClick', ConfirmClick)
	.directive('ngCannotClick', CannotClick)
  	.directive('ngConfirmSubmit', ConfirmSubmit)
  	.directive('ngMyEnter', MyEnter)
  	.directive('ngMyFocus', MyFocus)
  	// .directive('ExpenseCatList', ExpenseCatList)
	.config(['$interpolateProvider', function ($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');
	}]);

