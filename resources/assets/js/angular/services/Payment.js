/**
 * @ngInject
 */
function Payment($http) {

	var Payment = {
		'paymentList' : [],
		'busy': false,
		'baseUrl': '/kpi/public/expense-app/api'
	};

	Payment.paymentList = function() {
		this.busy = true;
		$http.get(this.baseUrl + '/get/payments').success(function(data) {
	        this.paymentList = data;
	        this.busy = false;
	    }.bind(this))
	    .error(function(data, status, headers, config) {
			this.busy = false
		}.bind(this));
	}

	Payment.createNewPayment = function(formData) {
		this.busy = true;
		return $http.post(this.baseUrl + '/store/payment', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Payment.updatePayment = function(formData, payment) {
		this.busy = true;
		return $http.post(this.baseUrl + '/update/'+ payment.id +'/payment', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Payment.deletePayment = function(payment) {
		this.busy = true;
		return $http.get(this.baseUrl + '/delete/'+payment.id+'/payment', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) {  this.busy = false; return data; }
		});
	}
	
	return Payment;
}