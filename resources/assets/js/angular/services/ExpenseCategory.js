/**
 * @ngInject
 */
function ExpenseCategory($http) {

	var ExpenseCategory = {
		'categoryList' : [],
		'busy': false,
		'baseUrl': '/kpi/public/expense-app/api'
	};

	ExpenseCategory.categoryList = function() {
		this.busy = true;
		$http.get(this.baseUrl + '/get/categories').success(function(data) {
	        this.categoryList = data;
	        this.busy = false;
	    }.bind(this))
	    .error(function(data, status, headers, config) {
			this.busy = false
		}.bind(this));
	}

	ExpenseCategory.createNewCategory = function(formData) {
		this.busy = true;
		return $http.post(this.baseUrl + '/store/category', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	ExpenseCategory.updateCategory = function(formData, category) {
		this.busy = true;
		return $http.post(this.baseUrl + '/update/'+ category.id +'/category', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	ExpenseCategory.deleteCategory = function(category) {
		this.busy = true;
		return $http.get(this.baseUrl + '/delete/'+category.id+'/category', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) {  this.busy = false; return data; }
		});
	}

	ExpenseCategory.getCategoryChilds = function(category) {
		this.busy = true;
		return $http.get(this.baseUrl + '/get/'+category.id+'/child', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	ExpenseCategory.createChildCategory = function(formData, category) {
		this.busy = true;
		return $http.post(this.baseUrl + '/create/'+category.id+'/child', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	return ExpenseCategory;
}