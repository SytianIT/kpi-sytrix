/**
 * @ngInject
 */
function Currency($http) {

	var Currency = {
		'currencyList' : [],
		'busy': false,
		'baseUrl': '/kpi/public/expense-app/api'
	};

	Currency.currencyList = function() {
		this.busy = true;
		$http.get(this.baseUrl + '/get/currencies').success(function(data) {
	        this.currencyList = data;
	        this.busy = false;
	    }.bind(this))
	    .error(function(data, status, headers, config) {
			this.busy = false
		}.bind(this));
	}

	Currency.createNewCurrency = function(formData) {
		this.busy = true;
		return $http.post(this.baseUrl + '/store/currency', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Currency.updateCurrency = function(formData, currency) {
		this.busy = true;
		return $http.post(this.baseUrl + '/update/'+ currency.id +'/currency', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Currency.deleteCurrency = function(currency) {
		this.busy = true;
		return $http.get(this.baseUrl + '/delete/'+currency.id+'/currency', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) {  this.busy = false; return data; }
		});
	}
	
	return Currency;
}