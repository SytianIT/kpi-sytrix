function ConfirmClick() {
	return {
		link: function (scope, element, attr) {
			var msg = attr.ngConfirmClick || "Are you sure?";
			var clickAction = attr.confirmedClick;
			var text = attr.ngAlertText;
			element.bind('click', function(event) {
				swal({   
					title: msg,   
					type: "warning",
					text: text,   
					showCancelButton: true,   
					confirmButtonText: "Yes, Continue!",   
					confirmButtonColor: "#DD6B55",
					closeOnConfirm: false 
				}, function(isConfirm) {   
					if(isConfirm) {
						scope.$eval(clickAction);
						swal.close();
					}
				});
			});
		}
	};
}