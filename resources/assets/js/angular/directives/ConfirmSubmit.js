function ConfirmSubmit() {
    return {
      link: function (scope, element, attr) {
          var msg = attr.ngConfirmSubmit || "Are you sure?";
          var clickAction = attr.confirmedSubmit;
          element.bind('submit', function (event) {
              if ( window.confirm(msg) ) {
                  scope.$eval(clickAction)
              }
          });
      }
    };
}
