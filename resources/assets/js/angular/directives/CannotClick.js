function CannotClick() {
	return {
		link: function (scope, element, attr) {
			var msg = attr.ngCannotClick || 'Unable to Delete.';
			var text = attr.ngAlertText;
			element.bind('click', function(event) {
		        swal({   
		            title: msg,   
		            text: text,
		            html: true
		        });
			});
		}
	};
}