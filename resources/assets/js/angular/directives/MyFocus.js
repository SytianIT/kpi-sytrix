function MyFocus($timeout, $parse) {
	return {
		link: function (scope, element, attr) {
			scope.$watch(attr.ngMyFocus, function(value) {
				if(attr.ngMyFocus){
					$timeout(function () {
                        element[0].focus();
                    });
				}
			});
		}
	};
}