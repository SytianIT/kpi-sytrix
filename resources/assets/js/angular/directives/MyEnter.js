function MyEnter($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {
			element.on("keydown keypress", function(event) {
				if (event.which === 13) {
					// This will pass event where the expression used $event
					scope.$apply(function(){
						scope.$eval(attr.ngMyEnter);
					});
					event.preventDefault();
				}
			});
		}
	};
}