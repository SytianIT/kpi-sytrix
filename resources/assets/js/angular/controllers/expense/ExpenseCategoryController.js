/**
 * @ngInject
 * Part of Angular Module
 */
 function ExpenseCategoryController($http, $scope, ExpenseCategory){
 	let expenseCategory = this;
 	expenseCategory.serviceExpenseCategory = ExpenseCategory;
 	expenseCategory.busy = false;
 	expenseCategory.results = [];

 	expenseCategory.serviceExpenseCategory.categoryList();

	// Creating articles
	expenseCategory.isCreating = false;

	expenseCategory.startCreating = function() {
		expenseCategory.isCreating = true;
		expenseCategory.editing = null;
	}

	expenseCategory.isNotValid = false;
	expenseCategory.createCategory = function() {
		if(expenseCategory.categoryTitle == undefined){
			expenseCategory.isNotValid = true;

			return;
		} else {
			expenseCategory.isNotValid = false;
		}

		var formData = new FormData();
		formData.append('title', expenseCategory.categoryTitle);
		if(expenseCategory.categoryParent != undefined){
			formData.append('parent_id', expenseCategory.categoryParent);
		}
		var promise = expenseCategory.serviceExpenseCategory.createNewCategory(formData);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					expenseCategory.serviceExpenseCategory.categoryList = payload.data;
				}
				expenseCategory.isCreating = false;
				expenseCategory.categoryTitle = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	expenseCategory.editing = null;
	expenseCategory.setEditing = function(category) {
		expenseCategory.categoryTitle = '';
		expenseCategory.editing = category;
		expenseCategory.isCreating = false;
		expenseCategory.isCreatingChild = false;
		expenseCategory.title = category.title;
	}

	expenseCategory.updateCategory = function() {
		var formData = new FormData();
		formData.append('title', expenseCategory.title);

		var promise = expenseCategory.serviceExpenseCategory.updateCategory(formData, expenseCategory.editing);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					expenseCategory.serviceExpenseCategory.categoryList = payload.data;
				}
				expenseCategory.editing = null;
				expenseCategory.title = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	expenseCategory.deleteCategory = function(category) {
		var promise = expenseCategory.serviceExpenseCategory.deleteCategory(category);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					expenseCategory.serviceExpenseCategory.categoryList = payload.data;
				}
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	expenseCategory.htmlCategoryList = null;
	expenseCategory.generateReturnHtml = function(category) {
		angular.forEach(category, function(value, key){
			// if(item.childs != null){
			// 	expenseCategory.generateReturnHtml(item);
			// }
			expenseCategory.htmlCategoryList = '<span class="font-size-11 font-bold">CHILD CATEGORIES</span>'+
						'<table class="table table-bordered table-child-category">'+
							'<thead><tr>'+
								'<th width="90%" class="font-gray-dark font-size-11 text-uppercase">Name</th>'+
								'<th width="10%" class="text-center font-gray-dark font-size-11 text-uppercase">Actions</th>'+
							'</tr></thead>'+
							'<tbody>'+
								'<tr ng-repeat="child in category.childs">'+
									'<td>'+
										'<span ng-if="child != expenseCategory.editing" class="font-size-11">'+
										value.title+
										'</span>'+
										'<input type="text" class="form-control" ng-show="child == expenseCategory.editing" ng-model="expenseCategory.title" ng-value="expenseCategory.editing.title" ng-my-enter="expenseCategory.updateCategory()" ng-my-focus="child == expenseCategory.editing" ng-blur="expenseCategory.updateCategory()">'+
									'</td>'+
									'<td class="text-center">'+
										'<a href="javascript:;" class="btn btn-xs btn-success" ng-click="expenseCategory.setEditing(child)">EDIT</a>'+
	    								'<a href="javascript:;" class="btn btn-xs btn-danger" confirmed-click="expenseCategory.deleteCategory(child)" ng-confirm-click="">DELETE</a>'+
									'</td>'+
								'</tr>'+
							'</tbody>'+
						'</table>';
		});
	}
}