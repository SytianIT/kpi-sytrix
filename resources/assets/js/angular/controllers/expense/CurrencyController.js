/**
 * @ngInject
 * Part of Angular Module
 */
 function CurrencyController($http, $scope, Currency){
 	this.serviceCurrency = Currency;
 	this.busy = false;
 	this.results = [];

 	this.serviceCurrency.currencyList();

 	// Creating articles
	this.isCreating = false;
	this.startCreating = function() {
		this.isCreating = true;
		this.currencyPrefix = '';
		this.currencyTitle = '';
		this.currencyRate = 0;
		this.editing = null;
	}

	this.createCurrency = function() {

		this.isNotValid = false;
		this.noDuplicate = false;
		angular.forEach(this.serviceCurrency.currencyList, function(value, key){
			if(value.prefix.toUpperCase() == this.currencyPrefix.toUpperCase() || value.title.toUpperCase() == this.currencyTitle.toUpperCase()){
				this.noDuplicate = true;
			} else if(this.noDuplicate != true){
				this.noDuplicate = false;
			}
		}, this);

		if(this.noDuplicate == true){
			return;
		}

		if(this.currencyTitle == '' || this.currencyPrefix == '' || this.currencyRate == 0){
			this.isNotValid = true;

			return;
		} else {
			this.isNotValid = false;
		}

		var formData = new FormData();
		formData.append('title', this.currencyTitle);
		formData.append('prefix', this.currencyPrefix);
		formData.append('conversion_rate', this.currencyRate);

		var promise = this.serviceCurrency.createNewCurrency(formData);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.serviceCurrency.currencyList = payload.data;
				}
				this.isCreating = false;
				this.currencyTitle = undefined;
				this.currencyPrefix = undefined;
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
	
	this.editing = null;
	this.setEditing = function(currency) {
		this.currencyTitle = '';
		this.currencyPrefix = '';
		this.currencyRate = 0;
		this.editing = currency;
		this.isCreating = false;
		this.title = currency.title;
		this.prefix = currency.prefix;
		this.conversion_rate = currency.conversion_rate;
	}

	this.updateCurrency = function() {
		var formData = new FormData();
		formData.append('title', this.title);
		formData.append('prefix', this.prefix);
		formData.append('conversion_rate', this.conversion_rate);

		var promise = this.serviceCurrency.updateCurrency(formData, this.editing);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.serviceCurrency.currencyList = payload.data;
				}
				this.editing = null;
				this.title = '';
				this.prefix = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	this.deleteCurrency = function(currency) {
		var promise = this.serviceCurrency.deleteCurrency(currency);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.serviceCurrency.currencyList = payload.data;
				}
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
 }