/**
 * @ngInject
 * Part of Angular Module
 */
 function PaymentController($http, $scope, Payment){
 	this.servicePayment = Payment;
 	this.busy = false;
 	this.results = [];

 	this.servicePayment.paymentList();

 	// Creating articles
	this.isCreating = false;
	this.startCreating = function() {
		this.isCreating = true;
		this.editing = null;
	}

	this.createPayment = function() {

		this.isNotValid = false;
		this.noDuplicate = false;
		angular.forEach(this.servicePayment.paymentList, function(value, key){
			if(value.title.toUpperCase() == this.paymentTitle.toUpperCase()){
				this.noDuplicate = true;

				return;
			} else if(this.noDuplicate != true){
				this.noDuplicate = false;
			}
		}, this);

		if(this.noDuplicate == true){
			return;
		}

		if(this.paymentTitle == undefined){
			this.isNotValid = true;

			return;
		} else {
			this.isNotValid = false;
		}

		var formData = new FormData();
		formData.append('title', this.paymentTitle);
		var promise = this.servicePayment.createNewPayment(formData);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.servicePayment.paymentList = payload.data;
				}
				this.isCreating = false;
				this.paymentTitle = undefined;
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
	
	this.editing = null;
	this.setEditing = function(payment) {
		this.paymentTitle = '';
		this.editing = payment;
		this.isCreating = false;
		this.isCreatingChild = false;
		this.title = payment.title;
	}

	this.updatePayment = function() {
		var formData = new FormData();
		formData.append('title', this.title);

		var promise = this.servicePayment.updatePayment(formData, this.editing);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.servicePayment.paymentList = payload.data;
				}
				this.editing = null;
				this.title = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	this.deletePayment = function(category) {
		var promise = this.servicePayment.deletePayment(category);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.servicePayment.paymentList = payload.data;
				}
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
 }