$(document).ready(function(){

    var $baseUrl = $('meta[name="base-url"]').attr('content');

    /**
     * Script to Initialize Quick preview of image
     * use .quick-preview class with data-img attribute of path of the image
     */
    $('body').on('click', '.btn-preview-modal-image', function(e){
        var src = $(this).data('path');

        $('#modal-preview-image .modal-preview-img').attr('src', src);
    });

    /**
     * Script to initialize the Chosen SELECT
     */
    $(".chosen-select").chosen();
    $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
    
    /**
     * Script to Initialize Alert for Deletion
     */
    $(".confirmAlert").click(function(e) {
        e.preventDefault();

        var msg = $(this).data('confirm-message') || 'Are you sure?';
        var msgText = ($(this).data('confirm-text') != '') ? $(this).data('confirm-text') : '';
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#FF5722",
            closeOnConfirm: false 
        }, function(isConfirm) {   
            if(isConfirm) {
                window.location = _self.attr("href")
            }
        });
    });

     $(".confirmAlert-self").click(function(e) {
        e.preventDefault();

        var msg = $(this).data('confirm-message') || 'Are you sure?';
        var msgText = ($(this).data('confirm-text') != '') ? $(this).data('confirm-text') : '';
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#FF5722",
            closeOnConfirm: false 
        }, function(isConfirm) {   
            if(isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: _self.attr('href'),
                    success: function(){
                      window.location.reload(); 
                    }
                });

            }
        });
    });

    // Confirm alert ajax
    $(document).on('click', '.confirmAlert-ajax', function(e) {
        e.preventDefault();

        var $this = $(this);
        var msg = $(this).data('confirm-message') || 'Are you sure?';
        var msgText = ($(this).data('confirm-text') != '') ? $(this).data('confirm-text') : '';
        var _self = $(this);

        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#FF5722",
            closeOnConfirm: true 
        }, function(isConfirm) {   
            if(isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: _self.attr('href'),
                    success: function(){
                            
                        $this.closest('.card-item-wrapper').remove();

                        swal.closeModal();

                    }
                });

            }
        });
    });

    /**
     * Script to Initialize Alert to Prevent Deletion
     */
    $(".deleteAlert").click(function(e) {
        e.preventDefault();

        var msg = $(this).data('confirm-message') || 'Delete Unable!';
        var msgText = ($(this).data('confirm-text') != '') ? $(this).data('confirm-text') : '';
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,
            html: true,
            type: "warning"
        });
    });

    /**
    * Script to initialize color picker script
    **/
    if( $('.color-pick').length ) {
    
        var $defaultValue = '';

        $('.color-pick').each(function() {

            if( $(this).data('value') != null ) {
                $defaultValue = $(this).data('value');
            }

            $(this).minicolors({
                animationSpeed: 100,
                change: null,
                changeDelay: 0,
                control: 'hue',
                defaultValue: $defaultValue,
                hide: null,
                hideSpeed: 100,
                inline: false,
                letterCase: 'lowercase',
                opacity: false,
                position: 'bottom right',
                show: null,
                showSpeed: 100,
                textfield: true,
                theme: 'default',
                change: function($value) {
                    if( $(this).hasClass('change-color') ) {
                        $.ajax({
                            type: 'POST',
                            url: $baseUrl + '/sytrix-app/status/change-color',
                            data: { 'color' : $value, 'id' : $(this).data('id') },
                        });
                    }
                    else {
                        return false;
                    }
                }
            });

        });

    }

    /**
    * Popup Editable
    **/
    $('.popup-editable').popover({
        template: '<div class="popover" role="tooltip"><button class="popover-close glyph-icon icon-times pull-right"></button><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        html: true,
        content: function() {
            if( $(this).data('contenttarget') != 0 ) {
                var $content = $(this).data('contenttarget');
                return $($content).html();
            }
        }
    });
    $(document).on('click', '.popover-close', function(){
        $(this).closest('.popover').popover('hide');
    });

    /**
    * Data Table
    **/
    if( $('.dataTable, .data-with-paginate').length ) {

        $('.dataTable').dataTable({
            searching: false,
            "paging": false,
            "columnDefs": [
                { "orderable": false, "targets": 'nosort' }
            ],
            order: []
        });

        $('.data-with-paginate').dataTable({
            searching: false,
            "paging": true,
            "columnDefs": [
                { "orderable": false, "targets": 'nosort' }
            ],
            order: []
        });

    }

    /**
     * Scripts to initialize Table Script
     */
    $('.table-data-init').dataTable({
        "lengthMenu": [[ 10, 25, 50, -1 ], [10, 25, 50, "All"]],
    });


    $('.tabs').tabs();

    /**
     * Scripts to initialize Daterange Picker
     */
    $( function() {
        var dateFormat = "mm/dd/yy",
            defaultDate = new Date(),
            from = $( "#startDate" ).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
            }),
            tempTo = $('#dueDate').datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            }),
            to = $('#store_dueDate').datepicker();
               
        // if( $('.project-time').length ) {
        //     $('.project-time').on('change keyup', function(event){
        //         var $maxTime = 0,
        //             $fromDate;
        //         $('.project-time').each(function(){
        //             var $timeVal = ( $(this).val() != "" ) ? $(this).val() : 0;     
        //             $maxTime  = $maxTime + parseInt( $timeVal );
        //         });  

        //         if( $('#startDate').val() != "") {
        //             $fromDate = new Date( $('#startDate').datepicker('getDate') );
        //         }
        //         else {
        //             $fromDate = new Date( $('#startDate').datepicker('setDate', $.now()) );
        //         }
        //         $fromDate.setDate($fromDate.getDate() + parseInt( $maxTime / 8 ));
        //         $('#store_dueDate').datepicker('setDate', $fromDate);

        //         $('#dueDate').val( $('#store_dueDate').val() )

        //     });
        // }

        $('.daterange-picker').daterangepicker({
            separator: '-',
            format: 'YYYY/MM/DD',
        });

        $('.date-picker').datepicker();
    });

    /**
     * Script to initilize error messages at top of screen
     * @type {[type]}
     */
    var notifBar = $('#notification-show');
    if(notifBar.length > 0){
        var notes = [];

        // notes[''] = '<i class="glyph-icon icon-cog mrg5R"></i>This is a default notification message.';
        // notes['alert'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an alert notification message.';
        // notes['error'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an error notification message.';
        // notes['success'] = '<i class="glyph-icon icon-cog mrg5R"></i>You successfully read this important message.';
        // notes['information'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an information notification message!';
        // notes['notification'] = '<i class="glyph-icon icon-cog mrg5R"></i>This alert needs your attention, but it\'s for demonstration purposes only.';
        // notes['warning'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is a warning for demonstration purposes.';
        // 
        var self = notifBar;
        noty({
            text: self.data('message'),
            type: self.data('type'),
            dismissQueue: true,
            theme: 'agileUI',
            layout: self.data('layout')
        });
    }

    // Click of Enter the click the button to
    $('body').on('click', '.input-keypress', function(){
        let target = $(this).data('click-target');
        $(this).keypress(function(e) {
            if(e.which == 13) {
                $(target).click();
            }
        });
    });

    // Change Application Click Event
    $('#change-application').on('change', function(){
        var url = $(this).find('option:selected').val();
        window.location.href = url;
    });

    $(document).on('click', function(){

        // Context Menu
        if( !$(this).hasClass('contextmenu') && $('.contextmenu').is(':visible') ) {
            $('.contextmenu').remove();
        }

    });

    /**
    * Projects
    **/
    $('[name="project_status_id"]').change(function(){

        if( $(this).find(':selected').data('status') == 'completed' ) {
            $('.form-project-status').addClass('completed');
        }
        else {
            $('.form-project-status').removeClass('completed');
        }

    });

    /**
    * Tasks
    **/
    var $oldTaskLabel;
    $(document).on('click', '.add-task', function(){
        $(this).parents('.card-column')
            .find('.card-container')
                .append('<div class="card-item-wrapper temp"><div class="card-item"><input type="text" class="card-item-name"></div></div>')
            .find('.card-item-name')
                .focus();
    });

    $(document).on('blur', '.card-item-name', function(){

        if( $(this).closest('.card-item-wrapper').hasClass('temp') ) {
            $(this).closest('.card-item-wrapper').remove();
        }
        else {
            $(this)
                .closest('.card-item')
                    .find('.card-item-title')
                        .text( $oldTaskLabel )
                    .end()
                .end()
            .remove()
        }

    });

    $(document).on('keypress', '.card-item-name', function(event){
        if( event.keyCode == 13 ) {
            var $currentCard = $(this),
                $taskCardtitle = $currentCard.val(),
                $taskID = $currentCard.closest('.card-item-wrapper').data('taskid'),
                $cardID = $currentCard.parents('.card-column').attr('data-cardid');
                
            if( $taskCardtitle != "" ) {

                if( $currentCard.parents('.card-item-wrapper').hasClass('temp') ) {
                    $.ajax({
                        type: 'POST',
                        url: $baseUrl + '/sytrix-app/tasks/create',
                        data: { 'task_title': $taskCardtitle, 'card_id': $cardID },
                        success: function( $taskCard ) {   
                            $currentCard.parents('.card-column').find('.card-container').append('<div class="card-item-wrapper" data-taskid="' + $taskCard.id + '"><a href="'+$baseUrl+'/sytrix-app/tasks/'+$taskCard.id+'/delete" class="glyph-icon icon-remove task-remove confirmAlert-ajax cursor-pointer" title="remove"></a><div class="card-item cursor-pointer item-hover"><h5 class="card-item-title">' + $taskCard.title + '</h5></div></div>');
                            $currentCard.closest('.card-item-wrapper.temp').remove();
                        }
                    });
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: $baseUrl + '/sytrix-app/tasks/' + $taskID + '/update',
                        data: { 'task_title': $taskCardtitle },
                        success: function( $taskCard ) {   
                            $currentCard
                                .closest('.card-item')
                                    .find('.card-item-title')
                                        .text( $taskCard.title )
                                    .end()
                                .end()
                            .remove();
                        }
                    });
                }

            }
            else {
                if( $currentCard.closest('.card-item-wrapper').hasClass('temp') ) {
                    $currentCard.closest('.card-item-wrapper').remove();
                }
                else {
                    $currentCard
                        .closest('.card-item')
                            .find('.card-item-title')
                                .text( $oldTaskLabel )
                            .end()
                        .end()
                    .remove()
                }
            }
        }
     });

    $(document).on('click', '.card-item', function(){

        $oldTaskLabel = $(this).find('.card-item-title').text();

        if( $(this).find('.card-item-name').length == 0 ) {
            $(this).append('<input type="text" class="card-item-name">');
        }

        $(this)
            .find('.card-item-title')
                .text("")
            .end()
            .find('.card-item-name')
                .focus();

    });


    /**
    * Cards
    **/

    $.fn.equalize = function( options ) {

        if ( $(this).length ) {

            var $settings = $.extend({
                windowStart: 768,
                coverage: 'min-width'
            }, options);

            if( Modernizr.mq('(' + $settings.coverage + ':' + $settings.windowStart + 'px)') ) {
                var $maxHeight = 0;
                $(this).each(function(){
                   if ($(this).height() > $maxHeight) {
                        $maxHeight = $(this).height(); 
                    }
                });
        
                $(this).css({ 
                    height: $maxHeight
                }); 
            }
            else {
                $(this).css({
                    height: 'auto'
                });
            }

        }   

    };

    initTaksWidth();

    $('.add-card').on('click', function(){
        var $card = '<li class="card-column"> ' +
                '<div class="panel">' +
                    '<div class="panel-body">' +                             
                        '<h3 class="title-hero">' +
                            '<a href="" class="confirmAlert-self" title="Archive Card" data-confirm-message="Archive Card?"><span class="glyph-icon tooltip-button icon-archive archive-card cursor-pointer pull-right"></span></a>' +
                            '<span class="glyph-icon tooltip-button icon-users pull-right cursor-pointer popup-editable" data-contenttarget=".selected-members-content" data-placement="bottom" data-type="html" data-title="Assign Member(s)" title="Assign Members" data-original-title="Assign Member(s)"></span>' +
                            '<span class="glyph-icon tooltip-button icon-edit pull-right edit-card cursor-pointer" title="Edit Card Label"></span>' +
                            '<strong class="card-label"></strong>' +
                            '<input type="text" class="card-title">' +
                        '</h3>' +       
                        '<div class="card-container">' +
                        '</div>' +
                    '</div>' +
                    '<div class="panel-footer">' +
                        '<button class="btn btn-xs btn-success add-task">Add Task</button>'  +
                        '<div class="task-members pull-right"></div>' +
                        '<div class="clearfix"></div>'
                    '</div>' +
                '</div>' +
            '</li>';

        $('.card-columns')
            .prepend( $card )
            .end()
            .find('.card-column:first-child')
                .find('.card-title')
                    .focus()
                .end()
                .find('.popup-editable').popover({
                    template: '<div class="popover" role="tooltip"><button class="popover-close glyph-icon icon-times pull-right"></button><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
                    html: true,
                    content: function() {
                        if( $(this).data('contenttarget') != 0 ) {
                            var $content = $(this).data('contenttarget');
                            return $($content).html();
                        }
                    }
                });       

        initTaksWidth(); 
    });

    var $oldLabel;
    $(document).on('click', '.edit-card', function(){

        $oldLabel = $(this).closest('.title-hero').find('.card-label').text();

        $(this)
            .closest('.title-hero')
                .append('<input type="text" class="card-title">')
            .find('.card-title')
                .focus()
            .end()
            .closest('.title-hero')
                .find('.card-label')
                    .text("");
    });

    $(document).on('blur', '.card-title', function(){
        $(this)
            .closest('.title-hero')
                .find('.card-label')
                    .text( $oldLabel )
                 .end()
            .end()
            .remove();
    });

    $(document).on('keypress', '.card-title', function(event){
        if( event.keyCode == 13 ) {
            var $currentCard = $(this),
                $newLabel = $currentCard.val(),
                $cardID = $currentCard.parents('.card-column').data('cardid'),
                $projectID = $('.card-columns').data('projectid');

             if( $(this).val() != "" ) {
                $.ajax({
                    type: 'POST',
                    url: $baseUrl + '/sytrix-app/cards/' + $projectID + '/update',
                    data: { 'card_title': $newLabel, 'card_id': $cardID },
                    success: function( $response ) {
                        $currentCard
                            .parents('.card-column')
                                .attr('data-cardid', $response.id)
                                .attr('id', $response.id)
                            .end()
                            .closest('.title-hero')
                                .find('.card-label')
                                    .text( $newLabel )
                                .end()
                            .end()
                            .remove();
                    }
                });
            }
            else {
                $currentCard
                    .closest('.title-hero')
                        .find('.card-label')
                            .text( $oldLabel )
                        .end()
                    .end()
                    .remove();
            }

        }
    });

    // Members 
    $(document).on('click', '.member', function(){
        var $memberID = $(this).data('userid'),
            $projectID = $(this).data('projectid');       

        if( $(this).closest('.user-item').hasClass('active') ) {
            $toggleAction = 'remove-user';
        }
        else {
            $toggleAction = 'assign-user';
        }

        $(this).closest('.user-item').toggleClass('active');

        $.ajax({
            type: 'POST',
            url: $baseUrl + '/sytrix-app/project/' + $projectID + '/' + $toggleAction,
            data: { 'member_id': $memberID },
            success: function( $response ) {

                $('.selected-users').html("");
                $('.selected-members').html("");

                $response.forEach(function( $response ){

                    if( $response.user_picture == null ) {
                        $userPicture = $baseUrl + '/images/profile-default.jpg';
                    }
                    else {
                        $userPicture = $baseUrl + '/' + $response.user_picture;
                    }

                    $('.selected-users').append( '<li class="user-item text-center"><img src="' + $userPicture + '" class="img-responsive cursor-pointer"><small class="text-center">' + $response.firstname + '</small></li>' );
                    $('.selected-members').append( '<li class="user-item text-center"><img src="' + $userPicture + '" class="img-responsive cursor-pointer task-member" data-userid="' + $response.id + '"><small class="text-center">' + $response.firstname + '</small></li>' );
                });

            }
        });

    });

    $(document).on('click', '.task-member', function(){
        var $cardID = $(this).closest('.card-column').data('cardid'),
            $memberID = $(this).data('userid'),            
            $taskMembersContainer = $(this).closest('.card-column').find('.task-members');

            $.ajax({
                type: 'POST',
                url: $baseUrl + '/sytrix-app/card/' + $cardID + '/assign-members',
                data: { 'member_id': $memberID },
                success: function( $response ) {

                    if( $taskMembersContainer.find('.task-member-list').length == 0 ) {
                        $taskMembersContainer.append('<ul class="user-list task-member-list list-inline"></ul>');
                        $taskMembersContainer.find('.task-member-list').html("");
                    }
                    else {                        
                        $taskMembersContainer.find('.task-member-list').html("");
                    }

                    $response.forEach(function( $taskMember ){

                        if( $taskMember.user_picture != null ) {
                            $userPicture = $baseUrl + '/' + $taskMember.user_picture;
                        }
                        else {
                            $userPicture = $baseUrl + '/images/profile-default.jpg';
                        }

                        $taskMembersContainer
                            .find('.task-member-list')
                                .append('<li class="task-member-item user-item"><img src="' + $userPicture + '" title="'+ $taskMember.firstname +'" data-actionid="' + $taskMember.id + '" class="img-responsive member-assigned has-contextmenu"></li>');
                    });

                    // Wrap with context menu
                    wrap();

                }
            });

            $(this).closest('.popover').remove();
    });    
    
    $(document).on('click', '.remove_task_member', function(){
        var $memberID = $(this).closest('.contextmenu').siblings('.has-contextmenu').data('actionid'),
            $cardID = $(this).closest('.card-column').data('cardid'),
            $options = $(this).closest('.contextmenu').siblings('.has-contextmenu').data('options'),
            $taskMembersContainer = $(this).closest('.card-column').find('.task-members');

         $.ajax({
            type: 'POST',
            url: $baseUrl + '/sytrix-app/card/' + $cardID + '/remove-members',
            data: { 'member_id': $memberID },
            success: function( $response ) {

                if( $taskMembersContainer.find('.task-member-list').length == 0 ) {
                    $taskMembersContainer.append('<ul class="user-list task-member-list list-inline"></ul>');
                    $taskMembersContainer.find('.task-member-list').html("");
                }
                else {                        
                    $taskMembersContainer.find('.task-member-list').html("");
                }

                $response.forEach(function( $taskMember ){
                    $taskMembersContainer
                        .find('.task-member-list')
                            .append('<li class="task-member-item user-item"><img src="' + $baseUrl + '/' + $taskMember.user_picture + '" data-actionid="' + $taskMember.id + '" class="img-responsive member-assigned has-contextmenu"></li>');
                });

                wrap();

            }
        });
    });  

    $(document).on('keyup', '.search-member', function(){

        var $memberToSearch = $(this).val(),
            $projectID = $(this).data('projectid');

        $.ajax({
            type: 'POST',
            url: $baseUrl + '/sytrix-app/project/' + $projectID + '/search-users',
            data: { 'search_member': $memberToSearch },
            success: function( $response ) {

                if( $('.search-members').find('.user-list').length == 0 ) {
                    $('.search-members').html('<ul class="user-list list-inline"></ul>');
                }

                $('.search-members .user-list .user-item').remove();

                // Check for selected users
                $selectedUsers = [];
                if( $response.selectedUsers.length != 0 ) {
                    $response.selectedUsers.forEach(function($selectedUser){
                        $selectedUsers.push($selectedUser.id);
                    });
                }

                if( $response.users.length != 0 ) {
                    $response.users.forEach(function($user){
                        
                        if( $selectedUsers.length != 0 ) {
                            if( $.inArray($user.id, $selectedUsers) != -1 ) {
                                $selected = ' active';
                            }
                            else {
                                $selected = "";
                            }
                        }
                        else {
                            $selected = "";
                        }

                        if( $user.user_picture != null ) {
                            $userPicture = $baseUrl + '/' + $user.user_picture;
                        }
                        else {
                            $userPicture = $baseUrl + '/images/profile-default.jpg';
                        }

                        $('.search-members .user-list').append('<li class="user-item text-center' + $selected +'"><img src="' + $userPicture + '" data-userid="' + $user.id + '" data-projectid="' + $projectID + '"  class="img-responsive cursor-pointer member"><small>' + $user.firstname + '</small></li>');
                    });
                }
                else {
                    $('.search-members').html('Member not found..');
                }

            }
        });

    });

    // Timetrack entry
    // Selecting client and member
    $('.tt-client, .tt-member').on('change', function(){

        var $userID = $('.tt-member').val(),
            $clientID = $('.tt-client').val();

        $.ajax({
            type: 'POST',
            url: $baseUrl + '/sytrix-app/time-tracks/get-projects',
            data: { 'userid': $userID, 'clientid': $clientID },
            success: function( response ) {

                $('.tt-project').html("");

                //Display projects if exists
                if( response.length > 0 ) {
                    $('.tt-project').append('<option selected disabled>Select project</option>');
                    response.forEach(function($project){
                        $('.tt-project').append('<option value="'+$project.id+'">' + $project.name + '</option>');
                    });
                }
                else {
                    $('.tt-project').append('<option selected disabled>Projects not found</option>');
                }

            }
        });
    });

    // Selecting project
    $('.tt-project').on('change', function(){

        var $userID = $('.tt-member').val(),
            $projectID = $('.tt-project').val();

         $.ajax({
            type: 'POST',
            url: $baseUrl + '/sytrix-app/time-tracks/get-tasks',
            data: { 'userid': $userID, 'projectid': $projectID },
            success: function( response ) {

                $('.tt-task').html("");

                //Display cards if exists
                if( response.length > 0 ) {
                    $('.tt-task').append('<option selected disabled>Select task</option>');
                    response.forEach(function($card){
                        $('.tt-task').append('<option data-type="card" value="'+$card.id+'" class="font-bold">' + $card.title + '</option>');

                        //Check tasks if exists
                        if( $card.tasks.length > 0 ) {
                            $card.tasks.forEach(function($task){
                                $('.tt-task').append('<option data-type="task" value="'+$task.id+'">' + $task.title + '</option>');
                            });
                        }

                    });
                }
                else {
                    $('.tt-task').append('<option selected disabled>Tasks not found</option>');
                }

            }
        });       

    });

    // Selecting task
    $('.tt-task').on('change', function(){
        $('.timeables .time-id').val( $(this).val() );
        $('.timeables .time-type').val( $(this).find(':selected').data('type') );
    });

    $('.date-rendered').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    // ----------------  //

    // Sorting Cards
    $('.card-columns').sortable({
        revert: true,
        update: function(event, ui) {
            var $sorted = $('.card-columns').sortable("toArray"),  
                $projectID = $('.card-columns').data('projectid');

            $.ajax({
                type: 'POST',
                url: $baseUrl + '/sytrix-app/cards/' + $projectID + '/update-sort',
                data: { 'sort_val': $sorted }
            });

        }
    });

    // Sorting Tasks
    $('.tasks-sort').sortable({
        update: function(event, ui) {
            var $sorted = $( ".tasks-sort" ).sortable("toArray");

            $.ajax({
                type: 'POST',
                url: $baseUrl + '/sytrix-app/tasks/categories/update-sort',
                data: { 'sort_val': $sorted },
            });

        }
    });

    $('.status-sort').sortable({
        update: function(event, ui) {
            var $sorted = $( ".status-sort" ).sortable("toArray");

            $.ajax({
                type: 'POST',
                url: $baseUrl + '/sytrix-app/status/update-sort',
                data: { 'sort_val': $sorted },
            });

        }
    });   


    // ----------------  //
    // notifications
    $(document).on('click', '.clear-notif', function(){

        var $id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: $baseUrl + '/sytrix-app/notifications/' + $id + '/notified' ,
            success: function($notif){
                $('.notif-count').html( $notif.count );
                $('.notifications-box').html("");

                $notif.notifications.forEach(function($notification){

                    switch( $notification.contextable_value ) {
                        case 'project' :
                            var $icon = 'icon-tasks';
                            break;
                        case 'timetrack' :
                            var $icon = 'icon-clock-o';
                            break;
                        default :
                    }

                    var $theNotif = '<li>' +
                                        '<span class="glyph-icon icon-notification ' + $icon + ' bg-' + $notification.status + '"></span>' +
                                        '<span class="notification-text">' + $notification.description + '</span>' +
                                        '<span title="clear" data-id="'+ $notification.id +'" class="clear-notif glyph-icon icon-close pull-right cursor-pointer"></span>' +
                                        '<div class="clearfix"></div>' + 
                                        '<div class="notification-time">' + $notification.elapsed_time + 
                                            '<span class="glyph-icon icon-clock-o"></span>' +
                                        '</div>' + 
                                    '</li>';

                    $('.notifications-box').append( $theNotif );
                });

            }
        });

    });


    // Contextmenu
    $( function() {                

        wrap();

        $(document).on('click', '.has-contextmenu', function(){            

            $('.contextmenu').remove();

            var $this = $(this),
                $contextWrapper = $this.closest('.context-wrapper'),
                $contextmenu = $this.siblings('.contextmenu'),
                $options = {
                    "remove_task_member": "Remove Member", 
                    "view_profile": "View Profile"
                },
                $document = $(window);

            if( $contextmenu.length == 0 ) {
                
                $contextWrapper.append('<div class="contextmenu"></div>');

                var $contextmenu = $this.siblings('.contextmenu');
                    $contextOffset = $contextmenu.offset();

                if( $options.length != 0 ) {
                    $.each($options, function($key, $value){
                        $contextmenu.append('<div class="context-item ' + $key + '">' + $value + '</div>');
                    });
                }

                var $contextmenuHeight = $contextmenu.height();

                //Check for context menu position
                var $display;                
                if( $contextOffset.top < $document.outerHeight ) {
                    $display = {
                        top: 'auto',
                        left: 'auto',
                        bottom: '-10px',
                        right: 'auto',
                    };
                }
                else if( $contextOffset.right > $document.outerWidth() ) {
                    $display = {
                        top: '50%',
                        left: '-200px',
                        bottom: 'auto',
                        right: 'auto',
                        marginTop: -$contextmenuHeight / 2
                    };
                }
                else if( $contextOffset.bottom > $document.outerHeight ) {
                    $display = {
                        top: '-10px',
                        left: 'auto',
                        bottom: 'auto',
                        right: 'auto',
                    };
                }
                else {
                    $display = {
                        top: '50%',
                        left: 'auto',
                        bottom: 'auto',
                        right: '-200px',
                        marginTop: -$contextmenuHeight / 2
                    };
                }                

                $contextmenu.css({
                    top: $display.top,
                    left: $display.left,
                    right: $display.right,
                    bottom: $display.bottom,
                    marginTop: $display.marginTop
                });
            }

            $contextmenu.focus();

            return false;
        });

        $(document).on('click', '.context-item', function(){
            $(this).closest('.contextmenu').remove();
        });

    });
    function wrap() {
        $('.has-contextmenu').each(function(){
            var $contextWrapper = $(this).closest('.context-wrapper');
            if( $contextWrapper.length == 0 ) {
                $(this).wrap('<div class="context-wrapper"></div>');
            }            
        });
    }

	/* Multiselect inputs */
    $(function() {"use strict";
        $(".spinner-input").spinner();

        $('.input-switch').bootstrapSwitch();

        $(".multi-select").multiSelect();

        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    });

    /*
    * Time pickers
    */
    $('.clock-picker').clockpicker({
        donetext: 'Set Time',
        twelvehour: true,
        afterDone: function(){
            var $timeVal = $(this).val();
            console.log( $timeVal );
        }
    });

    /**
    * Select2
    **/
    $('.select2').select2();

    $.fn.numericInput = function() {
        return this.each(function() {
            $(this).keydown(function(e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 || 
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };

    $('.numericInput').numericInput();

    function isNumberKey(event){
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }   
    
    // Initialize tasks width
    function initTaksWidth() {
        $('.card-columns').each(function(){
            var $cardCount = $(this).find('.card-column').length;

            $(this).css({
                width: 400 * $cardCount
            })
        });
    } 

    // Nested sortable
    if( $('.sortable').length ) {
        $('.sortable').each(function(){
            $('.sortable').nestedSortable({
                items: 'li',
                protectRoot: true
            });
        });
    }

	$(window).load(function(){
        setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
    });	

});