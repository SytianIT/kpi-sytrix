var baseUrl = 'http://webserver/junjun/kpi/public/api';

angular.module('syionApp', ['ngSanitize'])
	.controller('ExpenseCategoryController', ExpenseCategoryController)
	.controller('PaymentController', PaymentController)
	.controller('CurrencyController', CurrencyController)
	.factory('ExpenseCategory', ExpenseCategory)
	.factory('Payment', Payment)
	.factory('Currency', Currency)
	.directive('ngConfirmClick', ConfirmClick)
	.directive('ngCannotClick', CannotClick)
  	.directive('ngConfirmSubmit', ConfirmSubmit)
  	.directive('ngMyEnter', MyEnter)
  	.directive('ngMyFocus', MyFocus)
  	// .directive('ExpenseCatList', ExpenseCatList)
	.config(['$interpolateProvider', function ($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');
	}]);


/**
 * @ngInject
 * Part of Angular Module
 */
 function ExpenseCategoryController($http, $scope, ExpenseCategory){
 	let expenseCategory = this;
 	expenseCategory.serviceExpenseCategory = ExpenseCategory;
 	expenseCategory.busy = false;
 	expenseCategory.results = [];

 	expenseCategory.serviceExpenseCategory.categoryList();

	// Creating articles
	expenseCategory.isCreating = false;

	expenseCategory.startCreating = function() {
		expenseCategory.isCreating = true;
		expenseCategory.editing = null;
	}

	expenseCategory.isNotValid = false;
	expenseCategory.createCategory = function() {
		if(expenseCategory.categoryTitle == undefined){
			expenseCategory.isNotValid = true;

			return;
		} else {
			expenseCategory.isNotValid = false;
		}

		var formData = new FormData();
		formData.append('title', expenseCategory.categoryTitle);
		if(expenseCategory.categoryParent != undefined){
			formData.append('parent_id', expenseCategory.categoryParent);
		}
		var promise = expenseCategory.serviceExpenseCategory.createNewCategory(formData);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					expenseCategory.serviceExpenseCategory.categoryList = payload.data;
				}
				expenseCategory.isCreating = false;
				expenseCategory.categoryTitle = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	expenseCategory.editing = null;
	expenseCategory.setEditing = function(category) {
		expenseCategory.categoryTitle = '';
		expenseCategory.editing = category;
		expenseCategory.isCreating = false;
		expenseCategory.isCreatingChild = false;
		expenseCategory.title = category.title;
	}

	expenseCategory.updateCategory = function() {
		var formData = new FormData();
		formData.append('title', expenseCategory.title);

		var promise = expenseCategory.serviceExpenseCategory.updateCategory(formData, expenseCategory.editing);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					expenseCategory.serviceExpenseCategory.categoryList = payload.data;
				}
				expenseCategory.editing = null;
				expenseCategory.title = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	expenseCategory.deleteCategory = function(category) {
		var promise = expenseCategory.serviceExpenseCategory.deleteCategory(category);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					expenseCategory.serviceExpenseCategory.categoryList = payload.data;
				}
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	expenseCategory.htmlCategoryList = null;
	expenseCategory.generateReturnHtml = function(category) {
		angular.forEach(category, function(value, key){
			// if(item.childs != null){
			// 	expenseCategory.generateReturnHtml(item);
			// }
			expenseCategory.htmlCategoryList = '<span class="font-size-11 font-bold">CHILD CATEGORIES</span>'+
						'<table class="table table-bordered table-child-category">'+
							'<thead><tr>'+
								'<th width="90%" class="font-gray-dark font-size-11 text-uppercase">Name</th>'+
								'<th width="10%" class="text-center font-gray-dark font-size-11 text-uppercase">Actions</th>'+
							'</tr></thead>'+
							'<tbody>'+
								'<tr ng-repeat="child in category.childs">'+
									'<td>'+
										'<span ng-if="child != expenseCategory.editing" class="font-size-11">'+
										value.title+
										'</span>'+
										'<input type="text" class="form-control" ng-show="child == expenseCategory.editing" ng-model="expenseCategory.title" ng-value="expenseCategory.editing.title" ng-my-enter="expenseCategory.updateCategory()" ng-my-focus="child == expenseCategory.editing" ng-blur="expenseCategory.updateCategory()">'+
									'</td>'+
									'<td class="text-center">'+
										'<a href="javascript:;" class="btn btn-xs btn-success" ng-click="expenseCategory.setEditing(child)">EDIT</a>'+
	    								'<a href="javascript:;" class="btn btn-xs btn-danger" confirmed-click="expenseCategory.deleteCategory(child)" ng-confirm-click="">DELETE</a>'+
									'</td>'+
								'</tr>'+
							'</tbody>'+
						'</table>';
		});
	}
}
/**
 * @ngInject
 * Part of Angular Module
 */
 function PaymentController($http, $scope, Payment){
 	this.servicePayment = Payment;
 	this.busy = false;
 	this.results = [];

 	this.servicePayment.paymentList();

 	// Creating articles
	this.isCreating = false;
	this.startCreating = function() {
		this.isCreating = true;
		this.editing = null;
	}

	this.createPayment = function() {

		this.isNotValid = false;
		this.noDuplicate = false;
		angular.forEach(this.servicePayment.paymentList, function(value, key){
			if(value.title.toUpperCase() == this.paymentTitle.toUpperCase()){
				this.noDuplicate = true;

				return;
			} else if(this.noDuplicate != true){
				this.noDuplicate = false;
			}
		}, this);

		if(this.noDuplicate == true){
			return;
		}

		if(this.paymentTitle == undefined){
			this.isNotValid = true;

			return;
		} else {
			this.isNotValid = false;
		}

		var formData = new FormData();
		formData.append('title', this.paymentTitle);
		var promise = this.servicePayment.createNewPayment(formData);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.servicePayment.paymentList = payload.data;
				}
				this.isCreating = false;
				this.paymentTitle = undefined;
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
	
	this.editing = null;
	this.setEditing = function(payment) {
		this.paymentTitle = '';
		this.editing = payment;
		this.isCreating = false;
		this.isCreatingChild = false;
		this.title = payment.title;
	}

	this.updatePayment = function() {
		var formData = new FormData();
		formData.append('title', this.title);

		var promise = this.servicePayment.updatePayment(formData, this.editing);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.servicePayment.paymentList = payload.data;
				}
				this.editing = null;
				this.title = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	this.deletePayment = function(category) {
		var promise = this.servicePayment.deletePayment(category);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.servicePayment.paymentList = payload.data;
				}
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
 }
/**
 * @ngInject
 * Part of Angular Module
 */
 function CurrencyController($http, $scope, Currency){
 	this.serviceCurrency = Currency;
 	this.busy = false;
 	this.results = [];

 	this.serviceCurrency.currencyList();

 	// Creating articles
	this.isCreating = false;
	this.startCreating = function() {
		this.isCreating = true;
		this.currencyPrefix = '';
		this.currencyTitle = '';
		this.currencyRate = 0;
		this.editing = null;
	}

	this.createCurrency = function() {

		this.isNotValid = false;
		this.noDuplicate = false;
		angular.forEach(this.serviceCurrency.currencyList, function(value, key){
			if(value.prefix.toUpperCase() == this.currencyPrefix.toUpperCase() || value.title.toUpperCase() == this.currencyTitle.toUpperCase()){
				this.noDuplicate = true;
			} else if(this.noDuplicate != true){
				this.noDuplicate = false;
			}
		}, this);

		if(this.noDuplicate == true){
			return;
		}

		if(this.currencyTitle == '' || this.currencyPrefix == '' || this.currencyRate == 0){
			this.isNotValid = true;

			return;
		} else {
			this.isNotValid = false;
		}

		var formData = new FormData();
		formData.append('title', this.currencyTitle);
		formData.append('prefix', this.currencyPrefix);
		formData.append('conversion_rate', this.currencyRate);

		var promise = this.serviceCurrency.createNewCurrency(formData);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.serviceCurrency.currencyList = payload.data;
				}
				this.isCreating = false;
				this.currencyTitle = undefined;
				this.currencyPrefix = undefined;
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
	
	this.editing = null;
	this.setEditing = function(currency) {
		this.currencyTitle = '';
		this.currencyPrefix = '';
		this.currencyRate = 0;
		this.editing = currency;
		this.isCreating = false;
		this.title = currency.title;
		this.prefix = currency.prefix;
		this.conversion_rate = currency.conversion_rate;
	}

	this.updateCurrency = function() {
		var formData = new FormData();
		formData.append('title', this.title);
		formData.append('prefix', this.prefix);
		formData.append('conversion_rate', this.conversion_rate);

		var promise = this.serviceCurrency.updateCurrency(formData, this.editing);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.serviceCurrency.currencyList = payload.data;
				}
				this.editing = null;
				this.title = '';
				this.prefix = '';
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}

	this.deleteCurrency = function(currency) {
		var promise = this.serviceCurrency.deleteCurrency(currency);
		promise.then(
			function(payload) { 
				if(! payload.data.error) {
					this.serviceCurrency.currencyList = payload.data;
				}
				this.busy = false;
			}.bind(this),
			function(errorPayload) {
				this.busy = false;
			}.bind(this));
	}
 }
function ConfirmClick() {
	return {
		link: function (scope, element, attr) {
			var msg = attr.ngConfirmClick || "Are you sure?";
			var clickAction = attr.confirmedClick;
			var text = attr.ngAlertText;
			element.bind('click', function(event) {
				swal({   
					title: msg,   
					type: "warning",
					text: text,   
					showCancelButton: true,   
					confirmButtonText: "Yes, Continue!",   
					confirmButtonColor: "#DD6B55",
					closeOnConfirm: false 
				}, function(isConfirm) {   
					if(isConfirm) {
						scope.$eval(clickAction);
						swal.close();
					}
				});
			});
		}
	};
}
function CannotClick() {
	return {
		link: function (scope, element, attr) {
			var msg = attr.ngCannotClick || 'Unable to Delete.';
			var text = attr.ngAlertText;
			element.bind('click', function(event) {
		        swal({   
		            title: msg,   
		            text: text,
		            html: true
		        });
			});
		}
	};
}
function ConfirmSubmit() {
    return {
      link: function (scope, element, attr) {
          var msg = attr.ngConfirmSubmit || "Are you sure?";
          var clickAction = attr.confirmedSubmit;
          element.bind('submit', function (event) {
              if ( window.confirm(msg) ) {
                  scope.$eval(clickAction)
              }
          });
      }
    };
}

function MyEnter($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {
			element.on("keydown keypress", function(event) {
				if (event.which === 13) {
					// This will pass event where the expression used $event
					scope.$apply(function(){
						scope.$eval(attr.ngMyEnter);
					});
					event.preventDefault();
				}
			});
		}
	};
}
function MyFocus($timeout, $parse) {
	return {
		link: function (scope, element, attr) {
			scope.$watch(attr.ngMyFocus, function(value) {
				if(attr.ngMyFocus){
					$timeout(function () {
                        element[0].focus();
                    });
				}
			});
		}
	};
}
function ExpenseCatList() {
	return {
		template :'<h1>Test</h1>',
	};
}
/**
 * @ngInject
 */
function ExpenseCategory($http) {

	var ExpenseCategory = {
		'categoryList' : [],
		'busy': false,
		'baseUrl': '/kpi/public/expense-app/api'
	};

	ExpenseCategory.categoryList = function() {
		this.busy = true;
		$http.get(this.baseUrl + '/get/categories').success(function(data) {
	        this.categoryList = data;
	        this.busy = false;
	    }.bind(this))
	    .error(function(data, status, headers, config) {
			this.busy = false
		}.bind(this));
	}

	ExpenseCategory.createNewCategory = function(formData) {
		this.busy = true;
		return $http.post(this.baseUrl + '/store/category', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	ExpenseCategory.updateCategory = function(formData, category) {
		this.busy = true;
		return $http.post(this.baseUrl + '/update/'+ category.id +'/category', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	ExpenseCategory.deleteCategory = function(category) {
		this.busy = true;
		return $http.get(this.baseUrl + '/delete/'+category.id+'/category', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) {  this.busy = false; return data; }
		});
	}

	ExpenseCategory.getCategoryChilds = function(category) {
		this.busy = true;
		return $http.get(this.baseUrl + '/get/'+category.id+'/child', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	ExpenseCategory.createChildCategory = function(formData, category) {
		this.busy = true;
		return $http.post(this.baseUrl + '/create/'+category.id+'/child', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	return ExpenseCategory;
}
/**
 * @ngInject
 */
function Payment($http) {

	var Payment = {
		'paymentList' : [],
		'busy': false,
		'baseUrl': '/kpi/public/expense-app/api'
	};

	Payment.paymentList = function() {
		this.busy = true;
		$http.get(this.baseUrl + '/get/payments').success(function(data) {
	        this.paymentList = data;
	        this.busy = false;
	    }.bind(this))
	    .error(function(data, status, headers, config) {
			this.busy = false
		}.bind(this));
	}

	Payment.createNewPayment = function(formData) {
		this.busy = true;
		return $http.post(this.baseUrl + '/store/payment', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Payment.updatePayment = function(formData, payment) {
		this.busy = true;
		return $http.post(this.baseUrl + '/update/'+ payment.id +'/payment', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Payment.deletePayment = function(payment) {
		this.busy = true;
		return $http.get(this.baseUrl + '/delete/'+payment.id+'/payment', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) {  this.busy = false; return data; }
		});
	}
	
	return Payment;
}
/**
 * @ngInject
 */
function Currency($http) {

	var Currency = {
		'currencyList' : [],
		'busy': false,
		'baseUrl': '/kpi/public/expense-app/api'
	};

	Currency.currencyList = function() {
		this.busy = true;
		$http.get(this.baseUrl + '/get/currencies').success(function(data) {
	        this.currencyList = data;
	        this.busy = false;
	    }.bind(this))
	    .error(function(data, status, headers, config) {
			this.busy = false
		}.bind(this));
	}

	Currency.createNewCurrency = function(formData) {
		this.busy = true;
		return $http.post(this.baseUrl + '/store/currency', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Currency.updateCurrency = function(formData, currency) {
		this.busy = true;
		return $http.post(this.baseUrl + '/update/'+ currency.id +'/currency', formData, {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) { this.busy = false; return data; }
		});
	}

	Currency.deleteCurrency = function(currency) {
		this.busy = true;
		return $http.get(this.baseUrl + '/delete/'+currency.id+'/currency', {
			headers: { 'Content-Type': undefined },
			transformRequest: function(data) {  this.busy = false; return data; }
		});
	}
	
	return Currency;
}
//# sourceMappingURL=ng-app.js.map
