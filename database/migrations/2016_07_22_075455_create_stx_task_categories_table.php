<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStxTaskCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stx_task_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->int('position');
            $table->string('name');
            $table->string('title');
            $table->int('parent_id')->nullable();
            $table->int('lft')->nullable();
            $table->int('rgt')->nullable();
            $table->int('depth')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stx_task_categories');
    }
}
