<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('project_id');
            $table->integer('timeable_id');
            $table->string('timeable_type');
            $table->string('timeable_type_value');
            $table->date('time_in')->nullable();
            $table->date('time_out')->nullable();
            $table->integer('hours_rendered')->nullable();
            $table->date('date');
            $table->text('note')->nullable();
            $table->int('manual_entry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('time_tracks');
    }
}
