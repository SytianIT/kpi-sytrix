<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->text('description');
            $table->date('start_date')->nullable();
            $table->date('due_date')->nullable();
            $table->integer('max_development_time');
            $table->integer('max_design_time');
            $table->integer('max_pm_time');
            $table->integer('project_status_id')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('pm_id')->nullable();   
            $table->date('date_completed')->nullable();         
            $table->integer('parent_id')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
