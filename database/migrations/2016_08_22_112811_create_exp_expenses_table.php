<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->decimal('amount', 15, 2);
            $table->decimal('amount_in_php', 15, 2);
            $table->timestamp('date_occured');
            $table->text('attachments')->nullable();
            $table->integer('main_recurring_id')->nullable();
            $table->integer('recurring_id')->nullable();
            $table->integer('category_id');
            $table->integer('currency_id');
            $table->integer('payment_id');
            $table->integer('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exp_expenses');
    }
}
