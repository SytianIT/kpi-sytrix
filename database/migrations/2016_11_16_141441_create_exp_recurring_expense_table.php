<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpRecurringExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_recurring_expense', function (Blueprint $table) {
            $table->increments('id');
            $table->date('next_expense_recurring')->nullable();
            $table->integer('count_expense_recurring')->default(0);
            $table->decimal('total_expense_recurring', 15, 2)->default(0);
            $table->integer('recurring_every')->nullable();
            $table->string('recurring_of')->nullable();
            $table->integer('recurring_times')->nullable();
            $table->enum('status', ['active','expired','disabled'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exp_recurring_expense');
    }
}
