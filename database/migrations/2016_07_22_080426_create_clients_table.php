<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable();
            $table->string('company_name');
            $table->text('company_address');
            $table->text('contact_person')->nullable();
            $table->string('mobile_number');
            $table->string('landline_number');
            $table->string('email');
            $table->string('skype_id');
            $table->string('payment_terms')->nullable();
            $table->text('discounts')->nullable();
            $table->text('layout')->nullable();
            $table->text('hosting')->nullable();
            $table->string('domain')->nullable();
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
