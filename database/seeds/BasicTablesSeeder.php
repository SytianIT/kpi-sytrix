<?php

use App\Applications\Expense\Currency;
use App\Applications\Expense\ExpenseCategory;
use App\Applications\Expense\Payment;
use Illuminate\Database\Seeder;

class BasicTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExpenseCategory::truncate();
        Payment::truncate();
        ExpenseCategory::truncate();
        Currency::truncate();

        $firstCategory = new ExpenseCategory;
        $firstCategory->title = 'Advertising and Promotions';
        $firstCategory->save();

        $secondCategory = new ExpenseCategory;
        $secondCategory->title = 'Business taxes. Permit fees and licences';
        $secondCategory->save();

        $fourthCategory = new ExpenseCategory;
        $fourthCategory->title = 'Fuel and transportation';
        $fourthCategory->save();

        $fifthSecCategory = new ExpenseCategory;
        $fifthSecCategory->title = 'Legal. accounting and other professional fees';
        $fifthSecCategory->save();

        $sixthCategory = new ExpenseCategory;
        $sixthCategory->title = 'Office maintenance and repairs';
        $sixthCategory->save();

        $seventhCategory = new ExpenseCategory;
        $seventhCategory->title = 'Meals and entertainment';
        $seventhCategory->save();

        $eightCategory = new ExpenseCategory;
        $eightCategory->title = 'Training and Seminars';
        $eightCategory->save();

        $ninthCategory = new ExpenseCategory;
        $ninthCategory->title = 'Rentals';
        $ninthCategory->save();

        $tenthCategory = new ExpenseCategory;
        $tenthCategory->title = 'Salaries and benefits';
        $tenthCategory->save();

        $eleventhCategory = new ExpenseCategory;
        $eleventhCategory->title = 'Utilities';
        $eleventhCategory->save();

        $twelveCategory = new ExpenseCategory;
        $twelveCategory->title = 'Others';
        $twelveCategory->save();

        $thirteenthCategory = new ExpenseCategory;
        $thirteenthCategory->title = 'Office Supplies';
        $thirteenthCategory->save();

        $fourteenthCategory = new ExpenseCategory;
        $fourteenthCategory->title = 'Office furnitue and equipment';
        $fourteenthCategory->save();

        $fifthteenthCategory = new ExpenseCategory;
        $fifthteenthCategory->title = 'Server and Domain';
        $fifthteenthCategory->save();

        $sixteenthCategory = new ExpenseCategory;
        $sixteenthCategory->title = 'Softwares';
        $sixteenthCategory->save();

        $cash = new Payment;
        $cash->title = 'Cash';
        $cash->save();

        $cheque = new Payment;
        $cheque->title = 'Cheque';
        $cheque->save();

        $creditCard = new Payment;
        $creditCard->title = 'Credit Card';
        $creditCard->save();

        $paypal = new Payment;
        $paypal->title = 'PayPal';
        $paypal->save();

        $currency = new Currency;
        $currency->prefix = 'PHP';
        $currency->title = 'Philippine Peso';
        $currency->conversion_rate = 1;
        $currency->save();
    }
}
