 <?php

use App\Permission;
use App\Role;
use App\Syion;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_permissions')->truncate();
        Permission::truncate();
        Syion::truncate();

        /**
         * Creating SYION table entries
         */
        $generalSyion = new Syion;
        $generalSyion->name = Syion::GENERAL;
        $generalSyion->title = "General Permissions";
        $generalSyion->save();

        $sytrixSyion = new Syion;
        $sytrixSyion->name = Syion::SYTRIX;
        $sytrixSyion->title = "Sytian Project Metrics";
        $sytrixSyion->save();

        $expenseSyion = new Syion;
        $expenseSyion->name = Syion::EXPENSE;
        $expenseSyion->title = "Sytian Expense Metrics";
        $expenseSyion->save();

        $superAdmin = Role::where('name', '=', Role::SUPERADMIN)->first();

        $accesSystem = new Permission;
        $accesSystem->name = "access_system";
        $accesSystem->title = "Access Syion System";
        $accesSystem->description = "Able to access syion system";
        $accesSystem->syion()->associate($generalSyion);
        $accesSystem->save();

        $accessSiteSettings = new Permission;
        $accessSiteSettings->name = "access_site_settings";
        $accessSiteSettings->title = "Access Site Settings";
        $accessSiteSettings->description = "Able to access site settings";
        $accessSiteSettings->syion()->associate($generalSyion);
        $accessSiteSettings->save();

        $manageRoles = new Permission;
        $manageRoles->name = "manage_roles";
        $manageRoles->title = "Manage Roles";
        $manageRoles->description = "Able to Manage Roles";
        $manageRoles->syion()->associate($generalSyion);
        $manageRoles->save();

        $manageUsers = new Permission;
        $manageUsers->name = "manage_users";
        $manageUsers->title = "Manage Users";
        $manageUsers->description = "Able to manage users";
        $manageUsers->syion()->associate($generalSyion);
        $manageUsers->save();

        $manageDepartments = new Permission;
        $manageDepartments->name = "manage_departments";
        $manageDepartments->title = "Manage Departments";
        $manageDepartments->description = "Able to manage departments";
        $manageDepartments->syion()->associate($generalSyion);
        $manageDepartments->save();

        $manageProjectCategories = new Permission;
        $manageProjectCategories->name = "manage_project_categories";
        $manageProjectCategories->title = "Manage Project Categories";
        $manageProjectCategories->description = "Able to manage project categories";
        $manageProjectCategories->syion()->associate($generalSyion);
        $manageProjectCategories->save();

        $manageProjectStatus = new Permission;
        $manageProjectStatus->name = "manage_project_status";
        $manageProjectStatus->title = "Manage Project Status";
        $manageProjectStatus->description = "Able to manage project status";
        $manageProjectStatus->syion()->associate($generalSyion);
        $manageProjectStatus->save();

        $manageTaskCategorries = new Permission;
        $manageTaskCategorries->name = "manage_taks_categories";
        $manageTaskCategorries->title = "Manage Task Categories";
        $manageTaskCategorries->description = "Able to manage task categories";
        $manageTaskCategorries->syion()->associate($generalSyion);
        $manageTaskCategorries->save();

        /**
         * START PERMISSIONS FOR SYTRIX
         */
        $viewProjects = new Permission;
        $viewProjects->name = "access_sytrix";
        $viewProjects->title = "Access Sytian Project Metrics";
        $viewProjects->description = "Able to access the sytian project metrics";
        $viewProjects->syion()->associate($sytrixSyion);
        $viewProjects->save();

        $viewProjects = new Permission;
        $viewProjects->name = "access_sytrix_settings";
        $viewProjects->title = "Access Sytian Project Metrics Settings";
        $viewProjects->description = "Able to access the sytian project metrics settings";
        $viewProjects->syion()->associate($sytrixSyion);
        $viewProjects->save();

        $viewProjects = new Permission;
        $viewProjects->name = "view_projects";
        $viewProjects->title = "View Projects";
        $viewProjects->description = "Able to view list of projects";
        $viewProjects->syion()->associate($sytrixSyion);
        $viewProjects->save();

        $addProject = new Permission;
        $addProject->name = "add_project";
        $addProject->title = "Add Projects";
        $addProject->description = "Able to add a new project";
        $addProject->syion()->associate($sytrixSyion);
        $addProject->save();

        $manageProjectStatus = new Permission;
        $manageProjectStatus->name = "manage_project_status";
        $manageProjectStatus->title = "Manage Project Status";
        $manageProjectStatus->description = "Able to manage project status";
        $manageProjectStatus->syion()->associate($sytrixSyion);
        $manageProjectStatus->save();

        $manageProjectCategory = new Permission;
        $manageProjectCategory->name = "manage_project_category";
        $manageProjectCategory->title = "Manage Project Category";
        $manageProjectCategory->description = "Able to manage project category";
        $manageProjectCategory->syion()->associate($sytrixSyion);
        $manageProjectCategory->save();

        $manageTask = new Permission;
        $manageTask->name = "manage_task";
        $manageTask->title = "Manage Task";
        $manageTask->description = "Able to manage tasks";
        $manageTask->syion()->associate($sytrixSyion);
        $manageTask->save();

        $manageTaskCategory = new Permission;
        $manageTaskCategory->name = "manage_task_category";
        $manageTaskCategory->title = "Manage Task Category";
        $manageTaskCategory->description = "Able to manage project tasks category";
        $manageTaskCategory->syion()->associate($sytrixSyion);
        $manageTaskCategory->save();

        /**
         * START PERMISSIONS FOR SYTIAN EXPENSE
         */
        $accessExpense = new Permission;
        $accessExpense->name = "access_expense";
        $accessExpense->title = "Access Sytian Expense";
        $accessExpense->description = "Access to sytian expense";
        $accessExpense->syion()->associate($expenseSyion);
        $accessExpense->save();

        $accessExpenseSiteSettings = new Permission;
        $accessExpenseSiteSettings->name = "access_expense_settings";
        $accessExpenseSiteSettings->title = "Access Sytian Expense Settings";
        $accessExpenseSiteSettings->description = "Access to settings of sytian expense";
        $accessExpenseSiteSettings->syion()->associate($expenseSyion);
        $accessExpenseSiteSettings->save();

        $viewExpense = new Permission;
        $viewExpense->name = "view_expenses";
        $viewExpense->title = "View Expenses";
        $viewExpense->description = "Able to view list of all expenses";
        $viewExpense->syion()->associate($expenseSyion);
        $viewExpense->save();

        $createExpenses = new Permission;
        $createExpenses->name = "create_expense";
        $createExpenses->title = "Create Expense";
        $createExpenses->description = "Able to create expense";
        $createExpenses->syion()->associate($expenseSyion);
        $createExpenses->save();

        $updateExpenses = new Permission;
        $updateExpenses->name = "update_expense";
        $updateExpenses->title = "Update Expense";
        $updateExpenses->description = "Able to update expense";
        $updateExpenses->syion()->associate($expenseSyion);
        $updateExpenses->save();

        $deleteExpenses = new Permission;
        $deleteExpenses->name = "delete_expense";
        $deleteExpenses->title = "Delete Expense";
        $deleteExpenses->description = "Able to delete expense";
        $deleteExpenses->syion()->associate($expenseSyion);
        $deleteExpenses->save();

        $viewRecurringExpense = new Permission;
        $viewRecurringExpense->name = "view_recurring_expenses";
        $viewRecurringExpense->title = "View Recurring Expenses";
        $viewRecurringExpense->description = "Able to view list of all recurring expenses";
        $viewRecurringExpense->syion()->associate($expenseSyion);
        $viewRecurringExpense->save();

        $createRecurringExpense = new Permission;
        $createRecurringExpense->name = "create_recurring_expense";
        $createRecurringExpense->title = "Create Recurring Expense";
        $createRecurringExpense->description = "Able to create recurring expense";
        $createRecurringExpense->syion()->associate($expenseSyion);
        $createRecurringExpense->save();

        $updateRecurringExpense = new Permission;
        $updateRecurringExpense->name = "update_recurring_expense";
        $updateRecurringExpense->title = "Update Recurring Expense";
        $updateRecurringExpense->description = "Able to update recurring expense";
        $updateRecurringExpense->syion()->associate($expenseSyion);
        $updateRecurringExpense->save();

        $deleteRecurringExpense = new Permission;
        $deleteRecurringExpense->name = "delete_recurring_expense";
        $deleteRecurringExpense->title = "Delete Recurring Expense";
        $deleteRecurringExpense->description = "Able to delete recurring expense";
        $deleteRecurringExpense->syion()->associate($expenseSyion);
        $deleteRecurringExpense->save();

        $viewExpenseSyionAuditLog = new Permission;
        $viewExpenseSyionAuditLog->name = "view_expense_syion_log";
        $viewExpenseSyionAuditLog->title = "View Audit Log";
        $viewExpenseSyionAuditLog->description = "Able to view audit logs";
        $viewExpenseSyionAuditLog->syion()->associate($expenseSyion);
        $viewExpenseSyionAuditLog->save();

        $viewExpenseSyionReports = new Permission;
        $viewExpenseSyionReports->name = "view_expense_syion_reports";
        $viewExpenseSyionReports->title = "View Expense Reports";
        $viewExpenseSyionReports->description = "Able to view and generate reports";
        $viewExpenseSyionReports->syion()->associate($expenseSyion);
        $viewExpenseSyionReports->save();

        // Attach all permissions to superadmin staff
        $permissions = Permission::get();
        foreach($permissions as $permission){
            $superAdmin->permissions()->attach($permission);
        }
    }
}
