<?php

use App\Permission;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::truncate();
    	User::truncate();

        // Create User
    	$userSuperAdmin = User::firstOrCreate([
    		'firstname' => 'Sytian',
    		'lastname' => 'Admin',
    		'email' => 'admin@email.com',
    		'username' => 'admin',
    		'password' => bcrypt('admin2468'),
    		'job_title' => 'Administrator',
    		'mobile_number' => '(+63) 917-511-0403',
    		'birthdate' => Carbon::now(),
    		'age' => 1,
    		'address' => 'Unit 309, TDS Bldg. #72 Kamias Road, Brgy. East Kamias Quezon City, Philippines 1102'
    		]);

    	$userSuperAdmin->save();

       	// Creating Super Admin Staff
    	$roleSuperAdmin = Role::firstOrCreate([
    		'name' => 'superadmin',
            'title' => 'Super Admin',
    		'description' => 'Super admin staff',
    	]);
    	$roleSuperAdmin->save();

       	// Assigned superadmin staff to superadmin user
    	$userSuperAdmin->roles()->attach($roleSuperAdmin);
    }
}
